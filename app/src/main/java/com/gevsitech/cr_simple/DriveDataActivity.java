package com.gevsitech.cr_simple;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gevsitech.cr_simple.data_separation.PrimaevalData;
import com.gevsitech.cr_simple.data_separation.ReadCheck;
import com.gevsitech.cr_simple.data_separation.SettingCheck;

public class DriveDataActivity extends Activity {
    private ListView list_drive_data;
    private String[] drivedatahead=new String[]{"ID","型號","序號","PCB","韌體版本"};
    private String[] drivedatacontent=new String[5];
    private PrimaevalData primaevalData=new PrimaevalData();
    private ReadCheck readCheck=new ReadCheck();
    private SettingCheck settingCheck=new SettingCheck();
    private ProgressDialog dialog;
    private volatile Boolean checkflag = true;
    private int SendCount;
    private Boolean Sendflag = true;
    private Button drivedata_close;
    private BluetoothService bluetoothService=BluetoothService.getBluetoothService();
    String ID=null;
    String Snumber_F=null;
    String Snumber_M=null;
    String Snumber_R=null;
    String Model_name_1=null;
    String Model_name_2=null;
    String Model_name_3=null;
    String Model_name_4=null;
    String Model_name_5=null;
    String PCB_Version=null;
    String FW_Version=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_drive_data);

        bluetoothService.setBtHandler(BTPoccess);

        list_drive_data=(ListView)findViewById(R.id.list_drive_data);
        Drivedataadapter drivedataadapter=new Drivedataadapter(this);
        drivedata_close=(Button)findViewById(R.id.drivedata_close);

        list_drive_data.setAdapter(drivedataadapter);




        if(bluetoothService.isConnectedCheck()){
            dialog = ProgressDialog.show(DriveDataActivity.this,
                    "讀取初始資料中", "請稍待片刻...",true);
            SendCount = 1 ;
            Sendflag  = true;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(Sendflag){
                        try {
                            Thread.sleep(200);
                        }catch (Exception e){

                        }
                        switch (SendCount) {

                            case 1 :
                                //寫入1
                                char[] s1 = settingCheck.Send("0002");
                                if(checkflag){
                                    bluetoothService.write2(s1);
                                    SendCount = 2 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入25%");
                                }
                                break;
                            case 2 :
                                char[] s2 = settingCheck.Send("0003");
                                if(checkflag){
                                    bluetoothService.write2(s2);
                                    SendCount = 3 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入50%");
                                }
                                break;
                            case 3 :
                                char[] s3 = settingCheck.Send("0004");
                                if(checkflag){
                                    bluetoothService.write2(s3);
                                    SendCount = 4 ;
                                    checkflag = false;
                                    // dialog.setMessage("寫入75%");
                                }
                                break;
                            case 4 :
                                char[] s4 = settingCheck.Send("0005");
                                if(checkflag){
                                    bluetoothService.write2(s4);
                                    SendCount = 5 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入75%");
                                }
                                break;
                            case 5 :
                                char[] s5 = settingCheck.Send("0006");
                                if(checkflag){
                                    bluetoothService.write2(s5);
                                    SendCount = 6 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 6 :
                                char[] s6 = settingCheck.Send("0007");
                                if(checkflag){
                                    bluetoothService.write2(s6);
                                    SendCount = 7 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 7 :
                                char[] s7 = settingCheck.Send("0008");
                                if(checkflag){
                                    bluetoothService.write2(s7);
                                    SendCount = 8 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 8 :
                                char[] s8 = settingCheck.Send("0009");
                                if(checkflag){
                                    bluetoothService.write2(s8);
                                    SendCount = 9 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 9 :
                                char[] s9 = settingCheck.Send("000A");
                                if(checkflag){
                                    bluetoothService.write2(s9);
                                    SendCount = 10 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 10 :
                                char[] s10 = settingCheck.Send("000B");
                                if(checkflag){
                                    bluetoothService.write2(s10);
                                    SendCount = 11 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 11 :
                                char[] s11 = settingCheck.Send("03E8");
                                if(checkflag){
                                    bluetoothService.write2(s11);
                                    SendCount = 0 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 0 :
                                if(checkflag) {
                                    Sendflag = false;
                                    break;
                                }
                        }

                    }
                    BTPoccess.obtainMessage(5).sendToTarget();

                }
            });
            thread.start();



        }else{
            Toast.makeText(DriveDataActivity.this,"藍芽尚未連線，無法讀取資料",Toast.LENGTH_SHORT).show();

        }


        drivedata_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public class Drivedataadapter extends BaseAdapter{
        private LayoutInflater Inflater;
        public Drivedataadapter(Context c){
            Inflater=LayoutInflater.from(c);
        }
        @Override
        public int getCount(){return drivedatahead.length;}
        @Override
        public Object getItem(int position){return drivedatahead[position];}
        @Override
        public long getItemId(int position){return position;}
        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            ViewHolder viewHolder=null;
            if(convertView==null){
                viewHolder=new ViewHolder();
                convertView=Inflater.inflate(R.layout.list_setting,null);
                viewHolder.drivedatahead=(TextView)convertView.findViewById(R.id.txt_listhead);
                viewHolder.drivedatacontent=(TextView)convertView.findViewById(R.id.txt_listsub);

                viewHolder.drivedatahead.setText(drivedatahead[position]);
                viewHolder.drivedatacontent.setText(drivedatacontent[position]);

                convertView.setTag(viewHolder);
            }
            else{
                viewHolder=(ViewHolder) convertView.getTag();
                viewHolder.drivedatahead.setText(drivedatahead[position]);
                viewHolder.drivedatacontent.setText(drivedatacontent[position]);
            }
            return convertView;
        }

    }
    private static class ViewHolder{
        private TextView drivedatahead;
        private TextView drivedatacontent;
    }
    private Handler BTPoccess = new Handler(){
        private StringPoccess poccess = new StringPoccess();;
        private boolean tagflag = false;
        private boolean Aflag = false;

        String  data ;
        String  DC1 ;
        String  DC2 ;
        String  C0,C1,C2,C3,C4,C5;
        String  DM1 ;
        String  GPS ;
        /**
         *  連線中狀態值
         *  1. DISCONNECT 斷線狀態    0
         *  2. CONNECTED  連線中      1
         *  3. CONNECTING 嘗試連線中  2
         **/
        private final static int DISCONNECT = 0 ;
        private final static int CONNECTED = 1 ;
        private final static int CONNECTING = 2 ;

        /**
         *  連線中狀態值
         *  1. DATAWAIT   等待資料中    0
         *  2. DATAREAD   資料讀取      1
         *  3. DATAWIRTE  資料寫入      2
         **/

        private final static int DATAWAIT = 0 ;
        private final static int DATAREAD = 1 ;
        private final static int DATAWIRTE = 2 ;
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case DISCONNECT :
                    if(tagflag) Log.d("Handle" ,"DISCONNECT" );
                    break;
                case CONNECTED :
                    if(tagflag)Log.d("Handle" ,"CONNECTED" );
                    if(msg.arg1 == DATAREAD) {
                        data = (String) msg.obj;
                        if(data == null)return;
                        primaevalData.split(data);
                        System.out.println(primaevalData.getId()[2]);
                        //分解DC2資料
                    }
                    if(primaevalData.getId()[2]==0xCF){

                        readCheck.setData(primaevalData.getdata());
                        System.out.println("Address="+readCheck.getAddress());
                        if(readCheck.getAddress().equals("02")){
                            ID=String.valueOf(readCheck.getValues());
                            checkflag=true;
                        }
                        else if(readCheck.getAddress().equals("03")){
                            Snumber_F=String.valueOf(readCheck.getValues());
                            checkflag=true;
                        }
                        else if(readCheck.getAddress().equals("04")){
                            Snumber_M=String.valueOf(readCheck.getValues());
                            checkflag=true;
                        }
                        else if(readCheck.getAddress().equals("05")){
                            Snumber_R=String.valueOf(readCheck.getValues());
                            checkflag=true;
                        }
                        else if(readCheck.getAddress().equals("06")){
                            Model_name_1=String.valueOf((char)readCheck.getValues());
                            checkflag=true;
                        }
                        else if(readCheck.getAddress().equals("07")){
                            Model_name_2=String.valueOf((char)readCheck.getValues());
                            checkflag=true;
                        }
                        else if(readCheck.getAddress().equals("08")){
                            Model_name_3=String.valueOf((char)readCheck.getValues());
                            checkflag=true;
                        }
                        else if(readCheck.getAddress().equals("09")){
                            Model_name_4=String.valueOf((char)readCheck.getValues());
                            checkflag=true;
                        }
                        else if(readCheck.getAddress().equals("0a")){
                            Model_name_5=String.valueOf((char)readCheck.getValues());
                            checkflag=true;
                        }
                        else if(readCheck.getAddress().equals("0b")){
                            PCB_Version=String.valueOf((char)readCheck.getValues());
                            checkflag=true;
                        }
                        else if(readCheck.getAddress().equals("3e8")){
                            FW_Version=String.valueOf(readCheck.getValues());
                            checkflag=true;
                        }
                        else{
                            checkflag=true;
                        }

                        drivedatacontent[0]=ID;//ID
                        drivedatacontent[1]=Model_name_1+Model_name_2+Model_name_3+Model_name_4+Model_name_5;//型號
                        drivedatacontent[2]=Snumber_F+Snumber_M+Snumber_R;//序號
                        drivedatacontent[3]=PCB_Version;
                        drivedatacontent[4]=FW_Version;

                        list_drive_data.invalidateViews();





                    }
                    /***
                     * GPS 資料分解區
                     */
                    break;
                case CONNECTING :
                    if(tagflag)Log.d("Handle" ,"CONNECTING" );
                    break;
                case 5:
                    try {
                        dialog.dismiss();
                    }catch (Exception e){
                        dialog.dismiss();
                        e.printStackTrace();
                    }
                    break;
            }


        }
    };





}
