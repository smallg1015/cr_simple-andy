package com.gevsitech.cr_simple;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class VersionDataActivity extends Activity {
    TextView version_content;
    TextView version_title;
    Button version_close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_version_data);

        version_close = (Button) findViewById(R.id.version_close);
        version_title  = (TextView)findViewById(R.id.version_title);
        version_content = (TextView)findViewById(R.id.version_content);

        version_content.setText("Version0.1:\n1. 修正\"驅動器狀態\"調整控制器溫度1會影響控制器溫度2的問題\n2. 版本資訊移至設定頁面\n3. 錯誤偵測頁面\"MOS過高溫\",\"Bus過高壓\"新增說明\n\nVersion0.2:\n1.\"設定\"頁面新增\"資料繪圖\"頁面\n2.啟動紀錄及停止紀錄功能完成\n3.當CAN BUS斷線時會提醒使用者CAN BUS斷線\n\nVersion0.3Beta:\n1. 新增驅動器設定---保護-----電池保護\n" +
                "\nVersion0.4Beta:\n1. 資料圖表之圖表頁面修改,新增可選擇顯示項目之功能\n2. 設定頁面新增開關圖示\n\nVersion 0.5Beta:\n" + "1. 新增驅動器設定功能\n" + "2. 新增檔案頁面(包含檔案上傳雲端,清理手機檔案功能)\n" + "3. 新增授權碼功能\n" + "4. 新增控制器資訊功能"
                );


        version_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
