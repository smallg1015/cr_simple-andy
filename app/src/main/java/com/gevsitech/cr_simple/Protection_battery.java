package com.gevsitech.cr_simple;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Toast;

import com.gevsitech.cr_simple.data_separation.PrimaevalData;
import com.gevsitech.cr_simple.data_separation.ReadCheck;
import com.gevsitech.cr_simple.data_separation.SettingCheck;
import com.gevsitech.cr_simple.data_separation.WriteCheck;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

//import static com.gevsitech.cr_simple.R.id.view;

public class Protection_battery extends AppCompatActivity {
    private EditText batprotect_pn,batprotect_lp,batprotect_lr,batprotect_hp,batprotect_hr,batprotect_lw,batprotect_hw;
    private SeekBar batprotect_sBar1,batprotect_sBar2,batprotect_sBar3,batprotect_sBar4,batprotect_sBar5,batprotect_sBar6,batprotect_sBar7;
    private Button batprotect_btnnext,batprotect_btn1,batprotect_btn2,batprotect_btn3,batprotect_btn4,batprotect_btn5,batprotect_btn6,batprotect_btn7,batprotect_btnsetting;
    private String nm,lp,lr,hp,hr,lw,hw;
    private String[] hex=new String[]{"70,00","71,00","72,00","74,00","75,00","76,00","73,00"};
    private Boolean Sendflag = true;
    private volatile Boolean checkflag = true;
    private ProgressDialog dialog;
    private int SendCount = 1;
    private Boolean changenumber=true;
    private int save_nm;
    private float save_lp,save_lr,save_hp,save_hr,save_hw,save_lw;
    private ImageButton batterybacktoset;


    private PrimaevalData primaevalData = new PrimaevalData();
    private ReadCheck readCheck  = new ReadCheck();
    private WriteCheck writeCheck = new WriteCheck();
    private SettingCheck settingCheck = new SettingCheck();
    BluetoothService BluetoothS = BluetoothService.getBluetoothService();

    private File LogPath;
    String logFileName;
    LogService logService = LogService.getLogService();
    private String downFilename;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_protection_battery);

        init();
        LogPath = this.getExternalFilesDir(null);
        //取得今日日期
        Calendar mCal = Calendar.getInstance();
        CharSequence ss = DateFormat.format("yyyy-MM-dd", mCal.getTime());    // kk:24小時制, hh:12小時制
        logFileName = String.valueOf(ss)+".txt";

        logService.SaveLog(LogPath, logFileName, "START Protection_battery");
        BluetoothS.setBtHandler(BTPoccess);


        //BluetoothS.write2(settingCheck.SendPassword("03EE","4576"));
        System.out.println("PASSWORDSEND!!!!!!!!!!!!!!!");


        if(BluetoothS.isConnectedCheck()){
            dialog = ProgressDialog.show(Protection_battery.this,
                    "讀取初始資料中", "請稍待片刻...",true);
            SendCount = 1 ;
            Sendflag  = true;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(Sendflag){
                        try {
                            Thread.sleep(200);
                        }catch (Exception e){

                        }
                        switch (SendCount) {

                            case 1 :
                                //寫入1
                                char[] s1 = settingCheck.Send("005D");
                                if(checkflag){
                                    BluetoothS.write2(s1);
                                    SendCount = 2 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入25%");
                                }
                                break;
                            case 2 :
                                char[] s2 = settingCheck.Send("005E");
                                if(checkflag){
                                    BluetoothS.write2(s2);
                                    SendCount = 3 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入50%");
                                }
                                break;
                            case 3 :
                                char[] s3 = settingCheck.Send("005F");
                                if(checkflag){
                                    BluetoothS.write2(s3);
                                    SendCount = 4 ;
                                    checkflag = false;
                                    // dialog.setMessage("寫入75%");
                                }
                                break;
                            case 4 :
                                char[] s4 = settingCheck.Send("0060");
                                if(checkflag){
                                    BluetoothS.write2(s4);
                                    SendCount = 5 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入75%");
                                }
                                break;
                            case 5 :
                                char[] s5 = settingCheck.Send("0061");
                                if(checkflag){
                                    BluetoothS.write2(s5);
                                    SendCount = 6 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 6 :
                                char[] s6 = settingCheck.Send("0062");
                                if(checkflag){
                                    BluetoothS.write2(s6);
                                    SendCount = 7 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 7 :
                                char[] s7 = settingCheck.Send("0063");
                                if(checkflag){
                                    BluetoothS.write2(s7);
                                    SendCount = 0 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 0 :
                                if(checkflag) {
                                    Sendflag = false;
                                    break;
                                }
                        }

                    }
                    BTPoccess.obtainMessage(5).sendToTarget();

                }
            });
            thread.start();



        }else{
            Toast.makeText(Protection_battery.this,"藍芽尚未連線，無法讀取資料",Toast.LENGTH_SHORT).show();
            batprotect_btn1.setEnabled(false);
            batprotect_btn2.setEnabled(false);
            batprotect_btn3.setEnabled(false);
            batprotect_btn4.setEnabled(false);
            batprotect_btn5.setEnabled(false);
            batprotect_btn6.setEnabled(false);
            batprotect_btn7.setEnabled(false);
            batprotect_btnsetting.setEnabled(false);
            logService.SaveLog(LogPath, logFileName, "bluetooth isn't connect");
        }


        batterybacktoset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        batprotect_pn.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(batprotect_pn.getText().toString().length() > 0) {
                        int a =(int)(Integer.parseInt(batprotect_pn.getText().toString())/65535.0f*100);
                        save_nm=Integer.parseInt(batprotect_pn.getText().toString());
                        changenumber=false;
                        batprotect_sBar1.setProgress(a);

                    }
                }
                return false;
            }
        });

        batprotect_lp.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(batprotect_lp.getText().toString().length() > 0) {
                        int a=(int)((Float.valueOf(batprotect_lp.getText().toString())-65)/60.f*100);
                        save_lp=Float.valueOf(batprotect_lp.getText().toString());
                        changenumber=false;
                        batprotect_sBar2.setProgress(a);
                    }
                }
                return false;
            }
        });
        batprotect_lr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(batprotect_lr.getText().toString().length() > 0) {
                        int a=(int)((Float.valueOf(batprotect_lr.getText().toString())-65)/60.0f*100);
                        save_lr=Float.valueOf(batprotect_lr.getText().toString());
                        changenumber=false;
                        batprotect_sBar3.setProgress(a);
                    }
                }
                return false;
            }
        });
        batprotect_lw.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(batprotect_lw.getText().toString().length() > 0) {
                        int a=(int)((Float.valueOf(batprotect_lw.getText().toString())-65)/60.0f*100);
                        save_lw=Float.valueOf(batprotect_lw.getText().toString());
                        changenumber=false;
                        batprotect_sBar4.setProgress(a);
                    }
                }
                return false;
            }
        });
        batprotect_hp.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(batprotect_hp.getText().toString().length() > 0) {
                        int a=(int)((Float.valueOf(batprotect_hp.getText().toString())-65)/60.0f*100);
                        save_hp=Float.valueOf(batprotect_hp.getText().toString());
                        changenumber=false;
                        batprotect_sBar5.setProgress(a);
                    }
                }
                return false;
            }
        });
        batprotect_hr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(batprotect_hr.getText().toString().length() > 0) {
                        int a=(int)((Float.valueOf(batprotect_hr.getText().toString())-65)/60.0f*100);
                        save_hr=Float.valueOf(batprotect_hr.getText().toString());
                        changenumber=false;
                        batprotect_sBar6.setProgress(a);
                    }
                }
                return false;
            }
        });
        batprotect_hw.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(batprotect_hw.getText().toString().length() > 0) {
                        int a=(int)((Float.valueOf(batprotect_hw.getText().toString())-65)/60.0f*100);
                        save_hw=Float.valueOf(batprotect_hw.getText().toString());
                        changenumber=false;
                        batprotect_sBar7.setProgress(a);
                    }
                }
                return false;
            }
        });




        batprotect_sBar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int a=(int)(progress/100.0f*65535);
                System.out.println(a);
                if(changenumber) {
                    batprotect_pn.setText(a + "");
                }else {
                    batprotect_pn.setText(save_nm + "");
                    changenumber=true;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });
        batprotect_sBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(batprotect_sBar2.getProgress()/100.0f*60+65);
                if(changenumber) {
                    batprotect_lp.setText(String.format("%.1f",a));
                }else {
                    batprotect_lp.setText(save_lp + "");
                    changenumber=true;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });
        batprotect_sBar3.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(batprotect_sBar3.getProgress()/100.0f*60+65);
                if(changenumber) {
                    batprotect_lr.setText(String.format("%.1f",a));
                }else {
                    batprotect_lr.setText(save_lr + "");
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });
        batprotect_sBar4.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(batprotect_sBar4.getProgress()/100.0f*60+65);
                if(changenumber) {
                    batprotect_lw.setText(String.format("%.1f",a));
                }else {
                    batprotect_lw.setText(save_lw + "");
                    changenumber=true;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });
        batprotect_sBar5.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(batprotect_sBar5.getProgress()/100.0f*60+65);
                if(changenumber) {
                    batprotect_hp.setText(String.format("%.1f",a));
                }else {
                    batprotect_hp.setText(save_hp + "");
                    changenumber=true;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });
        batprotect_sBar6.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(batprotect_sBar6.getProgress()/100.0f*60+65);
                if(changenumber) {
                    batprotect_hr.setText(String.format("%.1f",a));
                }else {
                    batprotect_hr.setText(save_hr + "");
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });
        batprotect_sBar7.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(batprotect_sBar7.getProgress()/100.0f*60+65);
                if(changenumber) {
                    batprotect_hw.setText(String.format("%.1f",a));
                }else {
                    batprotect_hw.setText(save_hw + "");
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });
        batprotect_btn1.setOnClickListener(new Button.OnClickListener(){
        @Override
        public void onClick(View v){
            dialog = ProgressDialog.show(Protection_battery.this,
                    "寫入資料中", "請稍後...",true);
            SendCount = 1 ;
            Sendflag  = true;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {

                    while(Sendflag){

                        switch (SendCount) {

                            case 1 :
                                char[] s1 =settingCheck.SendWithData("005D" ,(short)(Integer.valueOf(batprotect_pn.getText().toString())*1));
                                if(checkflag){
                                    BluetoothS.write2(s1);
                                    SendCount = 0 ;
                                    checkflag = false;
                                    // dialog.setMessage("25%");
                                }
                                break;
                            case 0 :
                                if(checkflag) {
                                    Sendflag = false;
                                    break;
                                }
                        }
                    }
                    try {
                        System.out.println("B");
                        dialog.setMessage("寫入更新");
                        Thread.sleep(1000);
                        dialog.dismiss();
                    }catch (Exception e){
                        dialog.dismiss();
                        e.printStackTrace();
                    }

                }
            });
            thread.start();
            logService.SaveLog(LogPath, logFileName, "Battery保護計數值:"+batprotect_pn.getText().toString());
        }
        });
        batprotect_btn2.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_battery.this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    short a=(short)(Float.valueOf(batprotect_lp.getText().toString())*10);
                                    char[] s2 =settingCheck.SendWithData("005E" ,a);

                                    if(checkflag){
                                        BluetoothS.write2(s2);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;

                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入更新");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "Battery欠壓保護點:"+batprotect_lp.getText().toString());

            }

        });
        batprotect_btn3.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_battery.this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    short a=(short)(Float.valueOf(batprotect_lr.getText().toString())*10);
                                    char[] s3 =settingCheck.SendWithData("005F" ,a);

                                    if(checkflag){
                                        BluetoothS.write2(s3);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;

                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入更新");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "Battery欠壓回復點:"+batprotect_lr.getText().toString());

            }

        });
        batprotect_btn4.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_battery.this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    short a=(short)(Float.valueOf(batprotect_lw.getText().toString())*10);
                                    char[] s4 =settingCheck.SendWithData("0060" ,a);

                                    if(checkflag){
                                        BluetoothS.write2(s4);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;

                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入更新");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "Battery欠壓告警點:"+batprotect_lw.getText().toString());

            }

        });
        batprotect_btn5.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_battery.this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    short a=(short)(Float.valueOf(batprotect_hp.getText().toString())*10);
                                    char[] s5 =settingCheck.SendWithData("0061" ,a);

                                    if(checkflag){
                                        BluetoothS.write2(s5);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;

                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入更新");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "Battery欠壓保護點:"+batprotect_hp.getText().toString());


            }

        });
        batprotect_btn6.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_battery.this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    short a=(short)(Float.valueOf(batprotect_hr.getText().toString())*10);
                                    char[] s6 =settingCheck.SendWithData("0062" ,a);

                                    if(checkflag){
                                        BluetoothS.write2(s6);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入更新");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "Battery高溫回復點:"+batprotect_hr.getText().toString());


            }

        });
        batprotect_btn7.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_battery.this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    short a=(short)(Float.valueOf(batprotect_hw.getText().toString())*10);
                                    char[] s7 =settingCheck.SendWithData("0063" ,a);

                                    if(checkflag){
                                        BluetoothS.write2(s7);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;

                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入更新");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "Battery過壓告警點:"+batprotect_hw.getText().toString());

            }

        });

        batprotect_btnsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = ProgressDialog.show(Protection_battery.this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s1_e=settingCheck.SendEEPROMData("005D");

                                    if(checkflag){
                                        BluetoothS.write2(s1_e);
                                        SendCount = 2 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 2 :
                                    char[] s2_e=settingCheck.SendEEPROMData("005E");
                                    if(checkflag){
                                        BluetoothS.write2(s2_e);
                                        SendCount = 3 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 3 :
                                    char[] s3_e=settingCheck.SendEEPROMData("005F");
                                    if(checkflag){
                                        BluetoothS.write2(s3_e);
                                        SendCount = 4 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 4 :
                                    char[] s4_e=settingCheck.SendEEPROMData("0060");
                                    if(checkflag){
                                        BluetoothS.write2(s4_e);
                                        SendCount = 5 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 5 :
                                    char[] s5_e=settingCheck.SendEEPROMData("0061");
                                    if(checkflag){
                                        BluetoothS.write2(s5_e);
                                        SendCount = 6 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 6 :
                                    char[] s6_e=settingCheck.SendEEPROMData("0062");
                                    if(checkflag){
                                        BluetoothS.write2(s6_e);
                                        SendCount = 7 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 7 :
                                    char[] s7_e=settingCheck.SendEEPROMData("0063");
                                    if(checkflag){
                                        BluetoothS.write2(s7_e);
                                        SendCount = 0 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                    }
                                    break;

                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();

            }
        });
    }

    private void init(){
        batprotect_pn=(EditText)findViewById(R.id.batprotect_nm);
        batprotect_lp=(EditText)findViewById(R.id.batprotect_lp);
        batprotect_lr=(EditText)findViewById(R.id.batprotect_lr);
        batprotect_hp=(EditText)findViewById(R.id.batprotect_hp);
        batprotect_hr=(EditText)findViewById(R.id.batprotect_hr);
        batprotect_lw=(EditText)findViewById(R.id.batprotect_lw);
        batprotect_hw=(EditText)findViewById(R.id.batprotect_hw);
        batprotect_sBar1=(SeekBar) findViewById(R.id.batprotect_sBar1);
        batprotect_sBar2=(SeekBar) findViewById(R.id.batprotect_sBar2);
        batprotect_sBar3=(SeekBar) findViewById(R.id.batprotect_sBar3);
        batprotect_sBar4=(SeekBar) findViewById(R.id.batprotect_sBar4);
        batprotect_sBar5=(SeekBar) findViewById(R.id.batprotect_sBar5);
        batprotect_sBar6=(SeekBar) findViewById(R.id.batprotect_sBar6);
        batprotect_sBar7=(SeekBar) findViewById(R.id.batprotect_sBar7);
        batprotect_btn1=(Button) findViewById(R.id.batprotect_btn1);
        batprotect_btn2=(Button) findViewById(R.id.batprotect_btn2);
        batprotect_btn3=(Button) findViewById(R.id.batprotect_btn3);
        batprotect_btn4=(Button) findViewById(R.id.batprotect_btn4);
        batprotect_btn5=(Button) findViewById(R.id.batprotect_btn5);
        batprotect_btn6=(Button) findViewById(R.id.batprotect_btn6);
        batprotect_btn7=(Button) findViewById(R.id.batprotect_btn7);
        batprotect_btnsetting=(Button) findViewById(R.id.batprotect_btnsetting);
        batterybacktoset=(ImageButton)findViewById(R.id.batterybacktoset);

    }
    private Handler BTPoccess = new Handler(){
        private StringPoccess poccess = new StringPoccess();

        private String data ;

        private boolean tagflag = false;

        /**
         *  連線中狀態值
         *  1. DISCONNECT 斷線狀態    0
         *  2. CONNECTED  連線中      1
         *  3. CONNECTING 嘗試連線中  2
         **/
        private final static int DISCONNECT = 0 ;
        private final static int CONNECTED = 1 ;
        private final static int CONNECTING = 2 ;

        /**
         *  連線中狀態值
         *  1. DATAWAIT   等待資料中    0
         *  2. DATAREAD   資料讀取      1
         *  3. DATAWIRTE  資料寫入      2
         **/

        private final static int DATAWAIT = 0 ;
        private final static int DATAREAD = 1 ;
        private final static int DATAWIRTE = 2 ;

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case DISCONNECT :
                    if(tagflag) Log.d("Handle" ,"DISCONNECT" );
                    break;
                case CONNECTED :
                    if(tagflag)Log.d("Handle" ,"CONNECTED" );

                    if(msg.arg1 == DATAREAD) {
                        data = (String) msg.obj;
                        if(data == null)return;
                        System.out.println(data);
                        primaevalData.split(data);
                    }

                    //System.out.println(data);
                    if(primaevalData.startcheck()) {
                        if (primaevalData.getId()[2] == 0xCE) {
                            writeCheck.setIDCheck(primaevalData.getdata());
                            checkflag = true;
//                            if (writeCheck.CheckRAMWrite()) {
//                                checkflag = true;
//                            }
//                            if (writeCheck.CheckEEPROMWrite()) {
//                                checkflag = true;
//                            }
                        }

                        if (primaevalData.getId()[2] == 0xCF) {
                            readCheck.setData(primaevalData.getdata());
                            if (readCheck.getAddress().equals("05d")) {
                                int a = (int) readCheck.getValues();
                                String s = String.valueOf(a);
                                int b = (int) (a / 65535.0f * 100);
                                batprotect_sBar1.setProgress(b);
                                batprotect_pn.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load battery_nm:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("05e")) {
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) ((a-65) / 60.0f * 100);
                                batprotect_sBar2.setProgress(b);
                                batprotect_lp.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load battery_lp:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("05f")) {
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) ((a-65) / 60.0f * 100);
                                batprotect_sBar3.setProgress(b);
                                batprotect_lr.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load battery_lr:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("060")) {
                                checkflag = true;
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) ((a-65) / 60.0f * 100);
                                batprotect_sBar4.setProgress(b);
                                batprotect_lw.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load battery_lw:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("061")) {
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) ((a-65) / 60.0f * 100);
                                batprotect_sBar5.setProgress(b);
                                batprotect_hp.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load battery_hp:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("062")) {
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) ((a-65) / 60.0f * 100);
                                batprotect_sBar6.setProgress(b);
                                batprotect_hr.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load battery_hr:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("063")) {
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) ((a-65) / 60.0f * 100);
                                batprotect_sBar7.setProgress(b);
                                batprotect_hw.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load battery_hw:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else {
                                checkflag = false;
                            }
                        }
                    }

                    break;
                case CONNECTING :
                    if(tagflag)Log.d("Handle" ,"CONNECTING" );
                    break;
                case 5:
                    try {
                        dialog.dismiss();
                    }catch (Exception e){
                        dialog.dismiss();
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };
}
