package com.gevsitech.cr_simple;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Jian on 2016/10/6.
 */
public class ValueLineView extends View implements View.OnTouchListener {


    Handler handler ;

    private Bitmap point_a = BitmapFactory.decodeResource(this.getResources(), R.drawable.point_a);
    private Bitmap point_b = BitmapFactory.decodeResource(this.getResources(), R.drawable.point_a);
    private Bitmap point_c = BitmapFactory.decodeResource(this.getResources(), R.drawable.point_a);
    private Bitmap point_d = BitmapFactory.decodeResource(this.getResources(), R.drawable.point_a);
    private Bitmap point_e = BitmapFactory.decodeResource(this.getResources(), R.drawable.point_a);

    float pic_height = point_a.getHeight()/2;
    static float  pic_width;

    Paint backgroundPaint ;
    Paint Line1Paint ;
    Paint Line2Paint ;
    Paint Line3Paint ;
    Paint textPaint ;

    float[] y = { 200 , 200, 200 , 200 , 200 };

    Boolean[] inWidth = {false ,false ,false,false,false};

    private static int screenH;
    private static int screenW;
    private static int screenH_10 = 0;
    private static int screenW_10 = 0;
    private static int screenH_5 = 0;
    private static int screenW_5 = 0;

    public ValueLineView(Context context) {
        super(context);
        init();
    }

    public ValueLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ValueLineView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        backgroundPaint = new Paint();
        backgroundPaint.setColor(Color.BLACK);

        Line1Paint   = new Paint();
        Line1Paint.setColor(Color.WHITE);
        Line1Paint.setStrokeWidth((float) 1.0);

        Line2Paint   = new Paint();
        Line2Paint.setColor(Color.BLUE);
        Line2Paint.setStrokeWidth((float) 5.0);

        Line3Paint   = new Paint();
        Line3Paint.setColor(Color.RED);
        Line3Paint.setStrokeWidth((float) 3.0);

        textPaint = new Paint();
        textPaint.setTextSize(40);
        textPaint.setColor(Color.WHITE);

        setOnTouchListener(this);

    }
    static float a;
    static float b;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

            //繪製背景
            canvas.drawRect(0, 0, getWidth(), getHeight(), backgroundPaint);
            for(int i = 1 ; i<12 ; i++ ){
                canvas.drawLine(0 , (screenH/12) * i,   screenW , (screenH/12) * i, Line1Paint);
            }
            for(int i = 1 ; i<12 ; i++ ){
                canvas.drawLine((screenW/12)* i , 0 ,  (screenW/12) * i ,  screenH, Line1Paint);
            }
            canvas.drawLine(screenW, 0 , screenW,  screenH, Line3Paint);
            canvas.drawLine(0, screenH , screenW,  screenH, Line3Paint);
            a = point_a.getHeight()/2;
            b = point_a.getWidth()/2;
            screenW_5 = (screenW/5);


        if(inWidth[0]){
            if( y[0] <= (screenH/12)-a) {
                y[0] = (screenH/12)-a;
            }else if( y[0] >= (screenH/12)*11 -a){
                y[0] = (screenH/12)*11 -a;
            }
            String S_X1 =String.valueOf (99.0625-100* y[0]/((
                    (screenH/12)*11 -a)-(screenH/12 -a)));
            if( this.handler !=null) {
                this.handler.obtainMessage(3, 1, 0, S_X1 + "%"/*,25%"*/).sendToTarget();
            }

        }else if(inWidth[1]){
            if( y[1] <= (screenH/12)-a) {
                y[1] = (screenH/12)-a;
            }else if( y[1] >= (screenH/12)*11 -a){
                y[1] = (screenH/12)*11 -a;
            }
            String S_X2 =String.valueOf (99.0625-100* y[1]/((
                    (screenH/12)*11 -a)-(screenH/12 -a)));
            if( this.handler !=null) {
                this.handler.obtainMessage(3, 2, 0, S_X2 + "%"/*,25%"*/).sendToTarget();
            }
        }
        else if(inWidth[2]){
            if( y[2] <= (screenH/12)-a) {
                y[2] = (screenH/12)-a;
            }else if( y[2] >= (screenH/12)*11 -a){
                y[2] = (screenH/12)*11 -a;
            }
            String S_X3 =String.valueOf (99.0625-100* y[2]/((
                    (screenH/12)*11 -a)-(screenH/12 -a)));
            if( this.handler !=null) {
                this.handler.obtainMessage(3, 3, 0, S_X3 + "%"/*,25%"*/).sendToTarget();
            }
        }
        else if(inWidth[3]){
            if( y[3] <= (screenH/12)-a) {
                y[3] = (screenH/12)-a;
            }else if( y[3] >= (screenH/12)*11 -a){
                y[3] = (screenH/12)*11 -a;
            }
            String S_X4 =String.valueOf (99.0625-100* y[3]/((
                    (screenH/12)*11 -a)-(screenH/12 -a)));
            if( this.handler !=null) {
                this.handler.obtainMessage(3, 4, 0, S_X4 + "%"/*,25%"*/).sendToTarget();
            }
        }
        else if(inWidth[4]){
            if( y[4] <= (screenH/12)-a) {
                y[4] = (screenH/12)-a;
            }else if( y[4] >= (screenH/12)*11 -a){
                y[4] = (screenH/12)*11 -a;
            }

            String S_X5 =String.valueOf (99.0625-100* y[4]/(((screenH/12)*11 -a)-(screenH/12 -a)));
            if( this.handler !=null) {
                this.handler.obtainMessage(3, 5, 0, S_X5 + "%"/*,25%"*/).sendToTarget();
            }
        }

        canvas.drawLine((screenW_5 * 0 + b) , y[0]+a , screenW_5 * 1 + b ,y[1]+a , Line2Paint);
        canvas.drawLine((screenW_5 * 1 + b) , y[1]+a , screenW_5 * 2 + b ,y[2]+a , Line2Paint);
        canvas.drawLine((screenW_5 * 2 + b) , y[2]+a , screenW_5 * 3 + b ,y[3]+a , Line2Paint);
        canvas.drawLine((screenW_5 * 3 + b) , y[3]+a , screenW_5 * 4 + b ,y[4]+a , Line2Paint);

        canvas.drawBitmap(point_a , screenW_5 * 0  , y[0] , backgroundPaint);
        canvas.drawBitmap(point_a , screenW_5 * 1  , y[1] , backgroundPaint);
        canvas.drawBitmap(point_a , screenW_5 * 2 ,  y[2] , backgroundPaint);
        canvas.drawBitmap(point_a , screenW_5 * 3 ,  y[3] , backgroundPaint);
        canvas.drawBitmap(point_a , screenW_5 * 4 ,  y[4] , backgroundPaint);

    }



    @Override
    public boolean onTouch(View v, MotionEvent event) {


        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                float Mx = event.getX();
                float My = event.getY();

                if((Mx >screenW_5 * 0 ) && (Mx < screenW_5 * 0 + pic_width)) {
                    inWidth[0] = true;
                }else {
                    inWidth[0] = false;
                }

                if((Mx >screenW_5 * 1 ) && (Mx < screenW_5 * 1 + pic_width)) {
                    inWidth[1] = true;
                }else {
                    inWidth[1] = false;
                }

                if((Mx >screenW_5 * 2 ) && (Mx < screenW_5 * 2 + pic_width)) {
                    inWidth[2] = true;
                }else {
                    inWidth[2] = false;
                }


                if((Mx >screenW_5 * 3 ) && (Mx < screenW_5 * 3 + pic_width)) {
                    inWidth[3] = true;
                }else {
                    inWidth[3] = false;
                }

                if((Mx >screenW_5 * 4 ) && (Mx < screenW_5 * 4 + pic_width)) {
                    inWidth[4] = true;
                }else {
                    inWidth[4] = false;
                }


                break;

            case MotionEvent.ACTION_MOVE:
                if(inWidth[0]) {
                    y[0] =  event.getY();
                }
                if(inWidth[1]) {
                    y[1] =  event.getY();
                }
                if(inWidth[2]) {
                    y[2] =  event.getY();
                }
                if(inWidth[3]) {
                    y[3] =  event.getY();
                }
                if(inWidth[4]) {
                    y[4] =  event.getY();
                }
                break;
            case MotionEvent.ACTION_UP:
                break;
        }
        invalidate();
        return true;

    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        screenW = MeasureSpec.getSize(widthMeasureSpec);
        screenH = MeasureSpec.getSize(heightMeasureSpec);

        float parmater = 0.10f;

        point_a = Bitmap.createScaledBitmap(point_a
                , (int) (screenW*parmater)
                , (int) (screenW*parmater)
                , true
        );
        point_b = Bitmap.createScaledBitmap(point_b
                , (int) (screenW*parmater)
                , (int) (screenW*parmater)
                , true
        );
        point_c = Bitmap.createScaledBitmap(point_c
                , (int) (screenW*parmater)
                , (int) (screenW*parmater)
                , true
        );
        point_d = Bitmap.createScaledBitmap(point_d
                , (int) (screenW*parmater)
                , (int) (screenW*parmater)
                , true
        );
        point_e = Bitmap.createScaledBitmap(point_e
                , (int) (screenW*parmater)
                , (int) (screenW*parmater)
                , true
        );

        pic_width = point_a.getWidth();

    }



    public void sethandle(Handler handler){
        this.handler = handler;
    }

}
