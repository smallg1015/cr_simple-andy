package com.gevsitech.cr_simple;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Toast;

import com.gevsitech.cr_simple.data_separation.PrimaevalData;
import com.gevsitech.cr_simple.data_separation.ReadCheck;
import com.gevsitech.cr_simple.data_separation.SettingCheck;
import com.gevsitech.cr_simple.data_separation.WriteCheck;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Protection_mos extends AppCompatActivity {
    private EditText mosprotect_nm,mosprotect_lp,mosprotect_lr,mosprotect_hp,mosprotect_hr,mosprotect_lw,mosprotect_hw;
    private SeekBar mosprotect_sBar1,mosprotect_sBar2,mosprotect_sBar3,mosprotect_sBar4,mosprotect_sBar5,mosprotect_sBar6,mosprotect_sBar7;
    private Button mosprotect_btnsure,mosprotect_btnnext,mosprotect_btn1,mosprotect_btn2,mosprotect_btn3,mosprotect_btn4,mosprotect_btn5,mosprotect_btn6,mosprotect_btn7,mosprotect_btnsetting;
    private String nm,lp,lr,hp,hr,lw,hw;
    private String[] hex=new String[]{"86,00","87,00","88,00","8A,00","8B,00","8C,00","89,00"};
    private Boolean Sendflag = true;
    private volatile Boolean checkflag = true;
    private ProgressDialog dialog;
    private int SendCount = 1 ;
    private PrimaevalData primaevalData = new PrimaevalData();
    private ReadCheck readCheck  = new ReadCheck();
    private WriteCheck writeCheck = new WriteCheck();
    private SettingCheck settingCheck = new SettingCheck();
    private Boolean changenumber=true;
    private int save_nm;
    private float save_lp,save_lr,save_hp,save_hr,save_hw,save_lw;
    private File LogPath;
    private ImageButton mosbacktoset;

    String logFileName;
    LogService logService = LogService.getLogService();
    private String downFilename;

    BluetoothService BluetoothS = BluetoothService.getBluetoothService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_protection_mos);
        init();

        LogPath = this.getExternalFilesDir(null);
        //取得今日日期
        Calendar mCal = Calendar.getInstance();
        CharSequence ss = DateFormat.format("yyyy-MM-dd", mCal.getTime());    // kk:24小時制, hh:12小時制
        logFileName = String.valueOf(ss)+".txt";

        logService.SaveLog(LogPath, logFileName, "START Protection_mos");




        BluetoothS.setBtHandler(BTPoccess);
        if(BluetoothS.isConnectedCheck()){

            dialog = ProgressDialog.show( Protection_mos.this,
                    "讀取初始資料中", "請稍待片刻...",true);
            SendCount = 1 ;
            Sendflag  = true;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(Sendflag){
                        try {
                            Thread.sleep(200);
                        }catch (Exception e){

                        }
                        switch (SendCount) {

                            case 1 :
                                //寫入1
                                char[] s1 = settingCheck.Send("006F");
                                if(checkflag){
                                    BluetoothS.write2(s1);
                                    SendCount = 2 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入25%");
                                }
                                break;
                            case 2 :
                                char[] s2 = settingCheck.Send("0070");
                                if(checkflag){
                                    BluetoothS.write2(s2);
                                    SendCount = 3 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入50%");
                                }
                                break;
                            case 3 :
                                char[] s3 = settingCheck.Send("0071");
                                if(checkflag){
                                    BluetoothS.write2(s3);
                                    SendCount = 4 ;
                                    checkflag = false;
                                    // dialog.setMessage("寫入75%");
                                }
                                break;
                            case 4 :
                                char[] s4 = settingCheck.Send("0072");
                                if(checkflag){
                                    BluetoothS.write2(s4);
                                    SendCount = 5 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入75%");
                                }
                                break;
                            case 5 :
                                char[] s5 = settingCheck.Send("0073");
                                if(checkflag){
                                    BluetoothS.write2(s5);
                                    SendCount = 6 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 6 :
                                char[] s6 = settingCheck.Send("0074");
                                if(checkflag){
                                    BluetoothS.write2(s6);
                                    SendCount = 7 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 7 :
                                char[] s7 = settingCheck.Send("0075");
                                if(checkflag){
                                    BluetoothS.write2(s7);
                                    SendCount = 0 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 0 :
                                if(checkflag) {
                                    Sendflag = false;
                                    break;
                                }
                        }

                    }
                    BTPoccess.obtainMessage(5).sendToTarget();

                }
            });
            thread.start();






        }else{
            Toast.makeText(Protection_mos.this,"藍芽尚未連線，無法讀取資料",Toast.LENGTH_SHORT).show();
            mosprotect_btn1.setEnabled(false);
            mosprotect_btn2.setEnabled(false);
            mosprotect_btn3.setEnabled(false);
            mosprotect_btn4.setEnabled(false);
            mosprotect_btn5.setEnabled(false);
            mosprotect_btn6.setEnabled(false);
            mosprotect_btn7.setEnabled(false);
            mosprotect_btnsetting.setEnabled(false);
            logService.SaveLog(LogPath, logFileName, "bluetooth isn't connect");
        }


        mosbacktoset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mosprotect_nm.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(mosprotect_nm.getText().toString().length() > 0) {
                        int a =(int)(Integer.parseInt(mosprotect_nm.getText().toString())/65535.0f*100);
                        save_nm=Integer.parseInt(mosprotect_nm.getText().toString());
                        changenumber=false;
                        mosprotect_sBar1.setProgress(a);
                    }
                }
                return false;
            }
        });

        mosprotect_lp.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(mosprotect_lp.getText().toString().length() > 0) {
                        int a=(int)((Float.valueOf(mosprotect_lp.getText().toString())+45)/65.0f*100);
                        save_lp=Float.valueOf(mosprotect_lp.getText().toString());
                        changenumber=false;
                        mosprotect_sBar2.setProgress(a);
                        System.out.println(a);
                    }
                }
                return false;
            }
        });
        mosprotect_lr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(mosprotect_lr.getText().toString().length() > 0) {
                        int a=(int)((Float.valueOf(mosprotect_lr.getText().toString())+45)/65.0f*100);
                        save_lr=Float.valueOf(mosprotect_lr.getText().toString());
                        changenumber=false;
                        mosprotect_sBar3.setProgress(a);
                    }
                }
                return false;
            }
        });
        mosprotect_lw.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(mosprotect_lw.getText().toString().length() > 0) {
                        int a=(int)((Float.valueOf(mosprotect_lw.getText().toString())+45)/65.0f*100);
                        save_lw=Float.valueOf(mosprotect_lw.getText().toString());
                        changenumber=false;
                        mosprotect_sBar4.setProgress(a);
                    }
                }
                return false;
            }
        });
        mosprotect_hp.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(mosprotect_hp.getText().toString().length() > 0) {
                        int a=(int)(Float.valueOf(mosprotect_hp.getText().toString())/75.0f*100);
                        save_hp=Float.valueOf(mosprotect_hp.getText().toString());
                        changenumber=false;
                        mosprotect_sBar5.setProgress(a);
                    }
                }
                return false;
            }
        });
        mosprotect_hr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(mosprotect_hr.getText().toString().length() > 0) {
                        int a=(int)(Float.valueOf(mosprotect_hr.getText().toString())/75.0f*100);
                        save_hr=Float.valueOf(mosprotect_hr.getText().toString());
                        changenumber=false;
                        mosprotect_sBar6.setProgress(a);
                    }
                }
                return false;
            }
        });
        mosprotect_hw.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(mosprotect_hw.getText().toString().length() > 0) {
                        int a=(int)((Float.valueOf(mosprotect_hw.getText().toString()))/75.0f*100);
                        save_hw=Float.valueOf(mosprotect_hw.getText().toString());
                        changenumber=false;
                        mosprotect_sBar7.setProgress(a);
                    }
                }

                return false;
            }
        });


        mosprotect_sBar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int a=(int)(progress/100.0f*65535);
                System.out.println(a);
                if(changenumber) {
                    mosprotect_nm.setText(a + "");
                }else {
                    mosprotect_nm.setText(save_nm + "");
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mosprotect_sBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(mosprotect_sBar2.getProgress()/100.0f*65.0f-45);
                if(changenumber) {
                    mosprotect_lp.setText(String.format("%.1f",a));
                }else {
                    mosprotect_lp.setText(save_lp + "");
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mosprotect_sBar3.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(mosprotect_sBar3.getProgress()/100.0f*65-45);
                if(changenumber) {
                    mosprotect_lr.setText(String.format("%.1f",a));
                }else {
                    mosprotect_lr.setText(save_lr + "");
                    changenumber=true;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mosprotect_sBar4.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(mosprotect_sBar4.getProgress()/100.0f*65-45);
                if(changenumber) {
                    mosprotect_lw.setText(String.format("%.1f",a));
                }else {
                    mosprotect_lw.setText(save_lw + "");
                    changenumber=true;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mosprotect_sBar5.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(mosprotect_sBar5.getProgress()/100.0f*75);
                if(changenumber) {
                    mosprotect_hp.setText(String.format("%.1f",a));
                }else {
                    mosprotect_hp.setText(save_hp + "");
                    changenumber=true;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mosprotect_sBar6.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(mosprotect_sBar6.getProgress()/100.0f*75);
                if(changenumber) {
                    mosprotect_hr.setText(String.format("%.1f",a));
                }else {
                    mosprotect_hr.setText(save_hr + "");
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mosprotect_sBar7.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(mosprotect_sBar7.getProgress()/100.0f*75);
                if(changenumber) {
                    mosprotect_hw.setText(String.format("%.1f",a));
                }else {
                    mosprotect_hw.setText(save_hw + "");
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mosprotect_btn1.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_mos.this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s1 =settingCheck.SendWithData("006F" ,(short)(Integer.valueOf(mosprotect_nm.getText().toString())*1));

                                    if(checkflag){
                                        BluetoothS.write2(s1);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "mos保護計數值:"+mosprotect_nm.getText().toString());

            }



        });
        mosprotect_btn2.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialog = ProgressDialog.show(Protection_mos.this,
                        "寫入資料中", "請稍後...", true);
                SendCount = 1;
                Sendflag = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (Sendflag) {

                            switch (SendCount) {

                                case 1:
                                    char[] s1 = settingCheck.SendWithData("0070", (short)((Float.valueOf(mosprotect_lp.getText().toString()))* 10));
                                    System.out.println("short="+(short)((Float.valueOf(mosprotect_lp.getText().toString()))* 10));

                                    if (checkflag) {
                                        BluetoothS.write2(s1);
                                        SendCount = 0;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0:
                                    if (checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        } catch (Exception e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "mos低溫保護點:"+mosprotect_lp.getText().toString());


            }
        });
        mosprotect_btn3.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_mos.this,
                    "寫入資料中", "請稍後...", true);
                SendCount = 1;
                Sendflag = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (Sendflag) {

                            switch (SendCount) {

                                case 1:
                                    char[] s1 = settingCheck.SendWithData("0071", (short)((Float.valueOf(mosprotect_lr.getText().toString())) * 10));

                                    if (checkflag) {
                                        BluetoothS.write2(s1);
                                        SendCount = 0;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0:
                                    if (checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        } catch (Exception e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "mos低溫回復點:"+mosprotect_lr.getText().toString());

            }
        });
        mosprotect_btn4.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_mos.this,
                        "寫入資料中", "請稍後...", true);
                SendCount = 1;
                Sendflag = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (Sendflag) {

                            switch (SendCount) {

                                case 1:
                                    char[] s1 = settingCheck.SendWithData("0072", (short)(Float.valueOf(mosprotect_lw.getText().toString()) * 10));

                                    if (checkflag) {
                                        BluetoothS.write2(s1);
                                        SendCount = 0;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0:
                                    if (checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        } catch (Exception e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "mos低溫警告點:"+mosprotect_lw.getText().toString());

            }

        });
        mosprotect_btn5.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_mos.this,
                        "寫入資料中", "請稍後...", true);
                SendCount = 1;
                Sendflag = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (Sendflag) {

                            switch (SendCount) {

                                case 1:
                                    char[] s1 = settingCheck.SendWithData("0073", (short)(Float.valueOf(mosprotect_hp.getText().toString()) * 10));

                                    if (checkflag) {
                                        BluetoothS.write2(s1);
                                        SendCount = 0;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0:
                                    if (checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        } catch (Exception e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "mos高溫保護點:"+mosprotect_hp.getText().toString());

            }
        });
        mosprotect_btn6.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_mos.this,
                        "寫入資料中", "請稍後...", true);
                SendCount = 1;
                Sendflag = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (Sendflag) {

                            switch (SendCount) {

                                case 1:
                                    char[] s1 = settingCheck.SendWithData("0074", (short)(Float.valueOf(mosprotect_hr.getText().toString()) * 10));

                                    if (checkflag) {
                                        BluetoothS.write2(s1);
                                        SendCount = 0;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0:
                                    if (checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        } catch (Exception e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "mos高溫回復點:"+mosprotect_hr.getText().toString());

            }

        });
        mosprotect_btn7.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_mos.this,
                        "寫入資料中", "請稍後...", true);
                SendCount = 1;
                Sendflag = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (Sendflag) {

                            switch (SendCount) {

                                case 1:
                                    char[] s1 = settingCheck.SendWithData("0075", (short)((Float.valueOf(mosprotect_hw.getText().toString())) * 10));

                                    if (checkflag) {
                                        BluetoothS.write2(s1);
                                        SendCount = 0;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0:
                                    if (checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        } catch (Exception e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "mos高溫警告點:"+mosprotect_hw.getText().toString());

            }

        });

        mosprotect_btnsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = ProgressDialog.show(Protection_mos.this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s1_e=settingCheck.SendEEPROMData("006F");

                                    if(checkflag){
                                        BluetoothS.write2(s1_e);
                                        SendCount = 2 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 2 :
                                    char[] s2_e=settingCheck.SendEEPROMData("0070");
                                    if(checkflag){
                                        BluetoothS.write2(s2_e);
                                        SendCount = 3 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 3 :
                                    char[] s3_e=settingCheck.SendEEPROMData("0071");
                                    if(checkflag){
                                        BluetoothS.write2(s3_e);
                                        SendCount = 4 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 4 :
                                    char[] s4_e=settingCheck.SendEEPROMData("0072");
                                    if(checkflag){
                                        BluetoothS.write2(s4_e);
                                        SendCount = 5 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 5 :
                                    char[] s5_e=settingCheck.SendEEPROMData("0073");
                                    if(checkflag){
                                        BluetoothS.write2(s5_e);
                                        SendCount = 6 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 6 :
                                    char[] s6_e=settingCheck.SendEEPROMData("0074");
                                    if(checkflag){
                                        BluetoothS.write2(s6_e);
                                        SendCount = 7 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 7 :
                                    char[] s7_e=settingCheck.SendEEPROMData("0075");
                                    if(checkflag){
                                        BluetoothS.write2(s7_e);
                                        SendCount = 0 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                    }
                                    break;

                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();

            }
        });




    }
    private void init(){
        mosprotect_nm=(EditText)findViewById(R.id.mosprotect_nm);
        mosprotect_lp=(EditText)findViewById(R.id.mosprotect_lp);
        mosprotect_lr=(EditText)findViewById(R.id.mosprotect_lr);
        mosprotect_hp=(EditText)findViewById(R.id.mosprotect_hp);
        mosprotect_hr=(EditText)findViewById(R.id.mosprotect_hr);
        mosprotect_lw=(EditText)findViewById(R.id.mosprotect_lw);
        mosprotect_hw=(EditText)findViewById(R.id.mosprotect_hw);
        mosprotect_sBar1=(SeekBar) findViewById(R.id.mosprotect_sBar1);
        mosprotect_sBar2=(SeekBar) findViewById(R.id.mosprotect_sBar2);
        mosprotect_sBar3=(SeekBar) findViewById(R.id.mosprotect_sBar3);
        mosprotect_sBar4=(SeekBar) findViewById(R.id.mosprotect_sBar4);
        mosprotect_sBar5=(SeekBar) findViewById(R.id.mosprotect_sBar5);
        mosprotect_sBar6=(SeekBar) findViewById(R.id.mosprotect_sBar6);
        mosprotect_sBar7=(SeekBar) findViewById(R.id.mosprotect_sBar7);


        mosprotect_btn1=(Button) findViewById(R.id.mosprotect_btn1);
        mosprotect_btn2=(Button) findViewById(R.id.mosprotect_btn2);
        mosprotect_btn3=(Button) findViewById(R.id.mosprotect_btn3);
        mosprotect_btn4=(Button) findViewById(R.id.mosprotect_btn4);
        mosprotect_btn5=(Button) findViewById(R.id.mosprotect_btn5);
        mosprotect_btn6=(Button) findViewById(R.id.mosprotect_btn6);
        mosprotect_btn7=(Button) findViewById(R.id.mosprotect_btn7);
        mosprotect_btnsetting=(Button) findViewById(R.id.mosprotect_btnsetting);
        mosbacktoset=(ImageButton)findViewById(R.id.mosbacktoset);

    }
    private Handler BTPoccess = new Handler(){
        private StringPoccess poccess = new StringPoccess();

        private String data ;

        private boolean tagflag = false;

        /**
         *  連線中狀態值
         *  1. DISCONNECT 斷線狀態    0
         *  2. CONNECTED  連線中      1
         *  3. CONNECTING 嘗試連線中  2
         **/
        private final static int DISCONNECT = 0 ;
        private final static int CONNECTED = 1 ;
        private final static int CONNECTING = 2 ;

        /**
         *  連線中狀態值
         *  1. DATAWAIT   等待資料中    0
         *  2. DATAREAD   資料讀取      1
         *  3. DATAWIRTE  資料寫入      2
         **/

        private final static int DATAWAIT = 0 ;
        private final static int DATAREAD = 1 ;
        private final static int DATAWIRTE = 2 ;

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case DISCONNECT :
                    if(tagflag) Log.d("Handle" ,"DISCONNECT" );
                    break;
                case CONNECTED :
                    if(tagflag)Log.d("Handle" ,"CONNECTED" );

                    if(msg.arg1 == DATAREAD) {
                        System.out.println("Protection_mos here");
                        data = (String) msg.obj;
                        if(data == null)return;
                        System.out.println(data);
                        primaevalData.split(data);
                    }

                    //System.out.println(data);
                    System.out.println(primaevalData.startcheck());
                    if(primaevalData.startcheck()) {
                        if (primaevalData.getId()[2] == 0xCE) {
                            checkflag = true;
                            writeCheck.setIDCheck(primaevalData.getdata());
                            if (writeCheck.CheckRAMWrite()) {
                                checkflag = true;
                            }if (writeCheck.CheckEEPROMWrite()) {
                                checkflag = true;
                            }
                        }

                        if (primaevalData.getId()[2]==0xCF) {
                            readCheck.setData(primaevalData.getdata());
                            if (readCheck.getAddress().equals("06f")) {
                                int a = (int) readCheck.getValues();
                                String s = String.valueOf(a);
                                int b = (int) (a / 65535.0f * 100);
                                mosprotect_sBar1.setProgress(b);
                                mosprotect_nm.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load mos_nm:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("070")) {
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) ((a + 45) / 65.f*100);
                                mosprotect_sBar2.setProgress(b);
                                mosprotect_lp.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load mos_lp:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("071")) {
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) ((a + 45) / 65.f*100);
                                mosprotect_sBar3.setProgress(b);
                                mosprotect_lr.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load mos_lr:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("072")) {
                                checkflag = true;
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) ((a + 45) / 65.f*100);
                                mosprotect_sBar4.setProgress(b);
                                mosprotect_lw.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load mos_lw:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("073")) {
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) (a / 75.0f*100);
                                mosprotect_sBar5.setProgress(b);
                                mosprotect_hp.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load mos_hp:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("074")) {
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) (a / 75.0f*100);
                                mosprotect_sBar6.setProgress(b);
                                mosprotect_hr.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load mos_hr:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("075")) {
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) (a / 75.0f*100);
                                mosprotect_sBar7.setProgress(b);
                                mosprotect_hw.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load mos_hw:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else {
                                checkflag = false;
                            }
                        }
                    }

                    break;
                case CONNECTING :
                    if(tagflag)Log.d("Handle" ,"CONNECTING" );
                    break;
                case 5:
                    try {
                        dialog.dismiss();
                    }catch (Exception e){
                        dialog.dismiss();
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };

}
