package com.gevsitech.cr_simple;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gevsitech.cr_simple.data_separation.C0Separation;
import com.gevsitech.cr_simple.data_separation.C1Separation;
import com.gevsitech.cr_simple.data_separation.C2separation;
import com.gevsitech.cr_simple.data_separation.C3Separarion;
import com.gevsitech.cr_simple.data_separation.C5Separation;
import com.gevsitech.cr_simple.data_separation.DC2Separation;
import com.gevsitech.cr_simple.data_separation.DM1Separation;
import com.gevsitech.cr_simple.data_separation.GPSSeparation;
import com.gevsitech.cr_simple.data_separation.PrimaevalData;
import com.kyleduo.switchbutton.SwitchButton;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Formatter;
import java.util.List;

public class

DriverStateActivity extends Fragment {

    PrimaevalData primaevalData = new PrimaevalData();
    DC2Separation dc2Separation = new DC2Separation();
    DM1Separation dm1Separation = new DM1Separation();
    GPSSeparation gpsSeparation = new GPSSeparation();
    C0Separation c0Separation=new C0Separation();
    C1Separation c1Separation=new C1Separation();
    C2separation c2separation=new C2separation();
    C3Separarion c3Separarion=new C3Separarion();
    C5Separation c5Separation=new C5Separation();

    private ProgressBar VKey_P;
    private ProgressBar Battery_Voltage_P;
    private ProgressBar Battery_Current_P;
    private ProgressBar MOS_Temp1_P;
    private ProgressBar MOS_Tempe2_P;
    private ProgressBar ACC1_Voltage_P;
    private ProgressBar ACC2_Voltage_P;
    private ProgressBar Brake_Voltage_P;
    private ProgressBar Motor_Speed_P;
    private ProgressBar Motor_current_V_P ;
    private ProgressBar Motor_current_U_P;
    private ProgressBar Motor_Temperature_1_P;
    private ProgressBar Motor_Temperature_2_P;
    private ProgressBar Motor_Voltage_U_P;
    private ProgressBar Motor_Voltage_V_P;
    private ProgressBar Motor_Voltage_W_P;

    private TextView Motor_Speed_V;
    private TextView Motor_current_U_V;
    private TextView Motor_current_V_V;
    private TextView Motor_Temperature_1_V ;
    private TextView Motor_Temperature_2_V;
    private TextView Motor_Voltage_U_V;
    private TextView Motor_Voltage_V_V;
    private TextView Motor_Voltage_W_V;
    private TextView VKey_V;
    private TextView Battery_Voltage_V;
    private TextView Battery_Current_V;
    private TextView MOS_Temp1_V;
    private TextView MOS_Tempe2_V;
    private TextView ACC1_Voltage_V;
    private TextView ACC2_Voltage_V;
    private TextView Brake_Voltage_V;
    private TextView ratingBar1;
    private TextView ratingBar2;

    BluetoothService bluetoothService = BluetoothService.getBluetoothService();

    //list_drivesrare之相關參數
    private ListView list_drivestate,list_drivestateopen1,list_drivestateopen2;
    private String[] drivestate_number=new String[16];
    private String[] drivestate_unit={"V","V","A","℃","℃","V"/*,"V","V"*/,"RPM","A","V","℃",/*"℃","V","V","V",*/"",""};
    private int[] drivestate_progress=new int[16];
    private String[] arrayList={"Key電壓","電池電壓","電池電流","控制器溫度1","控制器溫度2","油門位準"/*,"油門2","剎車"*/,"馬達轉速","馬達電流U","馬達電流V","馬達溫度",/*"馬達溫度2","馬達電壓U","馬達電壓V","馬達電壓W",*/"繼電器狀態","開關狀態"};
    private String[] open1list={"繼電器狀態"};
    private String[] open2list={"開關狀態"};

    //list_drivestateex相關參數
    private ListView list_drivestateex;


    //活動紀錄
    LogService logService = LogService.getLogService();
    private File LogPath;
    String logFileName;
    String SaveFileName;
    private static boolean data_Tran_OK;

    private Myadapter adapter;



    private boolean boo_state1,boo_start,boo_back;






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_driver_state, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bluetoothService.setBtHandler(BTPoccess);

        //設定listdrivestate
        list_drivestate=(ListView)getView().findViewById(R.id.list_drivestate);








        Myadapter adapter=new Myadapter(getActivity());
        list_drivestate.setAdapter(adapter);



        //活動紀錄
        LogPath = new File(Environment.getExternalStorageDirectory().getPath()+"/CR_simple");
        //取得今日日期
        Calendar mCal = Calendar.getInstance();
        CharSequence ss = DateFormat.format("yyyy-MM-dd", mCal.getTime());    // kk:24小時制, hh:12小時制
        logFileName = String.valueOf(ss)+".txt";
        SaveFileName = String.valueOf(ss)+"saveData"+".txt";

        logService.SaveLog(LogPath, logFileName, "START DriveState");

        if(bluetoothService.isConnectedCheck()){

        }else{
            Toast.makeText(getActivity(),"藍芽尚未連線，無法讀取資料",Toast.LENGTH_SHORT).show();
        }








    }


    public class Myadapter extends BaseAdapter{
        final int TYPE_1=0;
        final int TYPE_2=1;
        final int TYPE_3=2;
        private LayoutInflater Inflater;
        public Myadapter(Context c){
            Inflater=LayoutInflater.from(c);
        }
        @Override
        public int getCount(){
            return arrayList.length;
        }

        @Override
        public Object getItem(int position){
            return arrayList[position];
        }
        @Override
        public long getItemId(int position){
            return position;
        }
        @Override
        public int getItemViewType(int position){
            int p=position;
            if(p<10){
                return TYPE_1;
            }
            else if(p<11){return TYPE_2;}
            else{return TYPE_3;}
        }
        @Override
        public int getViewTypeCount(){
            return 3;
        }
        @Override
        public View getView(int position,View convertView,ViewGroup parent){
            int type=getItemViewType(position);
            ViewHolder holder1=null;
            ViewHolder2 holder2=null;
            ViewHolder3 holder3=null;
            //當ListView被拖拉時會不斷觸發getView，為了避免重複加載必須加上這個判斷
            if(convertView==null){
                switch(type){
                    case TYPE_1:
                        holder1=new ViewHolder();
                        convertView=Inflater.inflate(R.layout.drivestate,null);
                        holder1.driItem=(TextView)convertView.findViewById(R.id.drivestate_item);
                        holder1.driBar=(ProgressBar)convertView.findViewById(R.id.drivestate_bar);
                        holder1.driNum=(TextView)convertView.findViewById(R.id.drivestate_number);
                        holder1.driUnit=(TextView)convertView.findViewById(R.id.drivestate_V);

                        holder1.driItem.setText(arrayList[position]);
                        if(drivestate_progress!=null) {
                            holder1.driBar.setProgress(drivestate_progress[position]);
                            holder1.driNum.setText(drivestate_number[position]);
                        }
                        holder1.driUnit.setText(drivestate_unit[position]);

                        convertView.setTag(holder1);
                        break;



                    case TYPE_2:
                        holder2=new ViewHolder2();
                        convertView=Inflater.inflate(R.layout.drivestate_open1,null);
                        holder2.driItem=(TextView)convertView.findViewById(R.id.drivestateopen1_item);
                        holder2.swh_state1=(SwitchButton)convertView.findViewById(R.id.swh_state1);
                        holder2.swh_state1.setClickable(false);
                        holder2.swh_state1.setChecked(boo_state1);
                        holder2.driItem.setText(arrayList[position]);
                        convertView.setTag(holder2);
                        break;

                    case TYPE_3:
                        holder3=new ViewHolder3();
                        convertView=Inflater.inflate(R.layout.drivestate_open2,null);
                        holder3.driItem=(TextView)convertView.findViewById(R.id.drivestateopen2_item);
                        holder3.swh_startopen=(SwitchButton)convertView.findViewById(R.id.swh_openstart);
                        holder3.swh_backopen=(SwitchButton)convertView.findViewById(R.id.swh_backopen);
                        holder3.swh_startopen.setClickable(false);
                        holder3.swh_backopen.setClickable(false);
                        holder3.swh_startopen.setChecked(boo_start);
                        holder3.swh_backopen.setChecked(boo_back);

                        holder3.driItem.setText(arrayList[position]);
                        convertView.setTag(holder3);
                        break;
                }
            }
            else{
                switch(type) {
                    case TYPE_1:
                        holder1 = (ViewHolder) convertView.getTag();
                        holder1.driItem.setText(arrayList[position]);
                        if (drivestate_progress != null) {
                            holder1.driBar.setProgress(drivestate_progress[position]);
                            holder1.driNum.setText(drivestate_number[position]);
                        }
                        holder1.driUnit.setText(drivestate_unit[position]);

                        break;
                    case TYPE_2:
                        holder2 = (ViewHolder2) convertView.getTag();
                        holder2.swh_state1.setClickable(false);
                        holder2.swh_state1.setChecked(boo_state1);

                        break;

                    case TYPE_3:
                        holder3 = (ViewHolder3) convertView.getTag();
                        holder3.swh_startopen.setClickable(false);
                        holder3.swh_backopen.setClickable(false);
                        holder3.swh_startopen.setChecked(boo_start);
                        holder3.swh_backopen.setChecked(boo_back);
                        break;
                }
            }
            return  convertView;

        }
    }

    private static class ViewHolder{
        private TextView driItem;
        private ProgressBar driBar;
        private TextView driNum;
        private TextView driUnit;
    }

    private static class ViewHolder2{
        private TextView driItem;
        private SwitchButton swh_state1;
    }

    private static class ViewHolder3{
        private TextView driItem;
        private SwitchButton swh_startopen;
        private SwitchButton swh_backopen;
    }










    /***
     * 控件與按鈕初始化
     */


    /***
     * 藍芽與螢幕交互處裡用
     */
    private Handler BTPoccess = new Handler(){
        private StringPoccess poccess = new StringPoccess();;
        private boolean tagflag = false;
        private boolean Aflag = false;

        String  data ;
        String  DC1 ;
        String  DC2 ;
        String  C0,C1,C2,C3,C4,C5;
        String  DM1 ;
        String  GPS ;
        /**
         *  連線中狀態值
         *  1. DISCONNECT 斷線狀態    0
         *  2. CONNECTED  連線中      1
         *  3. CONNECTING 嘗試連線中  2
         **/
        private final static int DISCONNECT = 0 ;
        private final static int CONNECTED = 1 ;
        private final static int CONNECTING = 2 ;

        /**
         *  連線中狀態值
         *  1. DATAWAIT   等待資料中    0
         *  2. DATAREAD   資料讀取      1
         *  3. DATAWIRTE  資料寫入      2
         **/

        private final static int DATAWAIT = 0 ;
        private final static int DATAREAD = 1 ;
        private final static int DATAWIRTE = 2 ;
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case DISCONNECT :
                    if(tagflag) Log.d("Handle" ,"DISCONNECT" );
                    break;
                case CONNECTED :
                    if(tagflag)Log.d("Handle" ,"CONNECTED" );
                    if(msg.arg1 == DATAREAD) {
                        data = (String) msg.obj;
                        if(data == null)return;
                        primaevalData.split(data);
                        System.out.println(primaevalData.getId()[2]);
                        //分解DC2資料
                    }

                    if(primaevalData.getId()[2]==0xC0){
                        char[] _C0=primaevalData.getdata();
                        C0 =primaevalData.getDataLog();
                        c0Separation.setC0(_C0);
                        System.out.println("c0="+c0Separation.getRelay().substring(2,3));
                        System.out.println("c1="+c0Separation.getDigitalinputStatus().substring(6,7));
                        System.out.println("c2="+c0Separation.getDigitalinputStatus().substring(5,6));


                        if(c0Separation.getRelay().substring(2,3).equals("1")){
                           boo_state1=true;
                        }else{
                            boo_state1=false;
                        }
                        if(c0Separation.getDigitalinputStatus().substring(6,7).equals("1")){
                            boo_back=true;
                        }else{
                            boo_back=false;
                        }
                        if(c0Separation.getDigitalinputStatus().substring(5,6).equals("1")){

                            boo_start=true;
                        }else{
                            boo_start=false;
                        }

                        list_drivestate.invalidateViews();



                    }
                    if(primaevalData.getId()[2]==0xC1) {
                        System.out.println("yes1");
                        char[] _C1 = primaevalData.getdata();
                        C1 = primaevalData.getDataLog();
                        c1Separation.setC1(_C1);


                        //數值陣列區
                        drivestate_number[0] = String.valueOf((c1Separation.getBatteryVoltage())/10.f);//Vkey電壓
                        drivestate_number[6]=String.valueOf(c1Separation.getMotorSpeed());//MotorSpeeda馬達轉速
                        drivestate_number[7]=String.valueOf(c1Separation.getACoutputcurrent1()/10.f);//馬達電流U
                        drivestate_number[8]=String.valueOf(c1Separation.getACoutputcurrent2()/10.f);//馬達電流V


                        //progress陣列區

                        drivestate_progress[0] = (int) ((Math.abs(c1Separation.getBatteryVoltage()) /10.f/ 200.f) * 100.f);
                        drivestate_progress[6] = (int) ((Math.abs(c1Separation.getMotorSpeed()) /13500.f) * 100.f);
                        drivestate_progress[7] = (int) ((Math.abs(c1Separation.getACoutputcurrent1()) /10.f/ 1000.f) * 100.f);
                        drivestate_progress[8] = (int) ((Math.abs(c1Separation.getACoutputcurrent2()) / 10.f/1000.f) * 100.f);

                        list_drivestate.invalidateViews();

                    }

                    if(primaevalData.getId()[2]==0xC2){
                        char[] _C2=primaevalData.getdata();
                        C2 =primaevalData.getDataLog();
                        c2separation.setC2(_C2);

                        //數值陣列區
                        drivestate_number[3] = String.valueOf((c2separation.getMOSTemperature_1())/10.f);//控制器溫度1
                        drivestate_number[4]=String.valueOf((c2separation.getMOSTemperature_2())/10.f);//控制器溫度2
                        drivestate_number[9]=String.valueOf((c2separation.getMotorTemperature_1())/10.f);//馬達溫度1
                        //drivestate_number[12]=String.valueOf((c2separation.getMotorTemperature_2())/10.f);//馬達溫度2


                        //progress陣列區

                        drivestate_progress[3] = (int) (((Math.abs(c2separation.getMOSTemperature_1()))/10.f/255.f)*100.f);
                        drivestate_progress[4] = (int) (((Math.abs(c2separation.getMOSTemperature_2()))/10.f/255.f)*100.f);
                        drivestate_progress[9] = (int)(((Math.abs(c2separation.getMotorTemperature_1()))/10.f/255.f)*100.f);
                        //drivestate_progress[12] = (int) (((c2separation.getMotorTemperature_2())/10.f/255.f)*100.f);



                        list_drivestate.invalidateViews();




                    }

                    if(primaevalData.getId()[2]==0XC3){

                        char[] _C3=primaevalData.getdata();
                        C3 =primaevalData.getDataLog();
                        c3Separarion.setC3(_C3);
                        //數值陣列區
                        drivestate_number[5] = String.valueOf(c3Separarion.getACC1Status()/10.f);//油門1
                        //drivestate_number[6]=String.valueOf(c3Separarion.getACC2Status()/10.f);//油門2
                        //drivestate_number[7]=String.valueOf(c3Separarion.getBrakeStatus()/10.f);//剎車



                        //progress陣列區

                        drivestate_progress[5] = (int) ((c3Separarion.getACC1Status()/10.0f/12.f)*100.f);
                       // drivestate_progress[6] = (int) ((c3Separarion.getACC2Status()/10.0f/12.f)*100.f);
                        //drivestate_progress[7] = (int)((c3Separarion.getBrakeStatus()/10.0f/12.f)*100.f);

                    }

                    if(primaevalData.getId()[2]==0xC5) {

                        char[] _C5=primaevalData.getdata();
                        C5 =primaevalData.getDataLog();
                        c5Separation.setC5(_C5);
                        //數值陣列區

                        drivestate_number[2] = String.valueOf(c5Separation.getBusCurrent()/ 10.f);
                        drivestate_number[1] = String.valueOf(c5Separation.getBusVoltage()/10.f);


                        //progress陣列區

                        drivestate_progress[2] = (int) ((Math.abs(c5Separation.getBusCurrent()) / 10.f/1000.f) * 100.f);
                        drivestate_progress[1] = (int) ((Math.abs(c5Separation.getBusVoltage()) /10.f/1000.f) * 100.f);
                    }




                     /* 分解DM1資料
                     */

                    if(primaevalData.getId()[2]==0xDD){
                        char[] _DM1 = primaevalData.getdata();
                        DM1 = primaevalData.getDataLog();
                        dm1Separation.setDM1(_DM1);



                        drivestate_number[9]=String.valueOf(dm1Separation.getMotor_current_U());
                        drivestate_number[10]=String.valueOf(dm1Separation.getMotor_current_V());
                        drivestate_number[11]=String.valueOf(dm1Separation.getMotor_Temperature_1());
                        drivestate_number[12]=String.valueOf(dm1Separation.getMotor_Temperature_2());
                        drivestate_number[13]=String.valueOf(dm1Separation.getMotor_Voltage_U());
                        drivestate_number[14]=String.valueOf(dm1Separation.getMotor_Voltage_V());
                        drivestate_number[15]= String.valueOf(dm1Separation.getMotor_Voltage_W());

                        //progress陣列區
                        drivestate_progress[8]=(int)((dm1Separation.getMotorSpeed()/13500.f)*100.f);
                        drivestate_progress[9]=(int)((dm1Separation.getMotor_current_U()/1000.f)*100.f);
                        drivestate_progress[10]=(int)((dm1Separation.getMotor_current_V()/1000.f)*100.f);
                        drivestate_progress[11]=(int)((dm1Separation.getMotor_Temperature_1()/100.f)*100.f);
                        drivestate_progress[12]=(int)((dm1Separation.getMotor_Temperature_2()/100.f)*100.f);
                        drivestate_progress[13]=(int)((dm1Separation.getMotor_Voltage_U()/100.f)*100.f);
                        drivestate_progress[14]=(int)((dm1Separation.getMotor_Voltage_V()/100.f)*100.f);
                        drivestate_progress[15]=(int)((dm1Separation.getMotor_Voltage_W()/100.f)*100.f);

                        list_drivestate.invalidateViews();


                    }

                    if(primaevalData.getId()[2]==0xDC){
                        // String[] _DC1 = primaevalData.getStringSource();
                        DC1 = primaevalData.getDataLog();
                    }

                    /***
                     * GPS 資料分解區
                     */
                    if(primaevalData.getId()[1]==0xDL){
                        //String[] _GPS = primaevalData.getStringSource();
                        GPS = primaevalData.getDataLog();
                        //gpsSeparation.setGPS(_GPS);
                    }

                    data_Tran_OK=true;
                    break;
                case CONNECTING :
                    if(tagflag)Log.d("Handle" ,"CONNECTING" );
                    break;
            }


        }
    };
}

