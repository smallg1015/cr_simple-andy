package com.gevsitech.cr_simple;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.DropBoxManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;


import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class LineChartFragment2 extends Fragment {
    private RelativeLayout layout1;
    private SharedPreferences prefile;
    ArrayList<Entry> ListEntryIqF;
    ArrayList<Entry> ListEntryIdF;
    ArrayList<Entry> ListEntryVq;
    ArrayList<Entry> ListEntryVd;
    ArrayList<Entry> ListEntryIqRef;
    ArrayList<Entry> ListEntryIdRef;
    ArrayList<Entry> ListEntryACC1;
    ArrayList<String> label;
    ArrayList<ILineDataSet> Listdataset;
    LineChart lineChart;

    LineData Linedata;


    private CheckBox check2_IqF,check2_IdF,check2_Vq,check2_Vd,check2_IqRef,check2_IdRef,check2_ACC1;
    private Button btn_databack,btn_backtofile2;








    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_line_chart_fragment2, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //元件初始化區
        layout1=(RelativeLayout)getView().findViewById(R.id.chart2);


        check2_IqF=(CheckBox)getView().findViewById(R.id.check2_IqF);
        check2_IdF=(CheckBox)getView().findViewById(R.id.check2_IdF);
        check2_Vq=(CheckBox)getView().findViewById(R.id.check2_Vq);
        check2_Vd=(CheckBox)getView().findViewById(R.id.check2_Vd);
        check2_IqRef=(CheckBox)getView().findViewById(R.id.check2_IqRef);
        check2_IdRef=(CheckBox)getView().findViewById(R.id.check2_IdRef);
        check2_ACC1=(CheckBox)getView().findViewById(R.id.check2_ACC1);
        btn_databack=(Button)getView().findViewById(R.id.btn_databack);
        btn_backtofile2=(Button)getView().findViewById(R.id.btn_backtofile2);






        prefile=getActivity().getSharedPreferences("prefile",Context.MODE_PRIVATE);

        lineChart=new LineChart(getActivity());
        ListEntryIqF=new ArrayList<Entry>();
        ListEntryIdF=new ArrayList<Entry>();
        ListEntryVq=new ArrayList<Entry>();
        ListEntryVd=new ArrayList<Entry>();
        ListEntryIqRef=new ArrayList<Entry>();
        ListEntryIdRef=new ArrayList<Entry>();
        ListEntryACC1=new ArrayList<Entry>();
        label=new ArrayList<String>();




        File nowfilepath=new File(prefile.getString("nowfilepath",""));

        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(nowfilepath));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append(",");
            }
            br.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        String[] txtdata=text.toString().split(",");
        int labeldata=0;
        int c4data=0;
        int c5data=0;
        int c3data=0;

        for(int l=0;l<txtdata.length;l++){
            System.out.println(txtdata[l]);
            String[] dataline=txtdata[l].replace(" ",",").split(",");

            labeldata++;

            if(dataline[2].equals("c4")){
                c4data++;
                float IqF=Integer.parseInt(dataline[5]+dataline[4],16);
                System.out.println(IqF);
                float IdF=(float)Integer.parseInt(dataline[7]+dataline[6],16);
                float Vq=(float)Integer.parseInt(dataline[9]+dataline[8],16);
                float Vd=(float)Integer.parseInt(dataline[11]+dataline[10],16);

                ListEntryIqF.add(new Entry(IqF,c4data-1));
                ListEntryIdF.add(new Entry(IdF,c4data-1));
                ListEntryVq.add(new Entry(Vq,c4data-1));
                ListEntryVd.add(new Entry(Vd,c4data-1));

                System.out.println("c4data"+c4data);
            }

            if(dataline[2].equals("c3")){
                c3data++;
                float ACC1=(float)Integer.parseInt(dataline[5]+dataline[4],16);
                ListEntryACC1.add(new Entry(ACC1,c3data-1));

                System.out.println("c3data"+c3data);
//                short Brake=(short)Integer.parseInt(dataline[9]+dataline[8],16);
//                ListBrake.add(Brake);
            }
            if(dataline[2].equals("c5")){
                c5data++;
                float IqRef=(float)Integer.parseInt(dataline[5]+dataline[4],16);
                float IdRef=(float)Integer.parseInt(dataline[7]+dataline[6],16);


                ListEntryIqRef.add(new Entry(IqRef,c5data-1));
                ListEntryIdRef.add(new Entry(IdRef,c5data-1));


                System.out.println("c5data"+c5data);
            }


        }

        int labeldatause=labeldata/6+1;

        System.out.println("labeldatause="+labeldatause);

        for(int i=0;i<labeldatause;i++){
            label.add(String.valueOf(i));
        }

        Listdataset=new ArrayList<ILineDataSet>();

        Listdataset.add(createDataSet(ListEntryIqF,"Iq_F", Color.RED));
        Listdataset.add(createDataSet(ListEntryIdF,"Id_F", Color.parseColor("#FFFF7B00")));
        Listdataset.add(createDataSet(ListEntryVq,"Vq", Color.BLUE));
        Listdataset.add(createDataSet(ListEntryVd,"Vd", Color.GREEN));



        Linedata=new LineData(label,Listdataset);
        lineChart.clear();
        lineChart.setData(Linedata);
        lineChart.setDescription("進階資料");
        lineChart.refreshDrawableState();

        linechooseboxcreate(check2_IqF,check2_IdF,check2_Vq,check2_Vd,check2_IqRef,check2_IdRef,check2_ACC1);

        Chartsetting(lineChart);



        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        layout1.addView(lineChart,params);


        btn_databack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft=getFragmentManager().beginTransaction();
                ft.replace(R.id.center,new LineChartFragment());
                ft.commit();

            }
        });

        btn_backtofile2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft=getFragmentManager().beginTransaction();
                ft.replace(R.id.center,new FileFragment());
                ft.commit();
            }
        });


    }

    //建立圖表線資料區
    private LineDataSet createDataSet(ArrayList<Entry> whichlist, String linename, int color){
        LineDataSet dataset=new LineDataSet(whichlist,linename);
        dataset.setDrawValues(false);
        dataset.setDrawCircles(false);
        dataset.setColor(color);

        return dataset;


    }


    //可刪除或顯示資料
    public void linechooseboxcreate(CheckBox box1, CheckBox box2, CheckBox box3, CheckBox box4,CheckBox box5,CheckBox box6,CheckBox box7){
        box1.setChecked(true);
        box2.setChecked(true);
        box3.setChecked(true);
        box4.setChecked(true);

        box1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    Listdataset.add(createDataSet(ListEntryIqF,"Iq_F", Color.RED));
                }
                else {
                    Listdataset.remove(Linedata.getDataSetByLabel("Iq_F", true));

                }
                Linedata=new LineData(label,Listdataset);
                lineChart.clear();
                lineChart.setData(Linedata);

                lineChart.refreshDrawableState();
            }
        });
        box2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Listdataset.add(createDataSet(ListEntryIdF,"Id_F", Color.parseColor("#FFFF7B00")));
                }else{
                    Listdataset.remove(Linedata.getDataSetByLabel("Id_F",true));
                }
                Linedata=new LineData(label,Listdataset);
                lineChart.clear();
                lineChart.setData(Linedata);

                lineChart.refreshDrawableState();
            }
        });
        box3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Listdataset.add(createDataSet(ListEntryVq,"Vq", Color.BLUE));
                }
                else{
                    Listdataset.remove(Linedata.getDataSetByLabel("Vq",true));
                }
                Linedata=new LineData(label,Listdataset);
                lineChart.clear();
                lineChart.setData(Linedata);

                lineChart.refreshDrawableState();
            }
        });
        box4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Listdataset.add(createDataSet(ListEntryVd,"Vd", Color.GREEN));
                }else{
                    Listdataset.remove(Linedata.getDataSetByLabel("Vd",true));
                }
                Linedata=new LineData(label,Listdataset);
                lineChart.clear();
                lineChart.setData(Linedata);

                lineChart.refreshDrawableState();
            }
        });
        box5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Listdataset.add(createDataSet(ListEntryIqRef,"Iq_Ref", Color.parseColor("#FFB700FF")));

                }else{
                    Listdataset.remove(Linedata.getDataSetByLabel("Iq_Ref",true));

                }
                Linedata=new LineData(label,Listdataset);
                lineChart.clear();
                lineChart.setData(Linedata);

                lineChart.refreshDrawableState();
            }
        });
        box6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Listdataset.add(createDataSet(ListEntryIdRef,"Id_Ref", Color.CYAN));
                }else{
                    Listdataset.remove(Linedata.getDataSetByLabel("Id_Ref",true));
                }
                Linedata=new LineData(label,Listdataset);
                lineChart.clear();
                lineChart.setData(Linedata);

                lineChart.refreshDrawableState();
            }
        });
        box7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Listdataset.add(createDataSet(ListEntryACC1,"ACC1", Color.DKGRAY));
                }else{
                    Listdataset.remove(Linedata.getDataSetByLabel("ACC1",true));

                }
                Linedata=new LineData(label,Listdataset);
                lineChart.clear();
                lineChart.setData(Linedata);

                lineChart.refreshDrawableState();
            }
        });

    }

    public void Chartsetting(LineChart linechart){
        linechart.getAxisRight().setDrawLabels(false);
        linechart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        linechart.getXAxis().setDrawLimitLinesBehindData(true);
        linechart.getAxisLeft().setDrawZeroLine(false);
        linechart.getAxisLeft().setSpaceBottom(0f);
        linechart.getAxisRight().setSpaceBottom(0f);
        linechart.setDescription("");
    }





}
