package com.gevsitech.cr_simple;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.Handler;
import android.text.format.DateFormat;
import android.util.Log;

import com.gevsitech.cr_simple.data_separation.SaveTimeclass;
import com.gevsitech.cr_simple.data_separation.Savelag;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Formatter;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

/**
 * Created by steven on 2015/4/5.
 * 藍芽連線API
 */

public class BluetoothService {

    private BluetoothAdapter bluetoothAdapter ;
    private Handler handler;
    private Context context;
    private static final UUID MYUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private Savelag savelagclass=new Savelag();






    /**
     *  連線開始與斷線處裡
     *  1. DISCONNECT 斷線狀態    0
     *  2. CONNECTED  連線中      1
     *  3. CONNECTING 嘗試連線中  2
     **/

    private static int CONNECTFLAG = 0 ;
    private static boolean isdemorun = false ;
    private final static int _CONNECTED = 0x10 ;
    private final static int _DISCONNECT = 0x20 ;
    private final static int _FAULTCONNECT = 0x30 ;

    /**
     *  連線中狀態值
     *  1. DISCONNECT 斷線狀態    0
     *  2. CONNECTED  連線中      1
     *  3. CONNECTING 嘗試連線中  2
     **/

    private static int CONNECT_STATE = 0 ;
    private final static int DISCONNECT = 0 ;
    private final static int CONNECTED = 1 ;
    private final static int CONNECTING = 2 ;

    /**
     *  連線中狀態值
     *  1. DATAWAIT   等待資料中    0
     *  2. DATAREAD   資料讀取      1
     *  3. DATAWIRTE  資料寫入      2
     **/
    private static int READORWIRTE = 0 ;
    private final static int DATAWAIT = 0 ;
    private final static int DATAREAD = 1 ;
    private final static int DATAWIRTE = 2 ;

    private Connecting connecting;
    private Connected connected;

    private static BluetoothService bluetoothService = new BluetoothService();
    private BluetoothService(){
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }
    public static BluetoothService  getBluetoothService(){
        return bluetoothService;
    }

    //中斷藍芽連線用
    public void DisConnect(){
        CONNECT_STATE = DISCONNECT;
        connected = null;
        connecting = null;
        handler.obtainMessage(CONNECT_STATE,0,_DISCONNECT,null).sendToTarget();
    }
    //開始藍芽連線用
    public void StartConnect(String devicename){
        bluetoothAdapter.cancelDiscovery();
        BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(devicename);
        if((connecting == null)|| (connected==null)){
            connecting = null;
            connected = null;
        }
        connecting = new Connecting(bluetoothDevice);
        connecting.start();
    }
    //測試藍芽連線用
    public void StartDemo(){
        if(!isdemorun){
            Thread TESTMODE = new TESTMODE();
            TESTMODE.start();
        }
        isdemorun = true;
    }

    //測試藍芽發送連線用
    public void StartSendDemo(){
        if(!isdemorun){
            Thread TESTMODE2 = new TESTMODE2();
            TESTMODE2.start();
        }
        isdemorun = true;
    }

    //檢查藍芽連限用
    public Boolean isConnectedCheck(){
        if(CONNECT_STATE == CONNECTED){
            return true;
        }else{
            return false;
        }

    }
    //藍芽連線用途。
    private class Connecting extends Thread {
        private String tag = "Connecting :";
        private BluetoothSocket socket;
        private BluetoothDevice devicename;
        public Connecting(BluetoothDevice devicename){
            if(connected != null){
                connected = null;
            }
            this.devicename = devicename;
        }
        @Override
        public void run() {
            super.run();
            try {
                Log.d(tag, "Start Connecting Thread");
                socket = devicename.createInsecureRfcommSocketToServiceRecord(MYUUID);
                socket.connect();
                CONNECT_STATE = CONNECTING;
                handler.obtainMessage(CONNECT_STATE).sendToTarget();
            }catch (IOException e){
                try {
                    Log.d(tag, "連線錯誤關閉連線");
                    CONNECT_STATE = DISCONNECT;
                    handler.obtainMessage(CONNECT_STATE,0,_FAULTCONNECT,null).sendToTarget();
                    socket.close();
                } catch (IOException O) {
                    Log.d(tag, "關閉錯誤");
                }
            }
            if(socket.isConnected()){
                handler.obtainMessage(CONNECT_STATE,0,_CONNECTED,null).sendToTarget();
                Log.d(tag, "Bluetooth device is Connected");
                CONNECT_STATE = CONNECTED;
                connected = new Connected(socket);
                connected.start();
            }

        }
    }
    //連線成功後，傳輸訊息用。
    static int check = 0;
    private class Connected  extends Thread {
//        private String tag = "Connected :";
//        private String end =">";
//        private BluetoothSocket socket;
//        private InputStream datainput_int;
//        private OutputStream datainput_Out;
//        private StringBuilder curMsg = new StringBuilder();
//        private byte[] buffer = new byte[1024];
//        private int bytes;

        private String tag = "Connected :";
        private String end ="\n";
        private BluetoothSocket socket;
        private InputStream datainput_int;
        private OutputStream datainput_Out;
        private StringBuilder curMsg = new StringBuilder();
        private boolean GetSerialData_Flag= false;
        private byte[] buffer = new byte[1024];
        private byte incomingByte = 0x3c;
        private int ByteReadCount = 0;
        private int bytes;
        private int haveCheck=0;
        private SharedPreferences presave;
        String[] tempdata=new String[7];//暫存陣列
        Timer SaveTimer=new Timer();
        private SaveTimeclass saveTimeclass=new SaveTimeclass();
        private boolean saveok;





        Calendar mCal = Calendar.getInstance();
        CharSequence date = DateFormat.format("yyyy-MM-dd", mCal.getTime());    // kk:24小時制, hh:12小時制
        CharSequence time = DateFormat.format("yyyy-MM-dd-kk-mm-ss", mCal.getTime());    // kk:24小時制, hh:12小時制

        String SaveFileName = String.valueOf(time)+".txt";
        File LogPath=new File(Environment.getExternalStorageDirectory().getPath()+"/CR_simple/"+date);
        LogService logService=LogService.getLogService();


        private byte[] value = {0,0,0,0,0,0,0,0,0,0,0,0};
        public Connected(BluetoothSocket socket){
            this.socket = socket;
            //destory connecting
            connecting = null;
        }

        @Override
        public  void run(){
            super.run();
            Log.i(tag, "BEGIN mConnectedThread");
            if (socket == null) {
                return;
            }
            try {
                datainput_int = socket.getInputStream();
                datainput_Out = socket.getOutputStream();


            }catch (Exception e){
                e.printStackTrace();
            }


            //每固定時間儲存資料
            SaveTimer.schedule(new TimerTask() {
                @Override
                    public void run() {
                    if(savelagclass.getSavelag()) {
                        logService.SaveData(LogPath, SaveFileName, tempdata);
                    }
                        }
                },1000,1000);





            while (CONNECT_STATE == CONNECTED){



//                try {
//
//                    //接收資料
//                    while ((bytes = datainput_int.available())>0) {
//                        bytes = datainput_int.read(buffer);
//                        String ss = new String(buffer, 0, bytes, Charset.forName("ISO-8859-1"));
//                        System.out.println("ss="+ss);
//                        if(ss.length()>40)check++;
//                        else check = 6;
//
//                        if(check>5){
//                            curMsg.append(ss);
//                            ss = null;
//                            System.out.println("curMsg="+curMsg);
//                        }
//
//                        int endIdx = curMsg.indexOf(end);
//                        System.out.println("endIX="+endIdx);
//                        if (endIdx != -1) {
//                            String fullMessage = curMsg.substring(0, endIdx + end.length());
//                            System.out.println("fullMessage="+fullMessage);
//                            curMsg.delete(0, endIdx + end.length());
//                            READORWIRTE = DATAREAD;
//                            handler.obtainMessage(CONNECT_STATE, READORWIRTE, check, fullMessage).sendToTarget();
//                        }
//                    }
//
//                    //發送資料
//
//                }catch (Exception e){
//                    CONNECT_STATE = DISCONNECT;
//                    //handler.obtainMessage(CONNECT_STATE,0,0x30).sendToTarget();
//                }
//            }
//            try {
//                socket.close();
//                socket = null;
//                handler.obtainMessage(CONNECT_STATE,0,0x30).sendToTarget();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
                try {
                    if(datainput_int.available()>0) {
                        haveCheck=0;
                        incomingByte = (byte) datainput_int.read();
                        if (GetSerialData_Flag) //開始旗標是true 就可以開始抓取相關欄位的資料
                        {
                            //讀取到結束符號
                            if (incomingByte == '>') {

                                if (ByteReadCount == 12) {
                                    GetSerialData_Flag = false;
                                    ByteReadCount = 0;
                                    READORWIRTE = DATAREAD;
                                    String ss = "<" + new String(value, Charset.forName("ISO-8859-1")) + ">";

                                    handler.obtainMessage(CONNECT_STATE, READORWIRTE, check, ss).sendToTarget();

                                    //將byte轉為hex string
                                    Formatter formatter = new Formatter();
                                    for (byte b : value) {
                                        formatter.format("%02x", b);
                                    }
                                    String hex = formatter.toString();
                                    System.out.println("SaveString=" + hex);

                                    //將hex string分解用來判斷
                                    String d1=hex.substring(0,2);
                                    String d2=hex.substring(2,4);
                                    String d3=hex.substring(4,6);
                                    String d4=hex.substring(6,8);
                                    String d5=hex.substring(8,10);
                                    String d6=hex.substring(10,12);
                                    String d7=hex.substring(12,14);
                                    String d8=hex.substring(14,16);
                                    String d9=hex.substring(16,18);
                                    String d10=hex.substring(18,20);
                                    String d11=hex.substring(20,22);
                                    String d12=hex.substring(22,24);

                                    String savedata=d1+" "+d2+" "+d3+" "+d4+" "+d5+" "+d6+" "+d7+" "+d8+" "+d9+" "+d10+" "+d11+" "+d12;



                                    String IDdata=hex.substring(4,6);
                                    if(IDdata.equals("c0")){
                                        tempdata[0]=savedata;
                                    }
                                    else if(IDdata.equals("70")){
                                        tempdata[1]=savedata;
                                    }
                                    else if(IDdata.equals("c5")){
                                        tempdata[2]=savedata;
                                    }
                                    else if(IDdata.equals("c4")){
                                        tempdata[3]=savedata;
                                    }
                                    else if(IDdata.equals("c3")){
                                        tempdata[4]=savedata;
                                    }
                                    else if(IDdata.equals("c2")){
                                        tempdata[5]=savedata;
                                    }
                                    else if(IDdata.equals("c1")){
                                        tempdata[6]=savedata;
                                    }

                                    System.out.println(savelagclass.getSavelag());







                                } else if (ByteReadCount > 13) {
                                    GetSerialData_Flag = false;
                                    ByteReadCount = 0;
                                } else {
                                    value[ByteReadCount] = incomingByte;
                                }
                                ByteReadCount++;
                            } else {
                                value[ByteReadCount] = incomingByte;
                                ByteReadCount++;
                            }
                        } else if (incomingByte == '<') {
                            //讀取到開頭符號後，開始處理封包
                            ByteReadCount = 0;
                            //上一筆資料清空，重新開始計算
                            for (int k = 0; k < value.length; k++) {
                                value[k] = 0;
                            }
                            GetSerialData_Flag = true; //
                        } else {
                            for (int k = 0; k < value.length; k++) {
                                value[k] = 0;
                            }
                            GetSerialData_Flag = false;
                        }
                    }
                   else{
//

                    }





                }catch (Exception e){
                    CONNECT_STATE = DISCONNECT;
                    e.printStackTrace();
                    handler.obtainMessage(CONNECT_STATE,0,0x30).sendToTarget();
                }
            }
            try {
                System.out.println("ERROR");
                socket.close();
                socket = null;
                handler.obtainMessage(CONNECT_STATE,0,0x30).sendToTarget();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }



        public void write(String data) {
            byte[] buffer;
            if(data!=null){
                buffer = data.getBytes();
                try {
                    datainput_Out.write(buffer);
                } catch (IOException e) {
                    Log.e("TAG", "Exception during write", e);
                }
            }
        }

        public void write2(char[] data) {
            byte[] charArray = new byte[data.length + 1];
            charArray[data.length] = 0x3E;
            for(int i = 0 ; i < charArray.length-1 ; i++){
                charArray[i] = (byte) data[i];
            }

            try {
                datainput_Out.write(charArray,0,14);
            } catch (IOException e) {
                Log.e("TAG", "Exception during write", e);
                CONNECT_STATE = DISCONNECT;
                handler.obtainMessage(CONNECT_STATE, READORWIRTE, check, "0").sendToTarget();
            }
        }


    }

    //Demo用測試模式
    private static int a = 0;

    //測試接收線程
    private class TESTMODE extends Thread {
        private String tag = "TESTMODE :";
        private String DC1;
        private String DC2;
        private String DM1;
        private String DF1;
        private String DATA;
        public TESTMODE(){}

        @Override
        public  void run(){
            super.run();
            Log.i(tag, "BEGIN TEST MODE");
            while (true){
                try {
                    if(a == 0){
                        try {
                            Thread.sleep(1000);
                        }catch (Exception E) {

                        }
                        a++;
                    }else if(a == 1){
                        DATA =  "<DC1," + (int)(Math.random()*10)
                                +","+(int)(Math.random()*10)
                                +","+(int)(Math.random()*10)
                                +","+(int)((Math.random()*10))
                                +","+(int)((Math.random()*10))
                                +","+(int)(Math.random()*10)
                                +","+(int)(Math.random()*10)
                                +","+(int)(Math.random()*10)
                                +","+(int)(Math.random()*0)
                                +","+(int)(Math.random()*0);
                        a++;
                    }else if(a == 2){
                        DATA = "<DC2," + (int)(Math.random()*200)
                                +","+(int)(Math.random()*200)
                                +","+(int)(Math.random()*100)
                                +","+(int)((Math.random()*255-100))
                                +","+(int)((Math.random()*255-100))
                                +","+(int)(Math.random()*12)
                                +","+(int)(Math.random()*12)
                                +","+(int)(Math.random()*12)
                                +","+(int)(Math.random()*7)
                                +","+(int)(Math.random()*255);
                        a++;
                    }else if(a == 3){
                        DATA = "<DF1," + "20"
                                +","+(int)(Math.random()*1)
                                +","+(int)(Math.random()*65535)
                                +","+(int)((Math.random()*65535))
                                +","+(int)((Math.random()*65535))
                                +","+(int)(Math.random()*65535)
                                +","+(int)(Math.random()*0)
                                +","+(int)(Math.random()*0)
                                +","+(int)(Math.random()*0)
                                +","+(int)(Math.random()*0);
                        a++;
                    }else if(a == 4){
                        DATA = "<DM1," + (int)(Math.random()*6000)
                                +","+(int)(Math.random()*1000)
                                +","+(int)(Math.random()*1000)
                                +","+(int)((Math.random()*255-100))
                                +","+(int)((Math.random()*255-100))
                                +","+(int)(Math.random()*72)
                                +","+(int)(Math.random()*72)
                                +","+(int)(Math.random()*72)
                                +","+(int)(Math.random()*0)
                                +","+(int)(Math.random()*0);
                        a++;
                    }else if(a == 5){
                        DATA = "<GPS," + (int)(Math.random()*60)
                                + "," + (int)(Math.random()*60)
                                + "," + (int)(Math.random()*60)
                                + "," + (int)(Math.random()*60);
                        a++;
                    }else {
                        a = 0;
                    }
                    if(a!=6){
                        handler.obtainMessage(CONNECTED, DATAREAD, 0, DATA).sendToTarget();
                        System.out.println(DATA);
                    }
                    Thread.sleep(20);

                }catch (Exception e){
                    e.printStackTrace();
                }
                //handler.obtainMessage(CONNECT_STATE,0,0x30).sendToTarget();
            }

        }
    }

    //測試發送線程
    private class TESTMODE2 extends Thread {

        private String tag = "TESTMODE :";

        public TESTMODE2(){

        }

        @Override
        public  void run(){
            super.run();
            Log.i(tag, "BEGIN TEST MODE2");
            while (true){
                try {
                    connected.write("data123\n");
                    Thread.sleep(10);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }
    }

    //發送數據用途
    public void write(String s){

        //Log.d("write" , s + "\n");

        connected.write(s + "\n");
    }

    //發送數據用途
    public void write2(char[] chars){

        try {
            connected.write2(chars);
        }catch (Exception e ){



            e.printStackTrace();
        }
    }

    public void setBtHandler(Handler handler){
        this.handler = handler;
    }
    public void setContexts(Context context){
        this.context = context;
    }
    public Boolean getDemoState(){
        return isdemorun;
    }


}
