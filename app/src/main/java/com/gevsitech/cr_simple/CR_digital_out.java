package com.gevsitech.cr_simple;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.gevsitech.cr_simple.data_separation.PrimaevalData;
import com.gevsitech.cr_simple.data_separation.SettingCheck;

import java.io.File;
import java.util.Calendar;

public class CR_digital_out extends AppCompatActivity {
    BluetoothService bluetoothService = BluetoothService.getBluetoothService();
    private PrimaevalData primaevalData=new PrimaevalData();
    private ListView Outputlist;
    private String output[]=new String[]{"主繼電器","輔助繼電器1","輔助繼電器2"};
    String a="0",b="0",c="0";

    //活動紀錄
    LogService logService = LogService.getLogService();
    private File LogPath;
    String logFileName;
    SettingCheck settingCheck=new SettingCheck();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cr_digital_out);
        Outputlist=(ListView)findViewById(R.id.Outputlist);

        bluetoothService.setBtHandler(handler);
        bluetoothService.setContexts(this);

        //活動紀錄
        LogPath = this.getExternalFilesDir(null);
        //取得今日日期
        Calendar mCal = Calendar.getInstance();
        CharSequence ss = DateFormat.format("yyyy-MM-dd", mCal.getTime());    // kk:24小時制, hh:12小時制
        logFileName = String.valueOf(ss)+".txt";


        logService.SaveLog(LogPath, logFileName, "START CR_digital_out");

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_multiple_choice,output);
        Outputlist.setAdapter(adapter);
        Outputlist.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);


        Outputlist.setOnItemClickListener(mylistener);
    }

    private ListView.OnItemClickListener mylistener=new ListView.OnItemClickListener(){
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id){
            if(Outputlist.isItemChecked(0)){a="1";}
            else {a="0";}
            if(Outputlist.isItemChecked(1)){b="1";}
            else {b="0";}
            if(Outputlist.isItemChecked(2)){c="1";}
            else {c="0";}
            char[] bluetoothout=settingCheck.SendDigital(a+b+c+"00000");
            bluetoothService.write2(bluetoothout);
        }
    };

    private Handler handler = new Handler(){
        boolean tagflag = false;
        String  data ;
        /**
         *  連線中狀態值
         *  1. DISCONNECT 斷線狀態    0
         *  2. CONNECTED  連線中      1
         *  3. CONNECTING 嘗試連線中  2
         **/
        private final static int DISCONNECT = 0 ;
        private final static int CONNECTED = 1 ;
        private final static int CONNECTING = 2 ;

        /**
         *  連線中狀態值
         *  1. DATAWAIT   等待資料中    0
         *  2. DATAREAD   資料讀取      1
         *  3. DATAWIRTE  資料寫入      2
         **/

        private final static int DATAWAIT = 0 ;
        private final static int DATAREAD = 1 ;
        private final static int DATAWIRTE = 2 ;
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what){
                case DISCONNECT :
                    if(tagflag) Log.d("Handle" ,"DISCONNECT" );
                    break;
                case CONNECTED :
                    //如成功連線，則拋出連線成功訊息，供使用者辨識
                    if(tagflag)Log.d("Handle" ,"CONNECTED" );
                    if(msg.arg1 == DATAREAD) {
                        data = (String) msg.obj;
                        System.out.println(data);
                        if(primaevalData.startcheck()){
                            if(primaevalData.getId()[2]==0xAD){
                                for(int p=0;p<3;p++){
                                    if(primaevalData.getdata()[p]==0x31){
                                        Outputlist.setItemChecked(p,true);
                                    }
                                    else{
                                        Outputlist.setItemChecked(p,false);
                                    }
                                }
                            }
                        }

                    }
                    break;
                case CONNECTING :
                    if(tagflag)Log.d("Handle" ,"CONNECTING" );
                    break;
            }

            switch (msg.arg2){
                case 0x20 :
                    Toast.makeText(CR_digital_out.this,"斷開藍芽連線",Toast.LENGTH_SHORT).show();
                    break;
                case 0x10 :
                    Toast.makeText(CR_digital_out.this,"藍芽連線成功",Toast.LENGTH_SHORT).show();
                    break;
                case 0x30 :
                    Toast.makeText(CR_digital_out.this,"藍芽連線失敗",Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}
