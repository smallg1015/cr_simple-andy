package com.gevsitech.cr_simple.data_separation;

/**
 * Created by user on 2017/6/27.
 */

public class C3Separarion {
    char ACC1_Status_L;
    char ACC1_Status_H;
    char ACC2_Status_L;
    char ACC2_Status_H;
    char Brake_Status_L;
    char Brake_Status_H;

    char Mode;
    String SystemMode;

    private static String s1 = "Initial Mode";
    private static String s2 = "Monitoring Mode";
    private static String s3 = "MotorCtrl Mode";
    private static String s4 = "Protection Mode";
    private static String s5 = "Fault Mode";
    private static String s6 = "Setting Mode";
    private static String s7 = "Calibration Mode";
    private static String s8 = "BootLoader Mode";

    public void setC3(char []_C3){
        ACC1_Status_L=_C3[0];
        ACC1_Status_H=_C3[1];
        ACC2_Status_L=_C3[2];
        ACC2_Status_H=_C3[3];
        Brake_Status_L=_C3[4];
        Brake_Status_H=_C3[5];
        Mode=_C3[6];

        switch(Mode){
            case 0x00:
                SystemMode = s1 ;
                break;
            case 0x10:
                SystemMode = s2 ;
                break;
            case 0x20:
                SystemMode = s3 ;
                break;
            case 0x30:
                SystemMode = s4 ;
                break;
            case 0x40:
                SystemMode = s5 ;
                break;
            case 0x50:
                SystemMode = s6 ;
                break;
            case 0x60:
                SystemMode = s7 ;
                break;
            case 0x70:
                SystemMode = s8 ;
                break;
        }


    }

    public short getACC1Status(){
        String ACC1_StatusL=Integer.toHexString((int)ACC1_Status_L);
        String ACC1_StatusH=Integer.toHexString((int)ACC1_Status_H);

        if(ACC1_StatusL.length()<2){
            ACC1_StatusL="0"+ACC1_StatusL;
        }
        if( ACC1_StatusH.length()<2){
            ACC1_StatusH="0"+ ACC1_StatusH;
        }

        short a=(short)Integer.parseInt(ACC1_StatusH+ACC1_StatusL,16);
        return a;
    }
    public short getACC2Status(){
        String ACC2_StatusL=Integer.toHexString((int)ACC2_Status_L);
        String ACC2_StatusH=Integer.toHexString((int)ACC2_Status_H);


        if(ACC2_StatusL.length()<2){
            ACC2_StatusL="0"+ACC2_StatusL;
        }
        if( ACC2_StatusH.length()<2){
            ACC2_StatusH="0"+ ACC2_StatusH;
        }

        short a=(short)Integer.parseInt(ACC2_StatusH+ACC2_StatusL,16);
        return a;
    }
    public short getBrakeStatus(){
        String Brake_StatusL=Integer.toHexString((int)Brake_Status_L);
        String Brake_StatusH=Integer.toHexString((int)Brake_Status_H);


        if(Brake_StatusL.length()<2){
            Brake_StatusL="0"+Brake_StatusL;
        }
        if( Brake_StatusH.length()<2){
            Brake_StatusH="0"+ Brake_StatusH;
        }

        short a=(short)Integer.parseInt(Brake_StatusH+Brake_StatusL,16);
        return a;

    }

    public String getSystemMode(){
        return  SystemMode;
    }

}