package com.gevsitech.cr_simple.data_separation;

/**
 * Created by Jian on 2016/7/18.
 */
public class DF1Separation  {
    /****/
    boolean[] flag1 = new boolean[16];
    boolean[] flag2 = new boolean[16];
    boolean[] flag3 = new boolean[16];
    boolean[] flag4 = new boolean[16];

    String runMotor;

    char DF1_0;
    String SystemMode ;
    String RunMotor;

    String SysInitChkFLG;
    String ProtectionFLG;
    String Protection2FLG;
    String WarningFLG;
    String F_Derating;


    /*****模式初始化定義******/
    private static String s1 = "Initial Mode";
    private static String s2 = "Monitoring Mode";
    private static String s3 = "MotorCtrl Mode";
    private static String s4 = "Protection Mode";
    private static String s5 = "Fault Mode";
    private static String s6 = "Setting Mode";
    private static String s7 = "Calibration Mode";
    private static String s8 = "BootLoader Mode";

    public void setDF1(char[] DF1){

        //資料一解析
        DF1_0  = DF1[0];
        switch (DF1_0) {
            case 0x00:
                SystemMode = s1 ;
                break;
            case 0x10:
                SystemMode = s2 ;
                break;
            case 0x20:
                SystemMode = s3 ;
                break;
            case 0x30:
                SystemMode = s4 ;
                break;
            case 0x40:
                SystemMode = s5 ;
                break;
            case 0x50:
                SystemMode = s6 ;
                break;
            case 0x60:
                SystemMode = s7 ;
                break;
            case 0x70:
                SystemMode = s8 ;
                break;
        }

        //資料二解析
        if(DF1[1]==0x01){
            runMotor = "Run";
        }else{
            runMotor = "Stop";
        }

       //資料三解析
        String d1 = Integer.toBinaryString(Integer.parseInt(Integer.toHexString((int)DF1[2]),16));
        if(d1.length()!=16){
            int len = 16-d1.length();
            for(int t = 0 ; t < len; t++){
                d1 =  "0".concat(d1);
            }
        }


        for(int a1 = 0 ; a1 <d1.length(); a1++){
            flag1[15-a1] = d1.startsWith("1",a1);
        }

        //資料四解析
        String d2 = Integer.toBinaryString(Integer.parseInt(Integer.toHexString((int)DF1[3])));
        if(d2.length()!=16){
            int len = 16-d2.length();
            for(int t = 0 ; t < len; t++){
                d2 =  "0".concat(d2);
            }
        }
        for(int a1 = 0 ; a1 <d2.length(); a1++){
            flag2[a1] = d2.startsWith("1",a1);
        }

       //資料五解析
        String d3 = Integer.toBinaryString(Integer.parseInt(Integer.toHexString((int)DF1[4])));
        if(d3.length()!=16){
            int len = 16-d3.length();
            for(int t = 0 ; t < len; t++){
                d3 =  "0".concat(d3);
            }
        }
        if(d3.length()!=16){
            for(int t = 0 ; t < 16-d3.length(); t++){
                d3 =  d3+"0";
            }
        }
        for(int a1 = 0 ; a1 <d3.length(); a1++){
            flag3[a1] = d3.startsWith("1",a1);
        }

         /*//資料六解析
        String d4 = Integer.toBinaryString(Integer.parseInt(DF1[2]));
        for(int a1 = 0 ; a1 <d4.length(); a1++){
            if(d4.length() ==0){
                flag4[a1] = d4.startsWith("1",a1);
            }
        }*/
    }
    public boolean[] getSysInitChkFLG(){
        return flag1;
    }
    public boolean[] getProtectionFLG(){
        return flag2;
    }
    public boolean[] Protection2FLG(){
        return flag3;
    }
    public boolean[] WarningFLG(){
        return flag4;
    }

    public String getSystemMode(){
        return SystemMode;
    }
    public String getrunMotor(){
        return runMotor;
    }
}
