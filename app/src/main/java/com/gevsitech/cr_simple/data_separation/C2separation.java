package com.gevsitech.cr_simple.data_separation;

/**
 * Created by user on 2017/6/27.
 */

public class C2separation {
    char MOS_Temperature_1_H;
    char MOS_Temperature_1_L;
    char MOS_Temperature_2_H;
    char MOS_Temperature_2_L;
    char Motor_Temperature_1_H;
    char Motor_Temperature_1_L;
    char Motor_Temperature_2_H;
    char Motor_Temperature_2_L;

    public void setC2(char[] _C2) {
        MOS_Temperature_1_L=_C2[0];
        MOS_Temperature_1_H=_C2[1];
        MOS_Temperature_2_L=_C2[2];
        MOS_Temperature_2_H=_C2[3];
        Motor_Temperature_1_L=_C2[4];
        Motor_Temperature_1_H=_C2[5];
        Motor_Temperature_2_L=_C2[6];
        Motor_Temperature_2_H=_C2[7];
    }

    public short getMOSTemperature_1(){
        String MOS_Temperature_1H=Integer.toHexString((int)MOS_Temperature_1_H);
        String MOS_Temperature_1L=Integer.toHexString((int)MOS_Temperature_1_L);

        if(MOS_Temperature_1H.length()<2){
            MOS_Temperature_1H="0"+MOS_Temperature_1H;
        }
        if( MOS_Temperature_1L.length()<2){
            MOS_Temperature_1L="0"+ MOS_Temperature_1L;
        }

        short a=(short)Integer.parseInt(MOS_Temperature_1H+MOS_Temperature_1L,16);
        return a;
    }
    public short getMOSTemperature_2(){
        String MOS_Temperature_2H=Integer.toHexString((int)MOS_Temperature_2_H);
        String MOS_Temperature_2L=Integer.toHexString((int)MOS_Temperature_2_L);

        if(MOS_Temperature_2H.length()<2){
            MOS_Temperature_2H="0"+MOS_Temperature_2H;
        }
        if( MOS_Temperature_2L.length()<2){
            MOS_Temperature_2L="0"+ MOS_Temperature_2L;
        }

        short a=(short)Integer.parseInt(MOS_Temperature_2H+MOS_Temperature_2L,16);
        return a;
    }
    public short getMotorTemperature_1(){
        String Motor_Temperature_1H=Integer.toHexString((int)Motor_Temperature_1_H);
        String Motor_Temperature_1L=Integer.toHexString((int)Motor_Temperature_2_L);

        if(Motor_Temperature_1H.length()<2){
            Motor_Temperature_1H="0"+Motor_Temperature_1H;
        }
        if( Motor_Temperature_1L.length()<2){
            Motor_Temperature_1L="0"+ Motor_Temperature_1L;
        }

        short a=(short)Integer.parseInt(Motor_Temperature_1H+Motor_Temperature_1L,16);
        return a;

    }
    public short getMotorTemperature_2(){
        String Motor_Temperature_2H=Integer.toHexString((int)Motor_Temperature_2_H);
        String Motor_Temperature_2L=Integer.toHexString((int)Motor_Temperature_2_L);

        if(Motor_Temperature_2H.length()<2){
            Motor_Temperature_2H="0"+Motor_Temperature_2H;
        }
        if( Motor_Temperature_2L.length()<2){
            Motor_Temperature_2L="0"+ Motor_Temperature_2L;
        }

        short a=(short)Integer.parseInt(Motor_Temperature_2H+Motor_Temperature_2L,16);
        return a;
    }
}
