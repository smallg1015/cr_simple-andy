package com.gevsitech.cr_simple.data_separation;

/**
 * Created by Jian on 2016/8/1.
 */
public class GPSSeparation {

    String speed ;
    String accuracy ;
    String longitude ;
    String latitude;

    public String getSpeed() {
        return speed;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }


    public void setGPS(String[] GPS){
        //資料一解析
        speed        = GPS[0];
        latitude     = GPS[1];
        longitude    = GPS[2];
        accuracy     = GPS[3];
    }
}
