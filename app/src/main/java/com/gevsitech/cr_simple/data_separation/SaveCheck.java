package com.gevsitech.cr_simple.data_separation;

/**
 * Created by user on 2017/4/10.
 */

public class SaveCheck {
    private char V_value_H;
    private char V_value_L;
    private char A_value_H;
    private char A_value_L;
    private char R_value_L;
    private char R_value_H;
    private char value6;
    private char value7;

    public void setCheck(char[] s){
        V_value_L= s[0];
        V_value_H= s[1];
        A_value_L= s[2];
        A_value_H= s[3];
        R_value_L= s[4];
        R_value_H= s[5];
        value7= s[6];

    }
    public int getVvalue() {
        int value=Integer.parseInt(Integer.toHexString((int)V_value_H)+Integer.toHexString((int)V_value_L),16);//V_value_H為char格式,用(int)強制轉型再轉為16位元字串
        return value;
    }
    public int getAvalue() {
        int value=Integer.parseInt(Integer.toHexString((int)A_value_H)+Integer.toHexString((int)A_value_L),16);
        return value;
    }
    public int getRvalue() {
        int value=Integer.parseInt(Integer.toHexString((int)R_value_H)+Integer.toHexString((int)R_value_L),16);
        return value;
    }


}


