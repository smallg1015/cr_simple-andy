package com.gevsitech.cr_simple.data_separation;

/**
 * Created by user on 2017/6/28.
 */

public class D70Speration {
    char SysInitChkFLG_L;
    char SysInitChkFLG_H;
    char ProtectionFLG_L;
    char ProtectionFLG_H;
    char HW_ProtectFLG_L;
    char HW_ProtectFLG_H;
    char WarningFLG_L;
    char WarningFLG_H;

    boolean[] flag1 = new boolean[16];
    boolean[] flag2 = new boolean[16];
    boolean[] flag3 = new boolean[16];
    boolean[] flag4 = new boolean[16];

    String d1,d2,d3;


    public void setD70(char[] _D70){
        SysInitChkFLG_L=_D70[0];
        SysInitChkFLG_H=_D70[1];
        ProtectionFLG_L=_D70[2];
        ProtectionFLG_H=_D70[3];
        HW_ProtectFLG_L=_D70[4];
        HW_ProtectFLG_H=_D70[5];
        WarningFLG_L=_D70[6];
        WarningFLG_H=_D70[7];

        String  SysInitChkFLGL=Integer.toHexString((int)SysInitChkFLG_L);
        String  SysInitChkFLGH=Integer.toHexString((int)SysInitChkFLG_H);

        if(SysInitChkFLGL.length()<2){SysInitChkFLGL="0"+SysInitChkFLGL;}
        if(SysInitChkFLGH.length()<2){SysInitChkFLGH="0"+SysInitChkFLGH;}

        d1=Integer.toBinaryString(Integer.parseInt(SysInitChkFLGL+SysInitChkFLGH,16));
        if(d1.length()<16){
            int len=16-d1.length();
            for(int t=0;t<len;t++){
                d1="0".concat(d1);
            }
        }
        System.out.println(d1);
        for(int a1=0;a1<d1.length();a1++){
            flag1[a1]=d1.startsWith("1",a1);
        }

        String ProtectionFLGL=Integer.toHexString((int)ProtectionFLG_L);
        String ProtectionFLGH=Integer.toHexString((int)ProtectionFLG_H);

        if(ProtectionFLGL.length()<2){ProtectionFLGL="0"+ProtectionFLGL;}
        if(ProtectionFLGH.length()<2){ProtectionFLGH="0"+ProtectionFLGH;}

        d2=Integer.toBinaryString(Integer.parseInt(ProtectionFLGL+ProtectionFLGH,16));
        if(d2.length()<16){
            int len=16-d2.length();
            for(int t=0;t<len;t++){
                d2="0".concat(d2);
            }
        }
        System.out.println(d2);
        for(int a2=0;a2<d2.length();a2++){

            flag2[a2]=d2.startsWith("1",a2);
        }

        String  HW_ProtectFLGL=Integer.toHexString((int)HW_ProtectFLG_L);
        String  HW_ProtectFLGH=Integer.toHexString((int)HW_ProtectFLG_H);

        if(HW_ProtectFLGL.length()<2){HW_ProtectFLGL="0"+HW_ProtectFLGL;}
        if(HW_ProtectFLGH.length()<2){HW_ProtectFLGH="0"+HW_ProtectFLGH;}

        d3=Integer.toBinaryString(Integer.parseInt(HW_ProtectFLGL+HW_ProtectFLGH,16));

        if(d3.length()<16){
            int len=16-d3.length();
            for(int t=0;t<len;t++){
                d3="0".concat(d3);
            }
        }
        System.out.println(d3);
        for(int a3=0;a3<d3.length();a3++){
            flag3[a3]=d3.startsWith("1",a3);
        }

        String WarningFLGL=Integer.toHexString((int)WarningFLG_L);
        String  WarningFLGH=Integer.toHexString((int)WarningFLG_H);

        String d4=Integer.toBinaryString(Integer.parseInt(WarningFLGH+WarningFLGL,16));

        if(d4.length()<16){
            int len=16-d4.length();
            for(int t=0;t<len;t++){
                d4="0".concat(d4);
            }
        }
        for(int a4=0;a4<d4.length();a4++){
            flag4[a4]=d3.startsWith("1",a4);
        }



    }

    public boolean[] getSysInitChkFLG(){
        return flag1;
    }
    public boolean[] getProtectionFLG(){
        return flag2;
    }
    public boolean[] getProtection2FLG(){
        return flag3;
    }
    public boolean[] getWarningFLG(){
        return flag4;
    }
    public String getSysInitestr(){return d1; }
    public String getProtectionstr(){return d2; }
    public String getProtection2str(){return d3; }
}
