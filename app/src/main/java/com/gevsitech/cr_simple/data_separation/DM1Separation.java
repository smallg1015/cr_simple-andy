package com.gevsitech.cr_simple.data_separation;

/**
 * Created by Jian on 2016/7/22.
 */
public class DM1Separation {

    Float MotorSpeed ;
    Float Motor_current_U ;
    Float Motor_current_V ;
    Float Motor_Temperature_1;
    Float Motor_Temperature_2;
    Float Motor_Voltage_U;
    Float Motor_Voltage_V;
    Float Motor_Voltage_W;


    public Float getMotorSpeed() {
        return MotorSpeed;
    }

    public Float getMotor_current_U() {
        return Motor_current_U;
    }

    public Float getMotor_current_V() {
        return Motor_current_V;
    }

    public Float getMotor_Temperature_1() {
        return Motor_Temperature_1;
    }

    public Float getMotor_Temperature_2() {
        return Motor_Temperature_2;
    }

    public Float getMotor_Voltage_U() {
        return Motor_Voltage_U;
    }

    public Float getMotor_Voltage_V() {
        return Motor_Voltage_V;
    }

    public Float getMotor_Voltage_W() {
        return Motor_Voltage_W;
    }

    public void setDM1(char[] DM1){
        //資料一解析
        MotorSpeed              = Math.abs(Float.valueOf((int)DM1[0]));
        Motor_current_U         = Float.valueOf((int)DM1[1]);
        Motor_current_V         = Float.valueOf((int)DM1[2]);
        Motor_Temperature_1     =Float.valueOf((int)DM1[3]);
        Motor_Temperature_2     = Float.valueOf((int)DM1[4]);
        Motor_Voltage_U         = Float.valueOf((int)DM1[5]);
        Motor_Voltage_V         = Float.valueOf((int)DM1[6]);
        Motor_Voltage_W         = Float.valueOf((int)DM1[7]);
    }
}
