package com.gevsitech.cr_simple.data_separation;

import android.util.Log;

/**
 * Created by Jian on 2016/9/23.
 */
public class SettingCheck {
    public char[] SendWithData(String address , short d0){
        Log.d("SendWithData" , address +"," +d0);

        //設定ID
        String Id=String.valueOf((char)0xAE);

        //設定空白及0值
        String zero=String.valueOf((char)0x00);
        String datalength=String.valueOf((char)0x08);
        String CASE=String.valueOf((char)0x0A);


        //設定資料所在地址
        String address_H ,  address_L;
        address_H = String.valueOf((char)Integer.parseInt(address.substring(0,2),16));
        address_L = String.valueOf((char)Integer.parseInt(address.substring(2,4),16));

        //設定完整字串
        String datastring ;




        //送出資料2Bytes,高與低位元
        System.out.println(d0);

        String d0int = Integer.toString(d0,16);
        System.out.println("transfer hex string:"+d0int);
        for (int i = d0int.length() ; i < 4 ; i++ ){
            d0int = "0" + d0int;
        }
        String value_H ,  value_L;
        value_H = String.valueOf((char)(short)(Integer.parseInt(d0int.substring(0,2),16)));
        value_L = String.valueOf((char)(short)(Integer.parseInt(d0int.substring(2,4),16)));

        System.out.println("value_H"+value_H);
        System.out.println("value_L"+value_L);



        //送出資料數
        String count =String.valueOf((char)0x00);



        //d0int = (int)(Float.parseFloat(d0)*100.f);





        datastring = "<"+zero+zero+Id+datalength+CASE+address_L+address_H+count+value_L+value_H+zero+zero+">";
        char[] datachar=datastring.toCharArray();
        return datachar;
    }

    public char[] SendEEPROMData(String address){
        Log.d("SendWithData" , address );

        //設定ID
        String Id=String.valueOf((char)0xAE);

        //設定空白及0值
        String zero=String.valueOf((char)0x00);
        String datalength=String.valueOf((char)0x08);
        String CASE=String.valueOf((char)0xA0);


        //設定資料所在地址
        String address_H ,  address_L;
        address_H = String.valueOf((char)Integer.parseInt(address.substring(0,2),16));
        address_L = String.valueOf((char)Integer.parseInt(address.substring(2,4),16));

        //設定完整字串
        String datastring ;


        //d0int = (int)(Float.parseFloat(d0)*100.f);


        datastring = "<"+zero+zero+Id+datalength+CASE+address_L+address_H+zero+zero+zero+zero+zero+">";
        char[] datachar=datastring.toCharArray();
        return datachar;
    }

    public char[] SendPassword(String address , String password){


        //設定ID
        String Id=String.valueOf((char)0xAE);

        //設定空白及0值
        String zero=String.valueOf((char)0x00);
        String datalength=String.valueOf((char)0x08);
        String CASE=String.valueOf((char)0x0A);


        //設定資料所在地址
        String address_H ,  address_L;
        address_H = String.valueOf((char)Integer.parseInt(address.substring(0,2),16));
        address_L = String.valueOf((char)Integer.parseInt(address.substring(2,4),16));

        //設定完整字串
        String datastring ;








        String value_H ,  value_L;
        value_H = String.valueOf((char)Integer.parseInt(password.substring(0,2),16));
        value_L = String.valueOf((char)Integer.parseInt(password.substring(2,4),16));

        //送出資料數
        String count =String.valueOf((char)0x00);



        //d0int = (int)(Float.parseFloat(d0)*100.f);





        datastring = "<"+zero+zero+Id+datalength+CASE+address_L+address_H+count+value_L+value_H+zero+zero+">";

        System.out.println("PASSWORDSTRING="+datastring);
        char[] datachar=datastring.toCharArray();
        return datachar;
    }
    public char[] Send(String address){

        //設定完整字串
        String datastring ;
        String zero=String.valueOf((char)0x00);
        String datalength=String.valueOf((char)0x08);
        String CASE=String.valueOf((char)0x0A);
        String Id=String.valueOf((char)0xAF);

        //設定資料所在地址
        String address_H ,  address_L;

        //讀取位元組
        String count =String.valueOf((char)0x01);

        address_H = String.valueOf((char)Integer.parseInt(address.substring(0,2),16));
        address_L = String.valueOf((char)Integer.parseInt(address.substring(2,4),16));

        datastring ="<"+zero+zero+Id+datalength+CASE+address_L+address_H+count+zero+zero+zero+zero+">";
        char[] datachar=datastring.toCharArray();

        return datachar;
    }

    public char[] SendDigital(String digital){
        String datastring;
        String zero=String.valueOf((char)0x00);
        String datalength=String.valueOf((char)0x08);
        String CASE=String.valueOf((char)0xA0);
        String Id=String.valueOf((char)0xAD);

        datastring="<"+zero+zero+Id+datalength+digital+">";

        char[] datachar=datastring.toCharArray();

        return datachar;

    }
}
