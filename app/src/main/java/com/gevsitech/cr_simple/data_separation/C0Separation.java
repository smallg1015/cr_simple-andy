package com.gevsitech.cr_simple.data_separation;

/**
 * Created by user on 2017/6/28.
 */

public class C0Separation {
    char TqCmd_Ramp_H;
    char TqCmd_Ramp_L;
    char Speed_command_H;
    char Speed_command_L;
    char RunMotor;
    char DeratingFLG;
    char Relay_Status;
    char Digital_input_Status;
    String Runmotorst;

    public void setC0(char [] _C0){
        TqCmd_Ramp_H=_C0[0];
        TqCmd_Ramp_L=_C0[1];
        Speed_command_H=_C0[2];
        Speed_command_L=_C0[3];
        RunMotor=_C0[4];
        DeratingFLG=_C0[5];
        Relay_Status=_C0[6];
        Digital_input_Status=_C0[7];




    }

    public int getTqCmdRamp(){
        String TqCmd_RampH=Integer.toHexString((int)TqCmd_Ramp_H);
        String TqCmd_RampL=Integer.toHexString((int)TqCmd_Ramp_L);

        int a=Integer.parseInt(TqCmd_RampH+TqCmd_RampL,16);
        return a;
    }

    public int getSpeedCommand(){
        String Speed_commandH=Integer.toHexString((int)Speed_command_H);
        String Speed_commandL=Integer.toHexString((int)Speed_command_L);

        int a=Integer.parseInt(Speed_commandH+Speed_commandL,16);
        return a;
    }

    public String getRunMotor(){
        if(RunMotor==0x01){
           Runmotorst="Enable";
        }else{
            Runmotorst="Disable";
        }
        return Runmotorst;
    }
    public String getRelay(){
        String Relay=Integer.toBinaryString((int)Relay_Status);
        System.out.println((int)Relay_Status);
        System.out.println(Relay);
        if(Relay.length()<8){
            int len=8-Relay.length();
            for(int t=0;t<len;t++){
                Relay="0".concat(Relay);
            }
        }
        Relay=Relay.substring(5,8);

        System.out.println("transferR="+Relay);


        //Relay = Relay.replaceAll("1" , "● ");
        //Relay = Relay.replaceAll("0" , "○ ");

        return Relay;


    }

    public String getDigitalinputStatus(){
        String DigitalinputStatus=Integer.toBinaryString((int)Digital_input_Status);
        System.out.println((int)Digital_input_Status);
        System.out.println(DigitalinputStatus);
        if(DigitalinputStatus.length()<8){
            int len=8-DigitalinputStatus.length();
            for(int t=0;t<len;t++){
                DigitalinputStatus="0".concat(DigitalinputStatus);
            }
        }

        System.out.println("transferD="+DigitalinputStatus);





        //DigitalinputStatus = DigitalinputStatus.replaceAll("1" , "● ");
        //DigitalinputStatus = DigitalinputStatus.replaceAll("0" , "○ ");

        return DigitalinputStatus;


    }
}
