package com.gevsitech.cr_simple.data_separation;

/**
 * Created by Jian on 2016/7/22.
 */
public class DC2Separation {
    Float VKey_V ;
    Float Batteryvoltage ;
    Float BatteryCurrent ;
    Float MOS_Temperature_1 ;
    Float MOS_Temperature_2 ;
    Float ACC1_Status ;
    Float ACC2_Status ;
    Float Brake_Status ;
    Integer Relay_Status ;
    Integer Digital_input_Status ;

    public void setDC2(char[] DC2){
        //資料一解析
        VKey_V                  = Float.valueOf((int)DC2[0]);
        Batteryvoltage          = Float.valueOf((int)DC2[1]);
        BatteryCurrent          = Float.valueOf((int)DC2[2]);
        MOS_Temperature_1       = Float.valueOf((int)DC2[3]);
        MOS_Temperature_2       = Float.valueOf((int)DC2[4]);
        ACC1_Status             =Float.valueOf((int)DC2[5]);
        ACC2_Status             = Float.valueOf((int)DC2[6]);
        Brake_Status            = Float.valueOf((int)DC2[7]);
//        Relay_Status            = Integer.valueOf(Integer.toHexString((int)DC2[0]));
 //       Digital_input_Status    = Integer.valueOf(Integer.toHexString((int)DC2[0]));
    }

    public Float getVKey_V() {
        return VKey_V;
    }

    public Float getBatteryvoltage() {
        return Batteryvoltage;
    }

    public Float getBatteryCurrent() {
        return BatteryCurrent;
    }

    public Float getMOS_Temperature_1() {
        return MOS_Temperature_1;
    }

    public Float getMOS_Temperature_2() {
        return MOS_Temperature_2;
    }

    public Float getACC1_Status() {
        return ACC1_Status;
    }

    public Float getACC2_Status() {
        return ACC2_Status;
    }

    public Float getBrake_Status() {
        return Brake_Status;
    }

    public int getRelay_Status() {
        return Relay_Status;
    }

    public int getDigital_input_Status() {
        return Digital_input_Status;
    }
}
