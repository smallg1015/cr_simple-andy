package com.gevsitech.cr_simple.data_separation;

/**
 * Created by Jian on 2016/9/23.
 */
public class WriteCheck {
    char Case;
    char eepStatusFlg_H ;
    char eepStatusFlg_L ;
    char rangeErr_H ;
    char rangeErr_L ;
    char writeFail_H;
    char writeFail_L;

    public void setIDCheck(char[] GPS){


        //資料一解析
        Case = GPS[0];
        eepStatusFlg_H    = GPS[1];
        eepStatusFlg_L    = GPS[2];
        rangeErr_H        = GPS[3];
        rangeErr_L        = GPS[4];
        writeFail_H       = GPS[5];
        writeFail_L       = GPS[6];

    }
    //<CE,8,A0,1,0,C6,0,0,0,0
    // <CE,8,A0,0,0,0,0,0,0,0
    public Boolean CheckRAMWrite(){
        if(Case==0x0A&&eepStatusFlg_H==0x00&&
                eepStatusFlg_L==0x00&&
                rangeErr_H==0x00&&
                rangeErr_L==0x00&&
                writeFail_H==0x00&&
                writeFail_L==0x00){
            return  true;
        }else {
            return  false;
        }
    }
    public Boolean CheckEEPROMWrite(){
        if(Case==0xA0&&eepStatusFlg_H==0x00&&
                eepStatusFlg_L==0x00&&
                rangeErr_H==0x00&&
                rangeErr_L==0x00&&
                writeFail_H==0x00&&
                writeFail_L==0x00){
            return  true;
        }else {
            return  false;
        }
    }
}
