package com.gevsitech.cr_simple.data_separation;

/**
 * Created by Jian on 2016/9/23.
 *
 *
 *
 */
public class ReadCheck {


    char Address_H ;
    char Address_L ;
    char Value_H;
    char Value_L ;
    char dataSize ;



    //資料格式-> {Size,cases,Address_L,Address_H,dataSize,Value_L,Value_H}
    public void setData(char[] data){
        //資料一解析

        Address_L     = data[1];
        Address_H     = data[2];
        dataSize      = data[3];
        Value_L       = data[4];
        Value_H       = data[5];
    }

    public short getValues(){
        String valueL=Integer.toHexString((int)Value_L);
        String valueH=Integer.toHexString((int)Value_H);

        if(valueL.length()<2){
            valueL="0"+valueL;
        }
        if(valueH.length()<2){
            valueH="0"+valueH;
        }

        short a =(short) Integer.parseInt(valueH +valueL , 16);
        //String s = String.valueOf(a);
        return a ;
    }

    public String getAddress(){
        String addressL=Integer.toHexString((int)Address_L);
        String addressH=Integer.toHexString((int)Address_H);
        String s = addressH + addressL;
        System.out.println(addressH + addressL);
        return s ;
    }

    public String getPassword(){
        String passwordL=Integer.toHexString((int)Value_L);
        String passwordH=Integer.toHexString((int)Value_H);
        String s = passwordH + passwordL;
        System.out.println(passwordH + passwordL);
        return s ;
    }

}
