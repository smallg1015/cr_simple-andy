package com.gevsitech.cr_simple.data_separation;

/**
 * Created by user on 2017/6/27.
 */

public class C5Separation {
    char Iq_Reference_L;
    char Iq_Reference_H;
    char Id_Reference_L;
    char Id_Reference_H;
    char Bus_Current_L;
    char Bus_Current_H;
    char Bus_Voltage_L;
    char Bus_Voltage_H;

    public void setC5(char[] _C5){
         Iq_Reference_L=_C5[0];
         Iq_Reference_H=_C5[1];
         Id_Reference_L=_C5[2];
         Id_Reference_H=_C5[3];
         Bus_Current_L=_C5[4];
         Bus_Current_H=_C5[5];
         Bus_Voltage_L=_C5[6];
         Bus_Voltage_H=_C5[7];
    }

    public int getIqReference(){
        String Iq_ReferenceL=Integer.toHexString((int)Iq_Reference_L);
        String Iq_ReferenceH=Integer.toHexString((int)Iq_Reference_H);

        if( Iq_ReferenceL.length()<2){
            Iq_ReferenceL="0"+ Iq_ReferenceL;
        }
        if( Iq_ReferenceH.length()<2){
            Iq_ReferenceH="0"+Iq_ReferenceH;
        }

        int a=Integer.parseInt(Iq_ReferenceH+Iq_ReferenceL,16);
        return a;
    }

    public int getIdReference(){
        String Id_ReferenceL=Integer.toHexString((int)Id_Reference_L);
        String Id_ReferenceH=Integer.toHexString((int)Id_Reference_H);

        if( Id_ReferenceL.length()<2){
            Id_ReferenceL="0"+ Id_ReferenceL;
        }
        if( Id_ReferenceH.length()<2){
            Id_ReferenceH="0"+Id_ReferenceH;
        }

        int a=Integer.parseInt(Id_ReferenceH+Id_ReferenceL,16);
        return a;
    }



    public short getBusCurrent(){
        String Bus_CurrentL=Integer.toHexString((int)Bus_Current_L);
        String Bus_CurrentH=Integer.toHexString((int)Bus_Current_H);

        if( Bus_CurrentL.length()<2){
            Bus_CurrentL="0"+ Bus_CurrentL;
        }
        if( Bus_CurrentH.length()<2){
            Bus_CurrentH="0"+ Bus_CurrentH;
        }


        short a=(short)Integer.parseInt(Bus_CurrentH+Bus_CurrentL,16);
        return a;
    }

    public short getBusVoltage(){
        String Bus_VoltageL=Integer.toHexString((int)Bus_Voltage_L);
        String Bus_VoltageH=Integer.toHexString((int)Bus_Voltage_H);

        if( Bus_VoltageL.length()<2){
            Bus_VoltageL="0"+ Bus_VoltageL;
        }
        if( Bus_VoltageH.length()<2){
            Bus_VoltageH="0"+ Bus_VoltageH;
        }

        short a=(short)Integer.parseInt(Bus_VoltageH+Bus_VoltageL,16);
        return a;
    }


}
