package com.gevsitech.cr_simple.data_separation;

/**
 * Created by user on 2017/6/27.
 */

public class C1Separation {
    char MotorSpeed_H;
    char MotorSpeed_L;
    char Battery_voltage_H;
    char Battery_voltage_L;
    char AC_output_current_1_H;
    char AC_output_current_1_L;
    char AC_output_current_2_H;
    char AC_output_current_2_L;


    public void setC1(char[] _C1){
        MotorSpeed_L=_C1[0];
        MotorSpeed_H=_C1[1];
        Battery_voltage_L=_C1[2];
        Battery_voltage_H=_C1[3];
        AC_output_current_1_L=_C1[4];
        AC_output_current_1_H=_C1[5];
        AC_output_current_2_L=_C1[6];
        AC_output_current_2_H=_C1[7];



    }

    public short getMotorSpeed(){
        String MotorSpeedH=Integer.toHexString(MotorSpeed_H);
        String MotorSpeedL=Integer.toHexString(MotorSpeed_L);

        if(MotorSpeedH.length()<2){
            MotorSpeedH="0"+MotorSpeedH;
        }
        if(MotorSpeedL.length()<2){
            MotorSpeedL="0"+MotorSpeedL;
        }

        short a=(short)Integer.parseInt(MotorSpeedH+MotorSpeedL,16);
        return a;

    }
    public short getBatteryVoltage(){
        String Battery_voltageH=Integer.toHexString(Battery_voltage_H);
        String Battery_voltageL=Integer.toHexString(Battery_voltage_L);

        if(Battery_voltageH.length()<2){
            Battery_voltageH="0"+Battery_voltageH;
        }
        if(Battery_voltageL.length()<2){
            Battery_voltageL="0"+Battery_voltageL;
        }

        System.out.println("Hexstring="+Battery_voltageH+Battery_voltageL);

        short a=(short)Integer.parseInt(Battery_voltageH+Battery_voltageL,16);
        return a;
    }
    public short getACoutputcurrent1(){
        String AC_output_current_1H=Integer.toHexString(AC_output_current_1_H);
        String AC_output_current_1L=Integer.toHexString(AC_output_current_1_L);

        if(AC_output_current_1H.length()<2){
            AC_output_current_1H="0"+AC_output_current_1H;
        }
        if(AC_output_current_1L.length()<2){
            AC_output_current_1L="0"+AC_output_current_1L;
        }

        short a=(short)Integer.parseInt(AC_output_current_1H+AC_output_current_1L,16);
        return a;
    }
    public short getACoutputcurrent2(){
        String AC_output_current_2H=Integer.toHexString(AC_output_current_2_H);
        String AC_output_current_2L=Integer.toHexString(AC_output_current_2_L);

        if(AC_output_current_2H.length()<2){
            AC_output_current_2H="0"+AC_output_current_2H;
        }
        if(AC_output_current_2L.length()<2){
            AC_output_current_2L="0"+AC_output_current_2L;
        }

        short a=(short)Integer.parseInt(AC_output_current_2H+AC_output_current_2L,16);
        return a;
    }

}
