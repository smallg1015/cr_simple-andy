package com.gevsitech.cr_simple;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gevsitech.cr_simple.data_separation.PrimaevalData;
import com.gevsitech.cr_simple.data_separation.SettingCheck;
import com.gevsitech.cr_simple.data_separation.WriteCheck;

public class LoginActivity extends Activity {
    private EditText edt_password;
    private Button btn_password,password_close;
    private BluetoothService bluetoothService=BluetoothService.getBluetoothService();
    private ProgressDialog dialog;
    private PrimaevalData primaevalData=new PrimaevalData();
    private WriteCheck writeCheck=new WriteCheck();
    private SettingCheck settingCheck=new SettingCheck();
    private int SendCount;
    private volatile boolean Sendflag=true;
    private volatile boolean checkflag=true;
    private SharedPreferences predrivesetcan,prepassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);


        btn_password=(Button)findViewById(R.id.btn_password);
        password_close=(Button)findViewById(R.id.password_close);
        edt_password=(EditText) findViewById(R.id.edt_password);

        predrivesetcan=getSharedPreferences("predrivesetcan",MODE_PRIVATE);
        prepassword=getSharedPreferences("prepassword",MODE_PRIVATE);


        bluetoothService.setBtHandler(BTPoccess);

        password_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btn_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edt_password.getText().toString().equals("abcd")){
                    prepassword.edit().putString("password","4576").commit();
                    predrivesetcan.edit().putBoolean("drivesetting",true).commit();
                    Toast.makeText(LoginActivity.this, "已經取得授權", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(LoginActivity.this, "授權碼錯誤,請重新輸入", Toast.LENGTH_SHORT).show();
                    predrivesetcan.edit().putBoolean("drivesetting",true).commit();
                }
            }
        });



    }


    private Handler BTPoccess = new Handler(){
        private StringPoccess poccess = new StringPoccess();

        private String data ;

        private boolean tagflag = false;

        /**
         *  連線中狀態值
         *  1. DISCONNECT 斷線狀態    0
         *  2. CONNECTED  連線中      1
         *  3. CONNECTING 嘗試連線中  2
         **/
        private final static int DISCONNECT = 0 ;
        private final static int CONNECTED = 1 ;
        private final static int CONNECTING = 2 ;

        /**
         *  連線中狀態值
         *  1. DATAWAIT   等待資料中    0
         *  2. DATAREAD   資料讀取      1
         *  3. DATAWIRTE  資料寫入      2
         **/

        private final static int DATAWAIT = 0 ;
        private final static int DATAREAD = 1 ;
        private final static int DATAWIRTE = 2 ;

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case DISCONNECT :
                    if(tagflag) Log.d("Handle" ,"DISCONNECT" );
                    break;
                case CONNECTED :
                    if(tagflag)Log.d("Handle" ,"CONNECTED" );

                    if(msg.arg1 == DATAREAD) {
                        data = (String) msg.obj;
                        if(data == null)return;
                        System.out.println(data);
                        primaevalData.split(data);
                    }

                    //System.out.println(data);
                    if(primaevalData.startcheck()) {
                        if (primaevalData.getId()[2] == 0xCE) {


//                            if (writeCheck.CheckRAMWrite()) {
//                                checkflag = true;
//                            }
//                            if (writeCheck.CheckEEPROMWrite()) {
//                                checkflag = true;
//                            }
                        }
                    }

                    break;
                case CONNECTING :
                    if(tagflag)Log.d("Handle" ,"CONNECTING" );
                    break;
                case 5:
                    try {
                        dialog.dismiss();
                    }catch (Exception e){
                        dialog.dismiss();
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };
}
