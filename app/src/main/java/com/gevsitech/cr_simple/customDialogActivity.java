package com.gevsitech.cr_simple;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class customDialogActivity extends Activity {

    TextView text_content;
    TextView text_title;
    Button dialog_close;
    String dsppwr1,dsppwr2,dsppwr3,eepromcs,paraset,vkeyov,vkeyuv,mosot,mosut,motorot,motorut,accinit,gearshiftinit,percharge,mainrelay,motorline,
            busuv,busov,busdv,mosut1,mosut2,mosot1,mosot2,mosdt,motorut1,motorut2,motorot1,motorot2,ospd,
            auxpower_hw,busov_hw,mosot_hw,falutcheck_hw,interlock,safty,accerr,accfault,encodererr,vcutout,gearshifterr,dischargeerr,oc,ocw_tout;
    ;
    ArrayList<String> list_content=new ArrayList<String>();
    SharedPreferences pre_dialogcontent;
    String[] list_title=new String[]{"DSP內部電源異常","DSP內部電源異常","DSP內部電源異常","記憶體檢查異常","參數設定異常","開機電壓過高","開機電壓過低",
    "控制器過高溫","控制器過低溫","馬達過高溫","馬達過低溫","油門未定位","檔位未復歸","預充電異常","主繼電器異常","馬達未接線",
    "Bus過低壓保護","Bus過高壓保護","Bus電壓瞬間變化過大保護","控制器過低溫保護","控制器過低溫保護","控制器過高溫保護","控制器過高溫保護","控制器內部溫差過大保護","馬達過低溫保護",
    "馬達過低溫保護","馬達過高溫保護","馬達過高溫保護","超速保護","輔助電源異常","電源過高壓","控制器過溫度","硬體異常保護訊號動作","InterLock 開關動作","Safty 開關動作","油門故障","油門故障",
    "旋轉編碼器故障","控制命令失聯","檔位開關故障","放電功能故障","過電流保護","過電流保護"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_custom_dialog);

        dialog_close = (Button) findViewById(R.id.text_close);
        text_title  = (TextView)findViewById(R.id.dialog_title);
        text_content = (TextView)findViewById(R.id.dialog_content);

        Content_string();

        Listcontentcreate();


        pre_dialogcontent=getSharedPreferences("pre_dialogcontent",MODE_PRIVATE);
        text_content.setText(list_content.get(pre_dialogcontent.getInt("which",0)));
        text_title.setText(list_title[pre_dialogcontent.getInt("which",0)]);





        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void Content_string(){
        dsppwr1="1. 重新關閉電源再開\n2. 若故障狀況仍是相同，則控制器已受損，需更換控制器";
        dsppwr2="1. 重新關閉電源再開 \n2. 若故障狀況仍是相同，則控制器已受損，需更換控制器";
        dsppwr3="1. 重新關閉電源再開 \n2. 若故障狀況仍是相同，則控制器已受損，需更換控制器";
        eepromcs="1. 重新關閉電源再開\n2. 若故障仍舊相同，則重新更新控制器設定參數或更新回出廠參數設定值，後再重新開機\n3. 若故障狀況仍未排除，則控制器已受損，需更換控制器";
        paraset="1. 重新關閉電源再開\n2. 若故障仍舊相同，則重新更新控制器設定參數或更新回出廠參數設定值，後再重新開機\n3. 若故障狀況仍未排除，則控制器已受損，需更換控制器";
        vkeyov="請檢查電池電壓是否超過使用範圍";
        vkeyuv="1. 請檢查電池接線是否有接觸不良\n2. 請檢查電池電壓是否超過使用範圍";
        mosot="關閉電源，並待控制器溫度至工作範圍後再重新開啟電源";
        mosut="關閉電源，並待控制器溫度至工作範圍後再重新開啟電源";
        motorot="關閉電源，並待馬達溫度至工作範圍後再重新開啟電源";
        motorut="關閉電源，並待馬達溫度至工作範圍後再重新開啟電源";
        accinit="1. 請將油門放開至啟始點\n2. 請檢查機構部件，是否有卡住油門的作動\n3.請檢查油門接線是否有接觸不良\n4.請檢查油門裝置是否損壞故障";
        gearshiftinit="1. 請將方向檔位切移至N檔\n2. 請檢查機構部件，是否有卡住方向檔的作動\n3.請檢查方向檔訊號接線是否有接觸不良\n4.請檢查方向檔開關是否損壞故障";
        percharge="1. 重新關閉電源再開\n2. 若故障狀況仍是相同，則控制器已受損，需更換控制器";
        mainrelay="1. 重新關閉電源再開\n2.請主繼電器迴路接線是否有接觸不良\n3.請檢查主繼電器是否損壞故障";
        motorline="請檢查馬達接線迴路接線是否良好";
        busuv="1. 請檢查電池接線是否有接觸不良\n2. 請檢查電池電壓是否超過使用範圍";
        busov="1. 請檢查電池接線是否有接觸不良\n2. 請檢查電池電壓是否超過使用範圍";
        busdv="1. 請檢查電池接線是否有接觸不良\n2. 請檢查電池電壓是否超過使用範圍\n3. 請檢查電池是否老化，需要更新";
        mosut1="關閉電源，並待控制器溫度至工作範圍後再重新開啟電源";
        mosut2="關閉電源，並待控制器溫度至工作範圍後再重新開啟電源";
        mosot1="關閉電源，並待馬達溫度至工作範圍後再重新開啟電源";
        mosot2="關閉電源，並待馬達溫度至工作範圍後再重新開啟電源";
        mosdt="關閉電源，並待控制器溫度至工作範圍後再重新開啟電源";
        motorut1="關閉電源，並待控制器溫度至工作範圍後再重新開啟電源";
        motorut2="關閉電源，並待馬達溫度至工作範圍後再重新開啟電源";
        motorot1="關閉電源，並待馬達溫度至工作範圍後再重新開啟電源";
        motorot2="關閉電源，並待馬達溫度至工作範圍後再重新開啟電源";
        ospd="檢查速度偵測組件是否接線良好";
        auxpower_hw="1. 重新關閉電源再開\n2. 若故障狀況仍是相同，則控制器已受損，需更換控制器";
        busov_hw="1. 請檢查電池接線是否有接觸不良\n2. 請檢查電池電壓是否超過使用範圍";
        mosot_hw="關閉電源，並待控制器溫度至工作範圍後再重新開啟電源";
        falutcheck_hw="1. 重新關閉電源再開\n2. 若故障狀況仍是相同，則控制器已受損，需更換控制器";
        interlock="1. 請檢查接線是否有接觸不良\n2. 請檢查開關元件是否損壞故障";
        safty="1. 請檢查接線是否有接觸不良\n2. 請檢查開關元件是否損壞故障";
        accerr="1. 請檢查機構部件，是否有卡住油門的作動\n2. 請檢查油門接線是否有接觸不良\n3. 請檢查油門裝置是否損壞故障";
        accfault="1. 請檢查機構部件，是否有卡住油門的作動\n2. 請檢查油門接線是否有接觸不良\n3.請檢查油門裝置是否損壞故障";
        encodererr="1. 請檢查油門接線是否有接觸不良\n2. 請檢查旋轉編碼器元件是否損壞故障";
        vcutout="1. 請檢查CAN  BUS與VCU接線是否有接觸不良\n2. 請檢查VCU是否損壞故障";
        gearshifterr="1. 請檢查接線是否有接觸不良\n2. 請檢查開關元件是否損壞故障";
        dischargeerr="請檢查馬達接線是否有接觸不良";
        oc="1. 請檢查手煞車是否解除\n2. 請檢查煞車機搆是否正常無卡死\n3. 請移除部分負載，重新關閉電源再開";
        ocw_tout="1. 請檢查手煞車是否解除\n2. 請檢查煞車機搆是否正常無卡死\n3. 請移除部分負載，重新關閉電源再開";

    }

    public void Listcontentcreate(){
        list_content.add(dsppwr1);
        list_content.add(dsppwr2);
        list_content.add(dsppwr3);
        list_content.add(eepromcs);
        list_content.add(paraset);
        list_content.add(vkeyov);
        list_content.add(vkeyuv);
        list_content.add(mosot);
        list_content.add(mosut);
        list_content.add(motorot);
        list_content.add(motorut);
        list_content.add(accinit);
        list_content.add(gearshiftinit);
        list_content.add(percharge);
        list_content.add(mainrelay);
        list_content.add(motorline);
        list_content.add(busuv);
        list_content.add(busov);
        list_content.add(busdv);
        list_content.add(mosut1);
        list_content.add(mosut2);
        list_content.add(mosot1);
        list_content.add(mosot2);
        list_content.add(mosdt);
        list_content.add(motorut1);
        list_content.add(motorut2);
        list_content.add(motorot1);
        list_content.add(motorot2);
        list_content.add(ospd);
        list_content.add(auxpower_hw);
        list_content.add(busov_hw);
        list_content.add(mosot_hw);
        list_content.add(falutcheck_hw);
        list_content.add(interlock);
        list_content.add(safty);
        list_content.add(accerr);
        list_content.add(accfault);
        list_content.add(encodererr);
        list_content.add(vcutout);
        list_content.add(gearshifterr);
        list_content.add(dischargeerr);
        list_content.add(oc);
        list_content.add(ocw_tout);

    }
}
