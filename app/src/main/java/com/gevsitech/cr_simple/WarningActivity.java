package com.gevsitech.cr_simple;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.gevsitech.cr_simple.data_separation.C0Separation;
import com.gevsitech.cr_simple.data_separation.C3Separarion;
import com.gevsitech.cr_simple.data_separation.D70Speration;
import com.gevsitech.cr_simple.data_separation.DF1Separation;
import com.gevsitech.cr_simple.data_separation.PrimaevalData;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;


public class WarningActivity extends Fragment {


   //ProtectionFLG Item
    private Button busuv_led_S;
    private Button busov_led_S;
    private Button busdv_led_S;
    private Button mosut1_led_S;
    private Button mosut2_led_S;
    private Button mosot1_led_S;
    private Button mosot2_led_S;
    private Button mosdt_led_S;
    private Button motorut1_led_S;
    private Button motorut2_led_S;
    private Button motorot1_led_S;
    private Button motorot2_led_S;
    private Button overspeed_led_S;

    //SysInitChkFLG Item
    private Button syspwr1err_led_S;
    private Button syspwr2err_led_S;
    private Button syspwr3err_led_S;
    private Button eepromcserr_led_S;
    private Button paraseterr_led_S;
    private Button vkeyinitov_led_S;
    private Button vkeyinituv_led_S;
    private Button mosinitot_led_S;
    private Button mosinitut_led_S;
    private Button motorinitot_led_S;
    private Button motorinitut_led_S   ;
    private Button acciniterr_led_S  ;
    private Button gearshiftiniterr_led_S  ;
    private Button prechargeerr_led_S   ;
    private Button mainrealyerr_led_S;
    private Button motorline_led_S;



    private Button auxpowerlow_hw_led_S;
    private Button busov_hw_led_S;
    private Button mosot_hw_led_S;
    private Button faultcheck_led_S;
    private Button interlock_led_S;
    private Button safty_led_S;
    private Button accerr_led_S;
    private Button accfault_led_S;
    private Button encodererr_led_S;
    private Button vcu_tout_led_S;
    private Button gearshifterr_led_S;
    private Button dischargeerr_led_S;
    private Button overcurrent_led_S;
    private Button ocw_tout_led_S;
    /****/

    Bitmap led_g_bitmap;
    Bitmap led_o_bitmap;
    Bitmap led_r_bitmap;
    Bitmap led_y_bitmap;
    boolean[] flag1 = new boolean[14];
    ImageView[] imageled ;
    ImageButton imageButton ;
    ImageButton photo ;
    DF1Separation df1Separation;
    D70Speration d70Speration=new D70Speration();
    C0Separation c0Separation=new C0Separation();
    C3Separarion c3Separarion=new C3Separarion();
    BluetoothService bluetoothService = BluetoothService.getBluetoothService();
    TextView mode ;
    TextView motorstate;

    private static boolean data_Tran_OK;
    private ArrayList<Button> arrayListButton;

    SharedPreferences pre_dialogcontent;

    private File LogPath,fileexist;
    String logFileName;
    String SaveFileName;
    LogService logService = LogService.getLogService();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_warning, container, false);
    }


    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);

//        mode=(TextView)getView().findViewById(R.id.Mode);
//        motorstate=(TextView)getView().findViewById(R.id.Motorstate);

        pre_dialogcontent=getActivity().getSharedPreferences("pre_dialogcontent",MODE_PRIVATE);

        LogPath = new File(Environment.getExternalStorageDirectory().getPath()+"/CR_simple");


        //取得今日日期
        Calendar mCal = Calendar.getInstance();
        CharSequence ss = DateFormat.format("yyyy-MM-dd", mCal.getTime());    // kk:24小時制, hh:12小時制
        CharSequence time = DateFormat.format("yyyy-MM-dd-kk-mm-ss", mCal.getTime());    // kk:24小時制, hh:12小時制
        logFileName = String.valueOf(ss)+".txt";
        SaveFileName = String.valueOf(time)+"saveData"+".txt";






       





        ImageLEDInit();
        setButtonListener();
        bluetoothService.setBtHandler(BTPoccess);
        df1Separation = new DF1Separation();

        if(bluetoothService.isConnectedCheck()){

        }else{
            Toast.makeText(getActivity(),"藍芽尚未連線，無法讀取資料",Toast.LENGTH_SHORT).show();
        }
        /*e w= new e();
        w.start();*/
    }


    /***
     * 保存截圖
     */
    private boolean saveBitMap() {
        Bitmap bitmap;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String fname = getActivity().getExternalCacheDir() + "/" + sdf.format(new Date()) + ".png";
        DisplayMetrics dm = new DisplayMetrics();
        this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int widths = dm.widthPixels;
        int heights = dm.heightPixels;
        View view = this.getActivity().getWindow().getDecorView();
        view.buildDrawingCache();
        Rect rect = new Rect();
        view.getWindowVisibleDisplayFrame(rect);
        int
                statusBarHeights = rect.top;
        view.setDrawingCacheEnabled(true);
        bitmap = Bitmap.createBitmap(view.getDrawingCache(), 0,
                statusBarHeights , widths,
                heights - statusBarHeights);
        view.destroyDrawingCache();

        if (bitmap != null) {
            try {
                FileOutputStream out = new FileOutputStream(fname);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.close();
                Toast.makeText(getActivity(), "截圖成功", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }

        return false;
    }

    private Handler BTPoccess = new Handler(){
        private StringPoccess poccess = new StringPoccess();
        private PrimaevalData primaevalData = new PrimaevalData();
        private boolean tagflag = false;
        private boolean tagflaga = false;  //系統提示開關
        String data ;

        private final static int DISCONNECT = 0 ;
        private final static int CONNECTED = 1 ;
        private final static int CONNECTING = 2 ;
        private final static int DATAWAIT = 0 ;
        private final static int DATAREAD = 1 ;
        private final static int DATAWIRTE = 2 ;

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what){
                case DISCONNECT :
                    if(tagflag) Log.d("Handle" ,"DISCONNECT" );
                    Toast.makeText(getActivity(),"CAN BUS斷線 請檢查",Toast.LENGTH_LONG);
                    break;
                case CONNECTED :
                    if(tagflag)Log.d("Handle" ,"CONNECTED" );

                    if(msg.arg1 == DATAREAD) {
                        data = (String) msg.obj;
                        System.out.println(data);
                        poccess.putData(data);
                        primaevalData.split(data);

                        if(primaevalData.getId()[2]==0x70){
                            d70Speration.setD70(primaevalData.getdata());

                            makeSysInitChkFLG(7,syspwr1err_led_S);
                            makeSysInitChkFLG(6,syspwr2err_led_S);
                            makeSysInitChkFLG(5,syspwr3err_led_S);
                            makeSysInitChkFLG(4,eepromcserr_led_S);
                            makeSysInitChkFLG(3,paraseterr_led_S);
                            makeSysInitChkFLG(2,vkeyinitov_led_S);
                            makeSysInitChkFLG(1,vkeyinituv_led_S);
                            makeSysInitChkFLG(0,mosinitot_led_S);
                            makeSysInitChkFLG(15,mosinitut_led_S);
                            makeSysInitChkFLG(14,motorinitot_led_S);
                            makeSysInitChkFLG(13,motorinitut_led_S);
                            makeSysInitChkFLG(12, acciniterr_led_S);
                            makeSysInitChkFLG(11, gearshiftiniterr_led_S);
                            makeSysInitChkFLG(10,prechargeerr_led_S);
                            makeSysInitChkFLG(9,mainrealyerr_led_S);
                            makeSysInitChkFLG(8,motorline_led_S);

                            makeProtectionFLG(7,busuv_led_S);
                            makeProtectionFLG(6,busov_led_S);
                            makeProtectionFLG(5,busdv_led_S);
                            makeProtectionFLG(4,mosut1_led_S);
                            makeProtectionFLG(3,mosut2_led_S);
                            makeProtectionFLG(2,mosot1_led_S);
                            makeProtectionFLG(1,mosot2_led_S);
                            makeProtectionFLG(0,mosdt_led_S);
                            makeProtectionFLG(15,motorut1_led_S);
                            makeProtectionFLG(14,motorut2_led_S);
                            makeProtectionFLG(13,motorot1_led_S);
                            makeProtectionFLG(12, motorot2_led_S);
                            makeProtectionFLG(11,overspeed_led_S);

                            makeHWProtectFLG(7,auxpowerlow_hw_led_S);
                            makeHWProtectFLG(6,busov_hw_led_S);
                            makeHWProtectFLG(5,mosot_hw_led_S);
                            makeHWProtectFLG(4,faultcheck_led_S);
                            makeHWProtectFLG(3,interlock_led_S);
                            makeHWProtectFLG(2,safty_led_S);
                            makeHWProtectFLG(1,accerr_led_S);
                            makeHWProtectFLG(0,accfault_led_S);
                            makeHWProtectFLG(15,encodererr_led_S);
                            makeHWProtectFLG(14,vcu_tout_led_S);
                            makeHWProtectFLG(13,gearshifterr_led_S);
                            makeHWProtectFLG(12,dischargeerr_led_S);
                            makeHWProtectFLG(11,overcurrent_led_S);
                            makeHWProtectFLG(10,ocw_tout_led_S);
                        }
                        if(primaevalData.getId()[2]==0xC0){
                            char[] _C0=primaevalData.getdata();
                            c0Separation.setC0(_C0);

//                            motorstate.setText(c0Separation.getRunMotor());

                        }

                        if(primaevalData.getId()[2]==0xC3){
                            char[] _C3=primaevalData.getdata();
                            c3Separarion.setC3(_C3);

//                            mode.setText(c3Separarion.getSystemMode());

                        }

                    }
                    data_Tran_OK=true;


                    break;
                case CONNECTING :
                    if(tagflag)Log.d("Handle" ,"CONNECTING" );
                    break;
            }

        }
    };


    private void ImageLEDInit(){
        led_g_bitmap =  BitmapFactory.decodeResource(WarningActivity.this.getResources(),R.drawable.led_g);
        led_o_bitmap =  BitmapFactory.decodeResource(WarningActivity.this.getResources(),R.drawable.led_o);
        led_r_bitmap =  BitmapFactory.decodeResource(WarningActivity.this.getResources(),R.drawable.led_r);
        led_y_bitmap =  BitmapFactory.decodeResource(WarningActivity.this.getResources(),R.drawable.led_y);

        busuv_led_S    		=(Button)getView().findViewById(R.id.busuv_led);
        busov_led_S    		=(Button)getView().findViewById(R.id.busov_led);
        busdv_led_S  		    =(Button)getView().findViewById(R.id.busdv_led);
        busov_hw_led_S   	    =(Button)getView().findViewById(R.id.busov_hw_led);
        mosut1_led_S    	    =(Button)getView().findViewById(R.id.mosut1_led);
        mosut2_led_S    	    =(Button)getView().findViewById(R.id.mosut2_led);
        mosot1_led_S    	    =(Button)getView().findViewById(R.id.mosot1_led);
        mosot2_led_S    	    =(Button)getView().findViewById(R.id.mosot2_led);
        mosdt_led_S    		=(Button)getView().findViewById(R.id.mosdt_led);
        motorut1_led_S    	=(Button)getView().findViewById(R.id.motorut1_led);
        motorut2_led_S    	=(Button)getView().findViewById(R.id.motorut2_led);
        motorot1_led_S    	=(Button)getView().findViewById(R.id.motorot1_led);
        motorot2_led_S    	=(Button)getView().findViewById(R.id.motorot2_led);
        overcurrent_led_S 	=(Button)getView().findViewById(R.id.overcurrent_led);
        overspeed_led_S    	=(Button)getView().findViewById(R.id.overspeed_led);

        //SysInitChkFLG
        syspwr1err_led_S = (Button)getView().findViewById(R.id.syspwr1err_led);
        syspwr2err_led_S = (Button)getView().findViewById(R.id.syspwr2err_led);
        syspwr3err_led_S=  (Button)getView().findViewById(R.id.syspwr3err_led);
        eepromcserr_led_S= (Button)getView().findViewById(R.id.eepromcserr_led);
        paraseterr_led_S=  (Button)getView().findViewById(R.id.paraseterr_led);
        vkeyinitov_led_S=  (Button)getView().findViewById(R.id.vkeyinitov_led);
        vkeyinituv_led_S=                  (Button)getView().findViewById(R.id.vkeyinituv_led);
        mosinitot_led_S=                  (Button)getView().findViewById(R.id.mosinitot_led);
        mosinitut_led_S=                  (Button)getView().findViewById(R.id.mosinitut_led);
        motorinitot_led_S=                  (Button)getView().findViewById(R.id.motorinitot_led);
        motorinitut_led_S      =            (Button)getView().findViewById(R.id.motorinitut_led);
        acciniterr_led_S         =         (Button)getView().findViewById(R.id.acciniterr_led);
        gearshiftiniterr_led_S  =(Button)getView().findViewById(R.id.gearshiftiniterr_led);
        prechargeerr_led_S    =              (Button)getView().findViewById(R.id.prechargeerr_led);
        mainrealyerr_led_S     =             (Button)getView().findViewById(R.id.mainrealyerr_led);
        motorline_led_S=(Button)getView().findViewById(R.id.motorlineyerr_led);

        auxpowerlow_hw_led_S=              (Button)getView().findViewById(R.id.auxpowerlow_hw_led);
        interlock_led_S=              (Button)getView().findViewById(R.id.interlock_led);
        safty_led_S=              (Button)getView().findViewById(R.id.safty_led);
        accerr_led_S=              (Button)getView().findViewById(R.id.accerr_led);
        accfault_led_S=              (Button)getView().findViewById(R.id.accfault_led);
        encodererr_led_S=              (Button)getView().findViewById(R.id.encodererr_led);
        vcu_tout_led_S=              (Button)getView().findViewById(R.id.vcu_tout_led);
        gearshifterr_led_S=              (Button)getView().findViewById(R.id.gearshifterr_led);
        mosot_hw_led_S=(Button)getView().findViewById(R.id.mosot_hw_led);
        faultcheck_led_S=(Button)getView().findViewById(R.id.faultcheck_hw_led);
        dischargeerr_led_S=(Button)getView().findViewById(R.id.discharge_led);
        ocw_tout_led_S=(Button)getView().findViewById(R.id.ocw_tout_led);




    }

    public void setButtonListener(){

        //SysInitChkFLG
        syspwr1err_led_S.setOnLongClickListener(explainListner);
        syspwr2err_led_S.setOnLongClickListener(explainListner);
        syspwr3err_led_S.setOnLongClickListener(explainListner);
        eepromcserr_led_S.setOnLongClickListener(explainListner);
        paraseterr_led_S.setOnLongClickListener(explainListner);
        vkeyinitov_led_S.setOnLongClickListener(explainListner);
        vkeyinituv_led_S.setOnLongClickListener(explainListner);
        mosinitot_led_S.setOnLongClickListener(explainListner);
        mosinitut_led_S.setOnLongClickListener(explainListner);
        motorinitot_led_S.setOnLongClickListener(explainListner);
        motorinitut_led_S.setOnLongClickListener(explainListner);
        acciniterr_led_S.setOnLongClickListener(explainListner);
        gearshiftiniterr_led_S.setOnLongClickListener(explainListner);
        prechargeerr_led_S.setOnLongClickListener(explainListner);
        mainrealyerr_led_S.setOnLongClickListener(explainListner);
        motorline_led_S.setOnLongClickListener(explainListner);

        busuv_led_S.setOnLongClickListener(explainListner);
        busov_led_S.setOnLongClickListener(explainListner);
        busdv_led_S.setOnLongClickListener(explainListner);
        mosut1_led_S.setOnLongClickListener(explainListner);
        mosut2_led_S.setOnLongClickListener(explainListner);
        mosdt_led_S.setOnLongClickListener(explainListner);
        mosot1_led_S.setOnLongClickListener(explainListner);
        mosot2_led_S.setOnLongClickListener(explainListner);
        motorut1_led_S.setOnLongClickListener(explainListner);
        motorut2_led_S.setOnLongClickListener(explainListner);
        motorot1_led_S.setOnLongClickListener(explainListner);
        motorot2_led_S.setOnLongClickListener(explainListner);
        overspeed_led_S.setOnLongClickListener(explainListner);



        auxpowerlow_hw_led_S.setOnLongClickListener(explainListner);
        busov_hw_led_S.setOnLongClickListener(explainListner);
        mosot_hw_led_S.setOnLongClickListener(explainListner);
        faultcheck_led_S.setOnLongClickListener(explainListner);
        interlock_led_S.setOnLongClickListener(explainListner);
        safty_led_S.setOnLongClickListener(explainListner);
        accerr_led_S.setOnLongClickListener(explainListner);
        accfault_led_S.setOnLongClickListener(explainListner);
        encodererr_led_S.setOnLongClickListener(explainListner);
        vcu_tout_led_S.setOnLongClickListener(explainListner);
        gearshifterr_led_S.setOnLongClickListener(explainListner);
        dischargeerr_led_S.setOnLongClickListener(explainListner);
        overcurrent_led_S.setOnLongClickListener(explainListner);
        ocw_tout_led_S.setOnLongClickListener(explainListner);




    }

    private Button.OnLongClickListener explainListner=new Button.OnLongClickListener(){
        @Override
        public boolean onLongClick(View v){
            Intent intent=new Intent(getActivity(),customDialogActivity.class);
            startActivity(intent);
            int which=v.getId();
            switch(which){
                case R.id.syspwr1err_led:
                    pre_dialogcontent.edit().putInt("which",0).commit();
                    break;
                case R.id.syspwr2err_led:
                    pre_dialogcontent.edit().putInt("which",1).commit();
                    break;
                case R.id.syspwr3err_led:
                    pre_dialogcontent.edit().putInt("which",2).commit();
                    break;
                case R.id.eepromcserr_led:
                    pre_dialogcontent.edit().putInt("which",3).commit();
                    break;
                case R.id.paraseterr_led:
                    pre_dialogcontent.edit().putInt("which",4).commit();
                    break;
                case R.id.vkeyinitov_led:
                    pre_dialogcontent.edit().putInt("which",5).commit();
                    break;
                case R.id.vkeyinituv_led:
                    pre_dialogcontent.edit().putInt("which",6).commit();
                    break;
                case R.id.mosinitot_led:
                    pre_dialogcontent.edit().putInt("which",7).commit();
                    break;
                case R.id.mosinitut_led:
                    pre_dialogcontent.edit().putInt("which",8).commit();
                    break;
                case R.id.motorinitot_led:
                    pre_dialogcontent.edit().putInt("which",9).commit();
                    break;
                case R.id.motorinitut_led:
                    pre_dialogcontent.edit().putInt("which",10).commit();
                    break;
                case R.id.acciniterr_led:
                    pre_dialogcontent.edit().putInt("which",11).commit();
                    break;
                case R.id.gearshiftiniterr_led:
                    pre_dialogcontent.edit().putInt("which",12).commit();
                    break;
                case R.id.prechargeerr_led:
                    pre_dialogcontent.edit().putInt("which",13).commit();
                    break;
                case R.id.mainrealyerr_led:
                    pre_dialogcontent.edit().putInt("which",14).commit();
                    break;
                case R.id.motorlineyerr_led:
                    pre_dialogcontent.edit().putInt("which",15).commit();
                    break;
                case R.id.busuv_led:
                    pre_dialogcontent.edit().putInt("which",16).commit();
                    break;
                case R.id.busov_led:
                    pre_dialogcontent.edit().putInt("which",17).commit();
                    break;
                case R.id.busdv_led:
                    pre_dialogcontent.edit().putInt("which",18).commit();
                    break;
                case R.id.mosut1_led:
                    pre_dialogcontent.edit().putInt("which",19).commit();
                    break;
                case R.id.mosut2_led:
                    pre_dialogcontent.edit().putInt("which",20).commit();
                    break;
                case R.id.mosot1_led:
                    pre_dialogcontent.edit().putInt("which",21).commit();
                    break;
                case R.id.mosot2_led:
                    pre_dialogcontent.edit().putInt("which",22).commit();
                    break;
                case R.id.mosdt_led:
                    pre_dialogcontent.edit().putInt("which",23).commit();
                    break;
                case R.id.motorut1_led:
                    pre_dialogcontent.edit().putInt("which",24).commit();
                    break;
                case R.id.motorut2_led:
                    pre_dialogcontent.edit().putInt("which",25).commit();
                    break;
                case R.id.motorot1_led:
                    pre_dialogcontent.edit().putInt("which",26).commit();
                    break;
                case R.id.motorot2_led:
                    pre_dialogcontent.edit().putInt("which",27).commit();
                    break;
                case R.id.overspeed_led:
                    pre_dialogcontent.edit().putInt("which",28).commit();
                    break;
                case R.id.auxpowerlow_hw_led:
                    pre_dialogcontent.edit().putInt("which",29).commit();
                    break;
                case R.id.busov_hw_led:
                    pre_dialogcontent.edit().putInt("which",30).commit();
                    break;
                case R.id.mosot_hw_led:
                    pre_dialogcontent.edit().putInt("which",31).commit();
                    break;
                case R.id.faultcheck_hw_led:
                    pre_dialogcontent.edit().putInt("which",32).commit();
                    break;
                case R.id.interlock_led:
                    pre_dialogcontent.edit().putInt("which",33).commit();
                    break;
                case R.id.safty_led:
                    pre_dialogcontent.edit().putInt("which",34).commit();
                    break;
                case R.id.accerr_led:
                    pre_dialogcontent.edit().putInt("which",35).commit();
                    break;
                case R.id.accfault_led:
                    pre_dialogcontent.edit().putInt("which",36).commit();
                    break;
                case R.id.encodererr_led:
                    pre_dialogcontent.edit().putInt("which",37).commit();
                    break;
                case R.id.vcu_tout_led:
                    pre_dialogcontent.edit().putInt("which",38).commit();
                    break;
                case R.id.gearshifterr_led:
                    pre_dialogcontent.edit().putInt("which",39).commit();
                    break;
                case R.id.discharge_led:
                    pre_dialogcontent.edit().putInt("which",40).commit();
                    break;
                case R.id.overcurrent_led:
                    pre_dialogcontent.edit().putInt("which",41).commit();
                    break;
                case R.id.ocw_tout_led:
                    pre_dialogcontent.edit().putInt("which",42).commit();
                    break;
            }



            return true;
        }
    };

    public void makeSysInitChkFLG(int i, Button textview){

        if(d70Speration.getSysInitChkFLG()[i]){
            textview.setBackgroundResource(R.drawable.warningred);
        }
        else{
            textview.setBackgroundResource(R.drawable.warninggreen);
        }


    }
    public void makeProtectionFLG(int i, Button textview){

        if(d70Speration.getProtectionFLG()[i]){
            textview.setBackgroundResource(R.drawable.warningred);
        }
        else{
            textview.setBackgroundResource(R.drawable.warninggreen);
        }


    }
    public void makeHWProtectFLG(int i, Button textview){

        if(d70Speration.getProtection2FLG()[i]){
            textview.setBackgroundResource(R.drawable.warningred);
        }
        else{
            textview.setBackgroundResource(R.drawable.warninggreen);
        }


    }



    /*private class e extends Thread{
        boolean d = false;
        int a = 0;
        @Override
        public void run() {
                super.run();

            while (true){

                a++;
                String d1 = Integer.toBinaryString(a);
                for(int a1 = 0 ; a1 <d1.length(); a1++) {
                    flag1[a1] = d1.startsWith("1",a1);
                }

                BTPoccess.obtainMessage(1, 0, 0, d).sendToTarget();

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
     }*/

}


