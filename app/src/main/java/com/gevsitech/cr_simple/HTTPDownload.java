package com.gevsitech.cr_simple;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.content.Context;

public class HTTPDownload
{
    private URL url = null;
	private Context context;
	private String path;
	public HTTPDownload(String path){
		this.path = path;
	}

    /**
     * 
     * 下載網路檔案的方法
     * 根據URL下載檔案，前提是這個檔案當中的內容是文本，函數的返回值就是檔案當中的內容
     * 步驟:
     * 1.建立一個URL物件
     * 2.通過URL物件，建立一個HttpURLConnection物件
     * 3.得到InputStram
     * 4.從InputStream當中讀取數據
     * 
     * -1：代表下載檔案出錯
     * 0：代表下載檔案成功
     * 1：代表檔案已經存在
     */
    public int downFile(String urlStr, String path, String fileName)
    {
	InputStream inputStream = null;
	try
	{
	    FileUtils fileUtils = new FileUtils();

	    if (fileUtils.isFileExist(path + fileName))
	    {
		boolean is = fileUtils.isFileExistDelete(path + fileName);

		//存在的話返回 1：代表檔案已經存在
		if (!is) 	{
			return 1;}
	    }
	    inputStream = getInputStreamFromUrl(urlStr);
	    //將輸入流寫入到記憶卡
	    File resultFile = fileUtils.write2SDFromInput(path, fileName, inputStream);
	    if (resultFile == null)
	    {
		return -1;
	    }
	}
	catch (Exception e)
	{
		System.out.println("123");
	    e.printStackTrace();
	    return -1;
	}
	finally
	{
	    try
	    {
		inputStream.close();
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
	return 0;
    }

    /**
     * 根據URL得到輸入流
     */
    public InputStream getInputStreamFromUrl(String urlStr) throws MalformedURLException, IOException
    {
	// 建立一個URL物件 
	url = new URL(urlStr);
	// 建立一個Http連接
	HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
	//得到輸入流
	InputStream inputStream = urlConn.getInputStream();
	return inputStream;
    }

    public class FileUtils
    {
	private String SDPATH;

	public String getSDPATH()
	{
	    return SDPATH;
	}

	public FileUtils()
	{
	    //得到當前SDCARD存儲設備的目錄 /SDCARD, Environment.getExternalStorageDirectory()這個方法比較通用
		//this.context.getExternalFilesDir(null);
		SDPATH = path;
	}

	/**
	 * 在SD卡上建立文件
	 */
	public File creatSDFile(String fileName) throws IOException
	{
		//System.out.println( fileName);
		System.out.println(path );
	    File file = new File(path +File.separator +fileName);
	    file.createNewFile();
	    return file;
	}

	/**
	 * 在SD卡上建立目錄
	 */
	public File creatSDDir(String dirName)
	{
	    File dir = new File(path+File.separator + dirName);
	    dir.mkdir();
	    return dir;
	}

	/**
	 * 判斷SD卡上的文件夾是否存在
	 */
	public boolean isFileExist(String fileName)
	{
	    File file = new File(fileName);
	    return file.exists();
	}

	/**
	 * 判斷SD卡上的文件夾是否存在且刪除
	 */
	public boolean isFileExistDelete(String fileName)
	{
	    File file = new File(fileName);
	    return file.delete();
	}

	/**
	 * 將一個InputStream裡面的數據寫入到SD卡中
	 */
	public File write2SDFromInput(String path, String fileName, InputStream input)
	{
	    File file = null;
	    OutputStream output = null;
	    try
	    //InputStream裡面的數據寫入到SD卡中的固定方法
	    {
		creatSDDir(path);
		file = creatSDFile(fileName);
		output = new FileOutputStream(file);
		byte buffer[] = new byte[4 * 1024];
		while ((input.read(buffer)) != -1)
		{
		    output.write(buffer);
		}
		output.flush();
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	    finally
	    {
		try
		{
		    output.close();
		}
		catch (Exception e)
		{
		    e.printStackTrace();
		}
	    }
	    return file;
	}
    }
}