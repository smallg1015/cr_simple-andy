package com.gevsitech.cr_simple;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gevsitech.cr_simple.data_separation.PrimaevalData;
import com.gevsitech.cr_simple.data_separation.ReadCheck;
import com.gevsitech.cr_simple.data_separation.SettingCheck;
import com.gevsitech.cr_simple.data_separation.WriteCheck;

import java.io.File;
import java.text.NumberFormat;
import java.util.Calendar;

import static android.media.CamcorderProfile.get;

public class CR_response extends AppCompatActivity {

    private SeekBar res_sBar1,res_sBar2,res_sBar3,res_sBar4,res_sBar5,res_sBar6,res_sBar7,res_sBar8;
    private EditText res_edit1,res_edit2,res_edit3,res_edit4,res_edit5,res_edit6,res_edit7,res_edit8;
    private Button res_btn1,res_btn2,res_btn3,res_btn4,res_btn5,res_btn6,res_btn7,res_btn8,crresponse_btnsetting;
    private String st1,st2,st3,st4,st5,st6;
    private String[] hex=new String[]{"86,00","87,00","88,00","89,00","8A,00","8B,00","8C,00"};
    private Boolean Sendflag = true;
    private volatile Boolean checkflag = true;
    private ProgressDialog dialog;
    private int SendCount = 1 ;
    private PrimaevalData primaevalData = new PrimaevalData();
    private ReadCheck readCheck  = new ReadCheck();
    private WriteCheck writeCheck = new WriteCheck();
    private SettingCheck settingCheck = new SettingCheck();
    BluetoothService BluetoothS = BluetoothService.getBluetoothService();
    private float save_edit1,save_edit2,save_edit3,save_edit4,save_edit5,save_edit6,save_edit7,save_edit8;
    private boolean changenumber=true;
    private ImageButton crresponsebacktoset;
    NumberFormat nf;


    //活動紀錄
    LogService logService = LogService.getLogService();
    private File LogPath;
    String logFileName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cr_response);
        init();
        BluetoothS.setBtHandler(BTPoccess);

        LogPath = this.getExternalFilesDir(null);
        //取得今日日期
        Calendar mCal = Calendar.getInstance();
        CharSequence ss = DateFormat.format("yyyy-MM-dd", mCal.getTime());    // kk:24小時制, hh:12小時制
        logFileName = String.valueOf(ss)+".txt";


        //設定顯示小數位數
        nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits( 5 );    //小數後五位




        logService.SaveLog(LogPath, logFileName, "START CR-response");


        if(BluetoothS.isConnectedCheck()){

            dialog = ProgressDialog.show( CR_response.this,
                    "讀取初始資料中", "請稍待片刻...",true);
            SendCount = 1 ;
            Sendflag  = true;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(Sendflag){
                        try {
                            Thread.sleep(200);
                        }catch (Exception e){

                        }
                        switch (SendCount) {

                            case 1 :
                                //寫入1
                                char[] s1 = settingCheck.Send("01CC");
                                if(checkflag){
                                    BluetoothS.write2(s1);
                                    SendCount = 2 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入25%");
                                }
                                break;
                            case 2 :
                                char[] s2 = settingCheck.Send("01CD");
                                if(checkflag){
                                    BluetoothS.write2(s2);
                                    SendCount = 3 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入50%");
                                }
                                break;
                            case 3 :
                                char[] s3 = settingCheck.Send("01CE");
                                if(checkflag){
                                    BluetoothS.write2(s3);
                                    SendCount = 4 ;
                                    checkflag = false;
                                    // dialog.setMessage("寫入75%");
                                }
                                break;
                            case 4 :
                                char[] s4 = settingCheck.Send("01CF");
                                if(checkflag){
                                    BluetoothS.write2(s4);
                                    SendCount = 5 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入75%");
                                }
                                break;
                            case 5 :
                                char[] s5 = settingCheck.Send("01D0");
                                if(checkflag){
                                    BluetoothS.write2(s5);
                                    SendCount = 6 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 6 :
                                char[] s6 = settingCheck.Send("01D1");
                                if(checkflag){
                                    BluetoothS.write2(s6);
                                    SendCount = 7 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 7 :
                                char[] s7 = settingCheck.Send("01D2");
                                if(checkflag){
                                    BluetoothS.write2(s7);
                                    SendCount = 8 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 8 :
                                char[] s8 = settingCheck.Send("01D3");
                                if(checkflag){
                                    BluetoothS.write2(s8);
                                    SendCount = 0 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 0 :
                                if(checkflag) {
                                    Sendflag = false;
                                    break;
                                }
                        }

                    }
                    BTPoccess.obtainMessage(5).sendToTarget();

                }
            });
            thread.start();
        }else{
            Toast.makeText( CR_response.this,"藍芽尚未連線，無法讀取資料",Toast.LENGTH_SHORT).show();
        }

        crresponsebacktoset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        res_edit1.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(res_edit1.getText().toString().length() > 0) {
                        save_edit1=Float.parseFloat(res_edit1.getText().toString());
                        System.out.println("res_edit1.getText().toString()="+res_edit1.getText().toString());
                        changenumber=false;
                        res_sBar1.setProgress((int)(Float.parseFloat(res_edit1.getText().toString())*10.00000f*100.00000f));
                        System.out.println("res_edit1.getText().toString()="+Float.parseFloat(res_edit1.getText().toString())*10*100);
                    }
                }
                return false;
            }
        });

        res_edit2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(res_edit2.getText().toString().length() > 0) {
                        save_edit2=Float.parseFloat(res_edit2.getText().toString());
                        changenumber=false;
                        res_sBar2.setProgress((int)(Float.parseFloat(res_edit2.getText().toString())*10.00000f*100.00000f));
                    }
                }
                return false;
            }
        });
        res_edit3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(res_edit3.getText().toString().length() > 0) {
                        save_edit3=Float.parseFloat(res_edit3.getText().toString());
                        changenumber=false;
                        res_sBar3.setProgress((int)(Float.parseFloat(res_edit3.getText().toString())*10.00000f*100.00000f));
                    }
                }
                return false;
            }
        });
        res_edit4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(res_edit4.getText().toString().length() > 0) {
                        save_edit4=Float.parseFloat(res_edit4.getText().toString());
                        changenumber=false;
                        res_sBar4.setProgress((int)(Float.parseFloat(res_edit4.getText().toString())*10.00000f*100.00000f));
                    }
                }
                return false;
            }
        });
        res_edit5.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(res_edit5.getText().toString().length() > 0) {
                        save_edit5=Float.parseFloat(res_edit5.getText().toString());
                        changenumber=false;
                        res_sBar5.setProgress((int)(Float.parseFloat(res_edit5.getText().toString())*10.00000f*100.00000f));
                    }
                }
                return false;
            }
        });
        res_edit6.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(res_edit6.getText().toString().length() > 0) {
                        save_edit6=Float.parseFloat(res_edit6.getText().toString());
                        changenumber=false;
                        res_sBar6.setProgress((int)(Float.parseFloat(res_edit6.getText().toString())*10.00000f*100.00000f));
                    }
                }
                return false;
            }
        });
        res_edit7.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(res_edit7.getText().toString().length() > 0) {
                        save_edit7=Float.parseFloat(res_edit7.getText().toString());
                        changenumber=false;
                        res_sBar7.setProgress((int)(Float.parseFloat(res_edit7.getText().toString())*10.00000f*100.00000f));
                    }
                }
                return false;
            }
        });
        res_edit8.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(res_edit8.getText().toString().length() > 0) {
                        save_edit6=Float.parseFloat(res_edit8.getText().toString());
                        changenumber=false;
                        res_sBar8.setProgress((int)(Float.parseFloat(res_edit8.getText().toString())*10.00000f*100.00000f));
                    }
                }
                return false;
            }
        });

        res_btn1.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(CR_response .this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s1 =settingCheck.SendWithData("01CC" ,(short)(Float.valueOf(res_edit1.getText().toString())*100000));

                                    if(checkflag){
                                        BluetoothS.write2(s1);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "前進檔加速率(%)"+res_edit1.getText().toString());
            }



        });
        res_btn2.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(CR_response .this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s1 =settingCheck.SendWithData("01CD" ,(short)(Float.valueOf(res_edit2.getText().toString())*100000));

                                    if(checkflag){
                                        BluetoothS.write2(s1);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "前進檔減速率(%)"+res_edit2.getText().toString());
            }
        });
        res_btn3.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(CR_response .this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s1 =settingCheck.SendWithData("01CE" ,(short)(Float.valueOf(res_edit3.getText().toString())*100000));

                                    if(checkflag){
                                        BluetoothS.write2(s1);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "後退檔加速率(%)"+res_edit3.getText().toString());
            }

        });
        res_btn4.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(CR_response .this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s1 =settingCheck.SendWithData("01CF" ,(short)(Float.valueOf(res_edit4.getText().toString())*100000));

                                    if(checkflag){
                                        BluetoothS.write2(s1);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "後退檔減速率(%)"+res_edit4.getText().toString());
            }
        });
        res_btn5.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(CR_response .this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s1 =settingCheck.SendWithData("01D0" ,(short)(Float.valueOf(res_edit5.getText().toString())*100000));

                                    if(checkflag){
                                        BluetoothS.write2(s1);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "檔位變換加/減速率(%)"+res_edit5.getText().toString());
            }
        });
        res_btn6.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(CR_response .this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s1 =settingCheck.SendWithData("01D1" ,(short)(Float.valueOf(res_edit6.getText().toString())*100000));

                                    if(checkflag){
                                        BluetoothS.write2(s1);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "剎車檔加/減速率(%)"+res_edit6.getText().toString());
            }
        });
        res_btn7.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(CR_response .this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s1 =settingCheck.SendWithData("01D2" ,(short)(Float.valueOf(res_edit7.getText().toString())*100000));

                                    if(checkflag){
                                        BluetoothS.write2(s1);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "剎車檔加/減速率(%)"+res_edit7.getText().toString());
            }
        });

        res_btn8.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(CR_response .this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s1 =settingCheck.SendWithData("01D3" ,(short)(Float.valueOf(res_edit8.getText().toString())*100000));

                                    if(checkflag){
                                        BluetoothS.write2(s1);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "加減速率默認值"+res_edit8.getText().toString());
            }
        });

        crresponse_btnsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = ProgressDialog.show(CR_response.this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s1_e=settingCheck.SendEEPROMData("01CC");

                                    if(checkflag){
                                        BluetoothS.write2(s1_e);
                                        SendCount = 2 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 2 :
                                    char[] s2_e=settingCheck.SendEEPROMData("01CD");
                                    if(checkflag){
                                        BluetoothS.write2(s2_e);
                                        SendCount = 3 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 3 :
                                    char[] s3_e=settingCheck.SendEEPROMData("01CE");
                                    if(checkflag){
                                        BluetoothS.write2(s3_e);
                                        SendCount = 4 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 4 :
                                    char[] s4_e=settingCheck.SendEEPROMData("01CF");
                                    if(checkflag){
                                        BluetoothS.write2(s4_e);
                                        SendCount = 5 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 5 :
                                    char[] s5_e=settingCheck.SendEEPROMData("01D0");
                                    if(checkflag){
                                        BluetoothS.write2(s5_e);
                                        SendCount = 6 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 6 :
                                    char[] s6_e=settingCheck.SendEEPROMData("01D1");
                                    if(checkflag){
                                        BluetoothS.write2(s6_e);
                                        SendCount = 7 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 7 :
                                    char[] s7_e=settingCheck.SendEEPROMData("01D2");
                                    if(checkflag){
                                        BluetoothS.write2(s7_e);
                                        SendCount = 8 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 8 :
                                    char[] s8_e=settingCheck.SendEEPROMData("01D3");
                                    if(checkflag){
                                        BluetoothS.write2(s8_e);
                                        SendCount = 0 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                    }
                                    break;

                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();

            }
        });
    }

    private SeekBar.OnSeekBarChangeListener SBL1=new SeekBar.OnSeekBarChangeListener(){
        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {
            //停止拖曳時觸發事件
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {
            //開始拖曳時觸發事件
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        {
            if(changenumber){
                res_edit1.setText(nf.format(res_sBar1.getProgress()/100.00000f/10.00000f)+"");
            }else{
                res_edit1.setText(save_edit1+"");
                changenumber=true;

            }


        }
    };
    private SeekBar.OnSeekBarChangeListener SBL2=new SeekBar.OnSeekBarChangeListener(){
        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {
            //停止拖曳時觸發事件
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {
            //開始拖曳時觸發事件
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        {
            if(changenumber){
                res_edit2.setText(nf.format(res_sBar2.getProgress()/100.00000f/10.00000f)+"");
            }else{
                res_edit2.setText(save_edit2+"");
                changenumber=true;

            }
        }
    };
    private SeekBar.OnSeekBarChangeListener SBL3=new SeekBar.OnSeekBarChangeListener(){
        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {
            //停止拖曳時觸發事件
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {
            //開始拖曳時觸發事件
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        {
            if(changenumber){
                res_edit3.setText(nf.format(res_sBar3.getProgress()/100.00000f/10.00000f)+"");
            }else{
                res_edit3.setText(save_edit3+"");
                changenumber=true;

            }
        }
    };
    private SeekBar.OnSeekBarChangeListener SBL4=new SeekBar.OnSeekBarChangeListener(){
        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {
            //停止拖曳時觸發事件
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {
            //開始拖曳時觸發事件
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        {
            if(changenumber){
                res_edit4.setText(nf.format(res_sBar4.getProgress()/100.00000f/10.00000f)+"");
            }else{
                res_edit4.setText(save_edit4+"");
                changenumber=true;

            }
        }
    };
    private SeekBar.OnSeekBarChangeListener SBL5=new SeekBar.OnSeekBarChangeListener(){
        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {
            //停止拖曳時觸發事件
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {
            //開始拖曳時觸發事件
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        {
            if(changenumber){
                res_edit5.setText(nf.format(res_sBar5.getProgress()/100.00000f/10.00000f)+"");
            }else{
                res_edit5.setText(save_edit5+"");
                changenumber=true;

            }
        }
    };
    private SeekBar.OnSeekBarChangeListener SBL6=new SeekBar.OnSeekBarChangeListener(){
        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {
            //停止拖曳時觸發事件
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {
            //開始拖曳時觸發事件
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        {

            if(changenumber){
                res_edit6.setText(nf.format(res_sBar6.getProgress()/100.00000f/10.00000f)+"");
            }else{
                res_edit6.setText(save_edit6+"");
                changenumber=true;

            }
        }
    };
    private SeekBar.OnSeekBarChangeListener SBL7=new SeekBar.OnSeekBarChangeListener(){
        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {
            //停止拖曳時觸發事件
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {
            //開始拖曳時觸發事件
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        {

            if(changenumber){
                res_edit7.setText(nf.format(res_sBar7.getProgress()/100.00000f/10.00000f)+"");
            }else{
                res_edit7.setText(save_edit7+"");
                changenumber=true;

            }
        }
    };
    private SeekBar.OnSeekBarChangeListener SBL8=new SeekBar.OnSeekBarChangeListener(){
        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {
            //停止拖曳時觸發事件
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {
            //開始拖曳時觸發事件
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        {

            if(changenumber){
                res_edit8.setText(nf.format(res_sBar8.getProgress()/100.00000f/10.00000f)+"");
            }else{
                res_edit8.setText(save_edit8+"");
                changenumber=true;

            }
        }
    };
    private void init(){
        res_edit1=(EditText) findViewById(R.id.res_edit1);
        res_edit2=(EditText) findViewById(R.id.res_edit2);
        res_edit3=(EditText) findViewById(R.id.res_edit3);
        res_edit4=(EditText) findViewById(R.id.res_edit4);
        res_edit5=(EditText) findViewById(R.id.res_edit5);
        res_edit6=(EditText) findViewById(R.id.res_edit6);
        res_edit7=(EditText) findViewById(R.id.res_edit7);
        res_edit8=(EditText) findViewById(R.id.res_edit8);
        res_sBar1=(SeekBar) findViewById(R.id.res_sBar1);
        res_sBar2=(SeekBar) findViewById(R.id.res_sBar2);
        res_sBar3=(SeekBar) findViewById(R.id.res_sBar3);
        res_sBar4=(SeekBar) findViewById(R.id.res_sBar4);
        res_sBar5=(SeekBar) findViewById(R.id.res_sBar5);
        res_sBar6=(SeekBar) findViewById(R.id.res_sBar6);
        res_sBar7=(SeekBar) findViewById(R.id.res_sBar7);
        res_sBar8=(SeekBar) findViewById(R.id.res_sBar8);
        res_btn1=(Button)findViewById(R.id.res_btn1);
        res_btn2=(Button)findViewById(R.id.res_btn2);
        res_btn3=(Button)findViewById(R.id.res_btn3);
        res_btn4=(Button)findViewById(R.id.res_btn4);
        res_btn5=(Button)findViewById(R.id.res_btn5);
        res_btn6=(Button)findViewById(R.id.res_btn6);
        res_btn7=(Button)findViewById(R.id.res_btn7);
        res_btn8=(Button)findViewById(R.id.res_btn8);
        crresponse_btnsetting=(Button)findViewById(R.id.crresponse_btnsetting);
        crresponsebacktoset=(ImageButton)findViewById(R.id.responsebacktoset);


        res_sBar1.setOnSeekBarChangeListener(SBL1);
        res_sBar2.setOnSeekBarChangeListener(SBL2);
        res_sBar3.setOnSeekBarChangeListener(SBL3);
        res_sBar4.setOnSeekBarChangeListener(SBL4);
        res_sBar5.setOnSeekBarChangeListener(SBL5);
        res_sBar6.setOnSeekBarChangeListener(SBL6);
        res_sBar7.setOnSeekBarChangeListener(SBL7);
        res_sBar8.setOnSeekBarChangeListener(SBL8);

    }
    private Handler BTPoccess = new Handler(){
        private StringPoccess poccess = new StringPoccess();

        private String data ;

        private boolean tagflag = false;

        /**
         *  連線中狀態值
         *  1. DISCONNECT 斷線狀態    0
         *  2. CONNECTED  連線中      1
         *  3. CONNECTING 嘗試連線中  2
         **/
        private final static int DISCONNECT = 0 ;
        private final static int CONNECTED = 1 ;
        private final static int CONNECTING = 2 ;

        /**
         *  連線中狀態值
         *  1. DATAWAIT   等待資料中    0
         *  2. DATAREAD   資料讀取      1
         *  3. DATAWIRTE  資料寫入      2
         **/

        private final static int DATAWAIT = 0 ;
        private final static int DATAREAD = 1 ;
        private final static int DATAWIRTE = 2 ;

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case DISCONNECT :
                    if(tagflag) Log.d("Handle" ,"DISCONNECT" );
                    break;
                case CONNECTED :
                    if(tagflag)Log.d("Handle" ,"CONNECTED" );

                    if(msg.arg1 == DATAREAD) {
                        data = (String) msg.obj;
                        if(data == null)return;
                        System.out.println(data);
                        primaevalData.split(data);
                    }

                    if(primaevalData.startcheck()) {
                        if (primaevalData.getId()[2]==0xCE) {
                            checkflag = true;
                            writeCheck.setIDCheck(primaevalData.getdata());
                            if (writeCheck.CheckRAMWrite()) {
                                checkflag = true;
                            }if (writeCheck.CheckEEPROMWrite()) {
                                checkflag = true;
                            }
                        }

                        if (primaevalData.getId()[2]==0xCF) {
                            readCheck.setData(primaevalData.getdata());
                            if (readCheck.getAddress().equals("1cc")) {
                                Float a = (float) readCheck.getValues() / 100000.00000f;
                                String s = String.valueOf(a);
                                int b = (int) (a*10.00000f*100.00000f);
                                res_sBar1.setProgress(b);
                                res_edit1.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("1cd")) {
                                Float a = (float) readCheck.getValues() / 100000.00000f;
                                String s = String.valueOf(a);
                                int b = (int) (a*10.00000f*100.00000f);
                                res_sBar2.setProgress(b);
                                res_edit2.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("1ce")) {
                                Float a = (float) readCheck.getValues() / 100000.00000f;
                                String s = String.valueOf(a);
                                int b = (int) (a*10.00000f*100.00000f);
                                res_sBar3.setProgress(b);
                                res_edit3.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("1cf")) {
                                checkflag = true;
                                Float a = (float) readCheck.getValues() / 100000.00000f;
                                String s = String.valueOf(a);
                                int b = (int) (a*10.00000f*100.00000f);
                                res_sBar4.setProgress(b);
                                res_edit4.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("1d0")) {
                                Float a = (float) readCheck.getValues() / 100000.00000f;
                                String s = String.valueOf(a);
                                int b = (int) (a*10.00000f*100.00000f);
                                res_sBar5.setProgress(b);
                                res_edit5.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("1d1")) {
                                Float a = (float) readCheck.getValues() / 100000.00000f;
                                String s = String.valueOf(a);
                                int b = (int) (a*10.00000f*100.00000f);
                                res_sBar6.setProgress(b);
                                res_edit6.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("1d2")) {
                                Float a = (float) readCheck.getValues() / 100000.00000f;
                                String s = String.valueOf(a);
                                int b = (int) (a*10.00000f*100.00000f);
                                res_sBar7.setProgress(b);
                                res_edit7.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            }else if (readCheck.getAddress().equals("1d3")) {
                                Float a = (float) readCheck.getValues() / 100000.00000f;
                                String s = String.valueOf(a);
                                int b = (int) (a*10.00000f*100.00000f);
                                res_sBar8.setProgress(b);
                                res_edit8.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            }
                            else {
                                checkflag = false;
                            }
                        }
                    }

                    break;
                case CONNECTING :
                    if(tagflag)Log.d("Handle" ,"CONNECTING" );
                    break;
                case 5:
                    try {
                        dialog.dismiss();
                    }catch (Exception e){
                        dialog.dismiss();
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };

}



