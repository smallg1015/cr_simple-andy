package com.gevsitech.cr_simple;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.gevsitech.cr_simple.data_separation.DF1Separation;
import com.gevsitech.cr_simple.data_separation.PrimaevalData;
import com.gevsitech.cr_simple.data_separation.SaveCheck;
import com.gevsitech.cr_simple.data_separation.SaveData;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;



public class MainActivity extends AppCompatActivity {

    BluetoothService bluetoothService = BluetoothService.getBluetoothService();

    private SharedPreferences prebluetooth;
    private SharedPreferences preautoblue;
    private SharedPreferences preautobluename;

    private static boolean data_Tran_OK =false;

    private ImageButton bluebutton;
    private TextView txt_bluestate;
    private TextView txt_mainstate;

    //fragement 參數
    private FragmentManager fm;
    //FragmentLayout ID
    //所有的Fragment集合
    private ArrayList<Fragment> fragments;

    private BottomBar bottomBar;
    private SharedPreferences predrivesetcan;









    //藍芽連線配對參數
    private String[] deviceString;
    private String[] deviceMAC;
    private int BT_count;

    private File LogPath;


    //存取接收資料參數
    private SaveData saveData=new SaveData();
    private SaveCheck saveCheck=new SaveCheck();
    private Boolean Savelag;
    DF1Separation df1Separation=new DF1Separation();
    PrimaevalData primaevalData = new PrimaevalData();
    private AHBottomNavigation bottomNavigation;

    //重啟程式保存參數
    private int WhatFragment;


    private int A;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        predrivesetcan=getSharedPreferences("predrivesetcan",MODE_PRIVATE);


       //建立程式資料夾
        LogPath = new File(Environment.getExternalStorageDirectory().getPath()+"/CR_simple");

        if(!LogPath.exists()){
            LogPath.mkdirs();
        }



//        //建立下方主選單
//         bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
//
//
//        // Create items
//        AHBottomNavigationItem item1 = new AHBottomNavigationItem("首頁 ", R.drawable.home, R.color.white);
//        AHBottomNavigationItem item2 = new AHBottomNavigationItem("驅動器狀態", R.drawable.drivestate, R.color.white);
//        AHBottomNavigationItem item3 = new AHBottomNavigationItem("錯誤偵測", R.drawable.warning, R.color.white);
//        AHBottomNavigationItem item4 = new AHBottomNavigationItem("驅動器設定", R.drawable.driveset, R.color.white);
//        AHBottomNavigationItem item5 = new AHBottomNavigationItem("設定", R.drawable.settingicon, R.color.white);
//        AHBottomNavigationItem item6 = new AHBottomNavigationItem("檔案",R.drawable.play,R.color.white);
//
//
//
//
//        // Add items
//        bottomNavigation.addItem(item1);
//        bottomNavigation.addItem(item2);
//        bottomNavigation.addItem(item3);
//        bottomNavigation.addItem(item4);
//        bottomNavigation.addItem(item5);
//
//
//        // Set background color
//        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#747474"));
//        bottomNavigation.setInactiveColor(Color.parseColor("#FFFFFF"));
//        bottomNavigation.setAccentColor(Color.parseColor("#FF5BE824"));
//
//
//        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomBar=(BottomBar)findViewById(R.id.bottomBar);

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                WhatFragment=tabId;
                switch(tabId){
                    case R.id.bottom_home:
                        showFragment(new MainFragment());
                        break;
                    case R.id.bottom_state:
                        showFragment(new DriverStateActivity());
                        break;
                    case R.id.bottom_file:
                        showFragment(new FileFuctionFragment());
                        break;
                    case R.id.bottom_warning:
                        showFragment(new WarningActivity());
                        break;
                    case R.id.bottom_driveset:
                        if(predrivesetcan.getBoolean("drivesetting",false)){
                            System.out.println("drivecan"+predrivesetcan.getBoolean("drivesetting",false));
                            showFragment(new CRSetting());
                        }else{
                            Toast.makeText(MainActivity.this,"請至設定頁面取得授權",Toast.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.bottom_setting:
                        showFragment(new Setting());
                        break;



                }
            }
        });



//        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
//            @Override
//            public boolean onTabSelected(int position, boolean wasSelected) {
//                WhatFragment=position;
//                switch (position) {
//                    case 0: {
//                        showFragment(new MainFragment());
//                    }
//                    break;
//                    case 1: {
//                        showFragment(new DriverStateActivity());
//
//                    }
//                    break;
//                    case 2: {
//                        showFragment(new WarningActivity());
//                    }
//                    break;
//                    case 3: {
//                        showFragment(new CRSetting());
//
//                    }
//                    break;
//                    case 4: {
//                        showFragment(new Setting());
//
//
//                    }
//                    break;
//                }
//                // Do something cool here...
//                return true;
//            }
//        });
    }


    //獲取暫存UI狀態
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        int WFragment=savedInstanceState.getInt("Fragement");
        switch (WFragment){
            case R.id.bottom_home: {
                showFragment(new MainFragment());
            }
            break;
            case R.id.bottom_state: {
                showFragment(new DriverStateActivity());

            }
            break;
            case R.id.bottom_warning: {
                showFragment(new WarningActivity());
            }
            break;
            case R.id.bottom_driveset: {
                showFragment(new CRSetting());

            }
            break;
            case R.id.bottom_setting: {
                showFragment(new Setting());


            }
            break;
        }
    }


    //重寫螢幕旋轉事件
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    //显示Fragment
    private void showFragment(Fragment fragment) {
        //先隐藏
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.center,fragment);
        ft.commit();
    }


    //暫存UI狀態
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putInt("Fragement",WhatFragment);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(!(bluetoothAdapter==null)){
            if(!(bluetoothAdapter.isEnabled()))bluetoothAdapter.enable();
            //Toast.makeText(EvtActivity.this, "自動開啟藍芽", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(MainActivity.this, "Your Cell Phone not suppose Bluetooth", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onDestroy(){
        super.onDestroy();
        bluetoothService.DisConnect();
        System.out.println("onDestroy act");

    }






//    private void initFragment() {
//        fragments = new ArrayList<Fragment>();
//        //添加自己的Fragment
//        fragments.add(new MainFragment());
//        fragments.add(new CRSetting());
//        fragments.add(new Setting());
//        FragmentTransaction ft=fm.beginTransaction();
//        for(Fragment fragment:fragments){
//            ft.add(R.id.center,fragment);
//        }
//        ft.commit();
//
//
//    }




    //隐藏所有的Fragment
//    private void hideFragments() {
//        FragmentTransaction ft = fm.beginTransaction();
//        for (Fragment fragment : fragments) {
//            if (fragment != null) {
//                ft.hide(fragment);
//            }
//        }
//        ft.commit();
//    }
















}
