package com.gevsitech.cr_simple;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.gevsitech.cr_simple.data_separation.D70Speration;
import com.gevsitech.cr_simple.data_separation.DF1Separation;
import com.gevsitech.cr_simple.data_separation.PrimaevalData;
import com.gevsitech.cr_simple.data_separation.SaveCheck;
import com.gevsitech.cr_simple.data_separation.SaveData;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;

import static android.content.Context.MODE_PRIVATE;


public class MainFragment extends Fragment {

    BluetoothService bluetoothService = BluetoothService.getBluetoothService();
    private SharedPreferences prebluetooth;
    private SharedPreferences preautoblue;
    private SharedPreferences preautobluename;
    private static boolean data_Tran_OK =false;
    private volatile boolean Canfaultlag=false;

    private ImageButton bluebutton,btn_enginestate;
    private TextView txt_bluestate;
    private TextView txt_mainstate;

    D70Speration d70Speration=new D70Speration();//偵測錯誤資料解析

    //fragement 參數
    private FragmentManager fm;
    //FragmentLayout ID
    //所有的Fragment集合
    private ArrayList<Fragment> fragments;

    //藍芽連線配對參數
    private String[] deviceString;
    private String[] deviceMAC;
    private int BT_count;
    private  static boolean linkflag;
    private static boolean autolinkflag=true;

    private File LogPath,fileexist;
    String logFileName;
    String SaveFileName;
    LogService logService = LogService.getLogService();
    private String downFilename;

    //存取接收資料參數
    private SaveData saveData=new SaveData();
    private SaveCheck saveCheck=new SaveCheck();
    private Boolean Savelag;
    DF1Separation df1Separation=new DF1Separation();
    PrimaevalData primaevalData = new PrimaevalData();




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState){
        super.onViewCreated(view,savedInstanceState);

        bluetoothService.setBtHandler(handler);




        txt_bluestate=(TextView)getView().findViewById(R.id.txt_bluestate);
        txt_mainstate=(TextView)getView().findViewById(R.id.txt_mainstate);
        bluebutton=(ImageButton)getView().findViewById(R.id.blueButton);
        btn_enginestate=(ImageButton) getView().findViewById(R.id.btn_enginestate);


        //取得今日日期
        LogPath = new File(Environment.getExternalStorageDirectory().getPath()+"/CR_simple");
        Calendar mCal = Calendar.getInstance();
        CharSequence ss = DateFormat.format("yyyy-MM-dd", mCal.getTime());    // kk:24小時制, hh:12小時制
        logFileName = String.valueOf(ss)+".txt";
        SaveFileName = String.valueOf(ss)+"saveData"+".txt";







        logService.SaveLog(LogPath, logFileName, "START APP");

        prebluetooth=getActivity().getSharedPreferences("prebluetooth",MODE_PRIVATE);
        preautoblue=getActivity().getSharedPreferences("autobluetooth",MODE_PRIVATE);
        preautobluename=getActivity().getSharedPreferences("preautobluename",MODE_PRIVATE);


        //自動連線地址有時,自動連線
        String autobluetoothadress=preautoblue.getString("autobluetoohadress","");
        if(!autobluetoothadress.equals("")){
            //目前尚未建立連線,自動連線
            if(autolinkflag){
            bluetoothService.StartConnect(autobluetoothadress);
        }}

        System.out.println(data_Tran_OK);






        //判定目前有無連線,顯示狀態,同時可回復原本畫面的狀態
        if(bluetoothService.isConnectedCheck()){
            bluebutton.setImageResource(R.drawable.bluetoothyes);
            bluebutton.setBackgroundResource(R.drawable.bluetoothstateyes);
            txt_bluestate.setText("連線裝置:"+preautobluename.getString("bluetoothname",""));
            data_Tran_OK=true;
        }else{
            bluebutton.setImageResource(R.drawable.bluetoothnot);
            bluebutton.setBackgroundResource(R.drawable.bluetoothstateno);
            txt_bluestate.setText("裝置未連線");
            data_Tran_OK=false;

        }




        bluebutton.setOnClickListener(new View.OnClickListener() {
            int i ;
            @Override
            public void onClick(View v) {
                if(!data_Tran_OK) {
                    BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
                    final Set<BluetoothDevice> devices = adapter.getBondedDevices();
                    i = 0;
                    if (devices.size() > 0) {
                        deviceString = new String[devices.size()];
                        deviceMAC = new String[devices.size()];
                        for (Iterator<BluetoothDevice> it = devices.iterator(); it.hasNext(); ) {
                            BluetoothDevice device = it.next();
                            deviceString[i] = device.getName();
                            deviceMAC[i] = device.getAddress();
                            ++i;
                        }

                    }

                    new AlertDialog.Builder(getActivity()).setTitle("選擇連線裝置").setIcon(
                            android.R.drawable.ic_dialog_info).setSingleChoiceItems(
                            deviceString, 0,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    BT_count = which;

                                }
                            }).setPositiveButton("連線", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //正式藍芽連線用途
                            bluetoothService.StartConnect(deviceMAC[BT_count]);
                            //儲存自動連線的藍芽地址
                            prebluetooth.edit()
                                    .putString("bluetoohaddress", deviceMAC[BT_count])
                                    .commit();
                            preautobluename.edit()
                                    .putString("bluetoothname",deviceString[BT_count])
                                    .commit();
                            logService.SaveLog(LogPath, logFileName, "Start Connect : " + deviceString[BT_count]);



                            //測試APP連線用途
                            //BluetoothS.StartSendDemo();
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                        }
                    }).setNeutralButton("配對", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto- generated method stub
                            Toast.makeText(getActivity(), "即將跳轉藍芽配對頁面", Toast.LENGTH_SHORT).show();
                            logService.SaveLog(LogPath, logFileName, "go to bluetooth pair page" + deviceMAC[BT_count]);
                            Intent settintIntent = new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
                            startActivity(settintIntent);
                        }
                    })/*.setNeutralButton("藍芽離線", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto- generated method stub
                            BluetoothS.DisConnect();
                }
                })*/.create().show();
                }else{
                    Toast.makeText(getActivity(),"現有藍芽連線中",Toast.LENGTH_SHORT).show();
                    bluetoothService.DisConnect();
                    logService.SaveLog(LogPath, logFileName, "bluetooth disconnect");
                    data_Tran_OK = false;
                    System.out.println(data_Tran_OK);
                }
            }
        });
    }



    private Handler handler = new Handler(){
        boolean tagflag = false;
        String  data ;
        /**
         *  連線中狀態值
         *  1. DISCONNECT 斷線狀態    0
         *  2. CONNECTED  連線中      1
         *  3. CONNECTING 嘗試連線中  2
         **/
        private final static int DISCONNECT = 0 ;
        private final static int CONNECTED = 1 ;
        private final static int CONNECTING = 2 ;

        /**
         *  連線中狀態值
         *  1. DATAWAIT   等待資料中    0
         *  2. DATAREAD   資料讀取      1
         *  3. DATAWIRTE  資料寫入      2
         **/

        private final static int DATAWAIT = 0 ;
        private final static int DATAREAD = 1 ;
        private final static int DATAWIRTE = 2 ;
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what){
                case DISCONNECT :
                    if(tagflag) Log.d("Handle" ,"DISCONNECT" );
                    txt_bluestate.setText("裝置未連線");
                    bluebutton.setBackgroundResource(R.drawable.bluetoothstateno);
                    bluebutton.setImageResource(R.drawable.bluetoothnot);
                    data_Tran_OK=false;
                    break;
                case CONNECTED :
                    //如成功連線，則拋出連線成功訊息，供使用者辨識
                    if(tagflag)Log.d("Handle" ,"CONNECTED" );
                    if(msg.arg1 == DATAREAD) {
                        data = (String) msg.obj;
                        System.out.println(data);
                        System.out.println("i have complete ");
                        primaevalData.split(data);
                    }

                    if(primaevalData.getId()[2]==0x70){
                        String normalstring="0000000000000000";
                        d70Speration.setD70(primaevalData.getdata());
                        if(!d70Speration.getSysInitestr().equals(normalstring)|!d70Speration.getProtectionstr().equals(normalstring)|
                                !d70Speration.getProtection2str().equals(normalstring)) {
                            btn_enginestate.setImageResource(R.drawable.enginefault);
                            btn_enginestate.setBackgroundResource(R.drawable.enginestatefalut);
                            txt_mainstate.setText("偵測到錯誤");
                        }else{
                            btn_enginestate.setImageResource(R.drawable.enginenormal);
                            btn_enginestate.setBackgroundResource(R.drawable.enginestatebk);
                            txt_mainstate.setText("狀態良好");

                        }
                    }


                    data_Tran_OK=true;

                    break;
                case CONNECTING :
                    if(tagflag)Log.d("Handle" ,"CONNECTING" );
                    break;
            }

            switch (msg.arg2){
                case 0x20 :
                    if(getActivity()!=null) {
                        Toast.makeText(getActivity(), "斷開藍芽連線", Toast.LENGTH_SHORT).show();
                    }
                    logService.SaveLog(LogPath, logFileName, "bluetooth disconnect");
                    linkflag = false;
                    txt_bluestate.setText("裝置未連線");
                    bluebutton.setBackgroundResource(R.drawable.bluetoothstateno);
                    bluebutton.setImageResource(R.drawable.bluetoothnot);
                    autolinkflag = true;
                    break;
                case 0x10 :
                    Toast.makeText(getActivity(),"藍芽連線成功",Toast.LENGTH_SHORT).show();
                    data_Tran_OK=true;
                    System.out.println(data_Tran_OK);
                    logService.SaveLog(LogPath, logFileName, "bluetooth success connect");
                    if(autolinkflag){
                        txt_bluestate.setText("連線裝置:"+preautobluename.getString("bluetoothname",""));
                    }
                    else {
                        txt_bluestate.setText("連線裝置:" + deviceString[BT_count]);
                    }
                    bluebutton.setBackgroundResource(R.drawable.bluetoothstateyes);
                    bluebutton.setImageResource(R.drawable.bluetoothyes);
                    linkflag=true;
                    Canfaultlag=true;
                    autolinkflag=false;
                    break;
                case 0x30 :
                    if(getActivity()!=null) {
                        Toast.makeText(getActivity(), "無法連線Can bus,請檢查", Toast.LENGTH_LONG).show();
                    }
                    txt_bluestate.setText("裝置未連線");
                    bluebutton.setBackgroundResource(R.drawable.bluetoothstateno);
                    bluebutton.setImageResource(R.drawable.bluetoothnot);
                    break;
            }
        }
    };




}
