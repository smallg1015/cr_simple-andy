package com.gevsitech.cr_simple;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Toast;

import com.gevsitech.cr_simple.data_separation.PrimaevalData;
import com.gevsitech.cr_simple.data_separation.ReadCheck;
import com.gevsitech.cr_simple.data_separation.SettingCheck;
import com.gevsitech.cr_simple.data_separation.WriteCheck;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Protection_moda extends AppCompatActivity {
    private EditText modaprotect_nm,modaprotect_lp,modaprotect_lr,modaprotect_hp,modaprotect_hr,modaprotect_lw,modaprotect_hw;
    private SeekBar modaprotect_sBar1,modaprotect_sBar2,modaprotect_sBar3,modaprotect_sBar4,modaprotect_sBar5,modaprotect_sBar6,modaprotect_sBar7;
    private Button modaprotect_btnsure,modaprotect_btnnext,modaprotect_btn1,modaprotect_btn2,modaprotect_btn3,modaprotect_btn4,modaprotect_btn5,modaprotect_btn6,modaprotect_btn7,modaprotect_btnsetting;
    private String nm,lp,lr,hp,hr,lw,hw;
    private String[] hex=new String[]{"9C,00","9D,00","9E,00","A0,00","A2,00","A3,00","9F,00"};
    private Boolean Sendflag = true;
    private volatile Boolean checkflag = true;
    private ProgressDialog dialog;
    private int SendCount = 1 ;
    private PrimaevalData primaevalData = new PrimaevalData();
    private ReadCheck readCheck  = new ReadCheck();
    private WriteCheck writeCheck = new WriteCheck();
    private SettingCheck settingCheck = new SettingCheck();
    BluetoothService BluetoothS = BluetoothService.getBluetoothService();
    private Boolean changenumber=true;
    private int save_nm;
    private float save_lp,save_lr,save_hp,save_hr,save_hw,save_lw;
    private ImageButton modabacktoset;

    private File LogPath;
    String logFileName;
    LogService logService = LogService.getLogService();
    private String downFilename;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_protection_moda);
        init();

        LogPath = this.getExternalFilesDir(null);
        //取得今日日期
        Calendar mCal = Calendar.getInstance();
        CharSequence ss = DateFormat.format("yyyy-MM-dd", mCal.getTime());    // kk:24小時制, hh:12小時制
        logFileName = String.valueOf(ss)+".txt";

        logService.SaveLog(LogPath, logFileName, "START Protection_moda");

        BluetoothS.setBtHandler(BTPoccess);
        if(BluetoothS.isConnectedCheck()){
            dialog = ProgressDialog.show( Protection_moda.this,
                    "讀取初始資料中", "請稍待片刻...",true);
            SendCount = 1 ;
            Sendflag  = true;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(Sendflag){
                        try {
                            Thread.sleep(200);
                        }catch (Exception e){

                        }
                        switch (SendCount) {

                            case 1 :
                                //寫入1
                                char[] s1 = settingCheck.Send("0081");
                                if(checkflag){
                                    BluetoothS.write2(s1);
                                    SendCount = 2 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入25%");
                                }
                                break;
                            case 2 :
                                char[] s2 = settingCheck.Send("0082");
                                if(checkflag){
                                    BluetoothS.write2(s2);
                                    SendCount = 3 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入50%");
                                }
                                break;
                            case 3 :
                                char[] s3 = settingCheck.Send("0083");
                                if(checkflag){
                                    BluetoothS.write2(s3);
                                    SendCount = 4 ;
                                    checkflag = false;
                                    // dialog.setMessage("寫入75%");
                                }
                                break;
                            case 4 :
                                char[] s4 = settingCheck.Send("0084");
                                if(checkflag){
                                    BluetoothS.write2(s4);
                                    SendCount = 5 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入75%");
                                }
                                break;
                            case 5 :
                                char[] s5 = settingCheck.Send("0085");
                                if(checkflag){
                                    BluetoothS.write2(s5);
                                    SendCount = 6 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 6 :
                                char[] s6 = settingCheck.Send("0086");
                                if(checkflag){
                                    BluetoothS.write2(s6);
                                    SendCount = 7 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 7 :
                                char[] s7 = settingCheck.Send("0087");
                                if(checkflag){
                                    BluetoothS.write2(s7);
                                    SendCount = 0 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 0 :
                                if(checkflag) {
                                    Sendflag = false;
                                    break;
                                }
                        }

                    }
                    BTPoccess.obtainMessage(5).sendToTarget();

                }
            });
            thread.start();


        }else{
            Toast.makeText( Protection_moda.this,"藍芽尚未連線，無法讀取資料",Toast.LENGTH_SHORT).show();
            modaprotect_btn1.setEnabled(false);
            modaprotect_btn2.setEnabled(false);
            modaprotect_btn3.setEnabled(false);
            modaprotect_btn4.setEnabled(false);
            modaprotect_btn5.setEnabled(false);
            modaprotect_btn6.setEnabled(false);
            modaprotect_btn7.setEnabled(false);
            modaprotect_btnsetting.setEnabled(false);
            logService.SaveLog(LogPath, logFileName, "bluetooth isn't connect");
        }


        modabacktoset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });




        modaprotect_nm.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(modaprotect_nm.getText().toString().length() > 0) {
                        int a =(int)(Integer.parseInt(modaprotect_nm.getText().toString())/65535.0f*100);
                        save_nm=Integer.parseInt(modaprotect_nm.getText().toString());
                        changenumber=false;
                        modaprotect_sBar1.setProgress(a);
                    }
                }
                return false;
            }
        });

        modaprotect_lp.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(modaprotect_lp.getText().toString().length() > 0) {
                        int a=(int)((Float.valueOf(modaprotect_lp.getText().toString())+50)/70.f*100);
                        save_lp=Float.valueOf(modaprotect_lp.getText().toString());
                        changenumber=false;
                        modaprotect_sBar2.setProgress(a);
                        System.out.println(a);
                    }
                }
                return false;
            }
        });
        modaprotect_lr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(modaprotect_lr.getText().toString().length() > 0) {
                        int a=(int)((Float.valueOf(modaprotect_lr.getText().toString())+50)/70.f*100);
                        save_lr=Float.valueOf(modaprotect_lr.getText().toString());
                        changenumber=false;
                        modaprotect_sBar3.setProgress(a);
                        System.out.println(a);
                    }
                }
                return false;
            }
        });
        modaprotect_lw.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(modaprotect_lw.getText().toString().length() > 0) {
                        int a=(int)(Float.valueOf(modaprotect_lw.getText().toString()+50)/70.f*100);
                        save_lw=Float.valueOf(modaprotect_lw.getText().toString());
                        changenumber=false;
                        modaprotect_sBar4.setProgress(a);
                    }
                }
                return false;
            }
        });
        modaprotect_hp.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(modaprotect_hp.getText().toString().length() > 0) {
                        int a=(int)(Float.valueOf(modaprotect_hp.getText().toString())/150.0f*100);
                        save_hp=Float.valueOf(modaprotect_hp.getText().toString());
                        changenumber=false;
                        modaprotect_sBar5.setProgress(a);
                    }
                }
                return false;
            }
        });
        modaprotect_hr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(modaprotect_hr.getText().toString().length() > 0) {
                        int a=(int)(Float.valueOf(modaprotect_hr.getText().toString())/150.0f*100);
                        save_hr=Float.valueOf(modaprotect_hr.getText().toString());
                        changenumber=false;
                        modaprotect_sBar6.setProgress(a);
                    }
                }
                return false;
            }
        });
        modaprotect_hw.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(modaprotect_hw.getText().toString().length() > 0) {
                        int a=(int)((Float.valueOf(modaprotect_hw.getText().toString()))/150.0f*100);
                        save_hw=Float.valueOf(modaprotect_hw.getText().toString());
                        changenumber=false;
                        modaprotect_sBar7.setProgress(a);
                        System.out.println(a);
                    }
                }

                return false;
            }
        });

        modaprotect_sBar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int a=(int)(progress/100.0f*65535);
                System.out.println(a);
                if(changenumber) {
                    modaprotect_nm.setText(a + "");
                }else {
                    modaprotect_nm.setText(save_nm + "");
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        modaprotect_sBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(modaprotect_sBar2.getProgress()/100.0f*70-50);
                if(changenumber) {
                    modaprotect_lp.setText(String.format("%.1f",a));
                }else {
                    modaprotect_lp.setText(save_lp + "");
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        modaprotect_sBar3.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(modaprotect_sBar3.getProgress()/100.0f*70-50);
                if(changenumber) {
                    modaprotect_lr.setText(String.format("%.1f",a));
                }else {
                    modaprotect_lr.setText(save_lr + "");
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        modaprotect_sBar4.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(modaprotect_sBar4.getProgress()/100.0f*70-50);
                if(changenumber) {
                    modaprotect_lw.setText(String.format("%.1f",a));
                }else {
                    modaprotect_lw.setText(save_lw + "");
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        modaprotect_sBar5.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(modaprotect_sBar5.getProgress()/100.0f*150);
                if(changenumber) {
                    modaprotect_hp.setText(String.format("%.1f",a));
                }else {
                    modaprotect_hp.setText(save_hp + "");
                    changenumber=true;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        modaprotect_sBar6.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(modaprotect_sBar6.getProgress()/100.0f*150);
                if(changenumber) {
                    modaprotect_hr.setText(String.format("%.1f",a));
                }else {
                    modaprotect_hr.setText(save_hr + "");
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        modaprotect_sBar7.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float a=(float)(modaprotect_sBar7.getProgress()/100.0f*150);
                if(changenumber) {
                    modaprotect_hw.setText(String.format("%.1f",a));
                }else {
                    modaprotect_hw.setText(save_hw + "");
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        modaprotect_btn1.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show( Protection_moda.this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s1 =settingCheck.SendWithData("0081" ,(short)(Integer.valueOf(modaprotect_nm.getText().toString())*1));

                                    if(checkflag){
                                        BluetoothS.write2(s1);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;

                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "moda保護計數值:"+modaprotect_nm.getText().toString());


            }



        });
        modaprotect_btn2.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show( Protection_moda.this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s2 =settingCheck.SendWithData("0082" ,(short)((Float.valueOf(modaprotect_lp.getText().toString()))* 10));

                                    if(checkflag){
                                        BluetoothS.write2(s2);
                                        SendCount = 0 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "moda低溫保護點:"+modaprotect_lp.getText().toString());


            }


        });
        modaprotect_btn3.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialog = ProgressDialog.show(Protection_moda.this,
                        "寫入資料中", "請稍後...", true);
                SendCount = 1;
                Sendflag = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (Sendflag) {

                            switch (SendCount) {

                                case 1:
                                    char[] s3 = settingCheck.SendWithData("0083", (short)((Float.valueOf(modaprotect_lr.getText().toString()))* 10));

                                    if (checkflag) {
                                        BluetoothS.write2(s3);
                                        SendCount = 0;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0:
                                    if (checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        } catch (Exception e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "moda低溫回復點:"+modaprotect_lr.getText().toString());

            }

        });
        modaprotect_btn4.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_moda.this,
                        "寫入資料中", "請稍後...", true);
                SendCount = 1;
                Sendflag = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (Sendflag) {

                            switch (SendCount) {

                                case 1:
                                    char[] s4 = settingCheck.SendWithData("0084", (short)(Float.valueOf(modaprotect_lw.getText().toString()) * 10));

                                    if (checkflag) {
                                        BluetoothS.write2(s4);
                                        SendCount = 0;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;

                                case 0:
                                    if (checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        } catch (Exception e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "moda低溫警告點:"+modaprotect_lw.getText().toString());

            }


        });
        modaprotect_btn5.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_moda.this,
                        "寫入資料中", "請稍後...", true);
                SendCount = 1;
                Sendflag = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (Sendflag) {

                            switch (SendCount) {

                                case 1:
                                    char[] s5 = settingCheck.SendWithData("0085", (short)(Float.valueOf(modaprotect_hp.getText().toString()) * 10));

                                    if (checkflag) {
                                        BluetoothS.write2(s5);
                                        SendCount = 0;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;

                                case 0:
                                    if (checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        } catch (Exception e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }


                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "moda高溫保護點:"+modaprotect_hp.getText().toString());

            }


        });
        modaprotect_btn6.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_moda.this,
                        "寫入資料中", "請稍後...", true);
                SendCount = 1;
                Sendflag = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (Sendflag) {

                            switch (SendCount) {

                                case 1:
                                    char[] s6 = settingCheck.SendWithData("0086", (short)(Float.valueOf(modaprotect_hr.getText().toString()) * 10));

                                    if (checkflag) {
                                        BluetoothS.write2(s6);
                                        SendCount = 0;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0:
                                    if (checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        } catch (Exception e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "moda高溫回復點:"+modaprotect_hr.getText().toString());

            }


        });
        modaprotect_btn7.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog = ProgressDialog.show(Protection_moda.this,
                        "寫入資料中", "請稍後...", true);
                SendCount = 1;
                Sendflag = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (Sendflag) {

                            switch (SendCount) {

                                case 1:
                                    char[] s7 = settingCheck.SendWithData("0087", (short)((Float.valueOf(modaprotect_hw.getText().toString()))* 10));

                                    if (checkflag) {
                                        BluetoothS.write2(s7);
                                        SendCount = 0;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 0:
                                    if (checkflag) {
                                        Sendflag = false;
                                        break;
                                    }
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        } catch (Exception e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                logService.SaveLog(LogPath, logFileName, "moda高溫警告點:"+modaprotect_hw.getText().toString());

            }


        });
        modaprotect_btnsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = ProgressDialog.show(Protection_moda.this,
                        "寫入資料中", "請稍後...",true);
                SendCount = 1 ;
                Sendflag  = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while(Sendflag){

                            switch (SendCount) {

                                case 1 :
                                    char[] s1_e=settingCheck.SendEEPROMData("0081");

                                    if(checkflag){
                                        BluetoothS.write2(s1_e);
                                        SendCount = 2 ;
                                        checkflag = false;
                                        // dialog.setMessage("25%");
                                    }
                                    break;
                                case 2 :
                                    char[] s2_e=settingCheck.SendEEPROMData("0082");
                                    if(checkflag){
                                        BluetoothS.write2(s2_e);
                                        SendCount = 3 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 3 :
                                    char[] s3_e=settingCheck.SendEEPROMData("0083");
                                    if(checkflag){
                                        BluetoothS.write2(s3_e);
                                        SendCount = 4 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 4 :
                                    char[] s4_e=settingCheck.SendEEPROMData("0084");
                                    if(checkflag){
                                        BluetoothS.write2(s4_e);
                                        SendCount = 5 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 5 :
                                    char[] s5_e=settingCheck.SendEEPROMData("0085");
                                    if(checkflag){
                                        BluetoothS.write2(s5_e);
                                        SendCount = 6 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 6 :
                                    char[] s6_e=settingCheck.SendEEPROMData("0086");
                                    if(checkflag){
                                        BluetoothS.write2(s6_e);
                                        SendCount = 7 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 7 :
                                    char[] s7_e=settingCheck.SendEEPROMData("0087");
                                    if(checkflag){
                                        BluetoothS.write2(s7_e);
                                        SendCount = 0 ;
                                        checkflag = false;
                                    }
                                    break;
                                case 0 :
                                    if(checkflag) {
                                        Sendflag = false;
                                    }
                                    break;

                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        }catch (Exception e){
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();

            }
        });

    }
    private void init(){
        modaprotect_nm=(EditText)findViewById(R.id.modaprotect_nm);
        modaprotect_lp=(EditText)findViewById(R.id.modaprotect_lp);
        modaprotect_lr=(EditText)findViewById(R.id.modaprotect_lr);
        modaprotect_hp=(EditText)findViewById(R.id.modaprotect_hp);
        modaprotect_hr=(EditText)findViewById(R.id.modaprotect_hr);
        modaprotect_lw=(EditText)findViewById(R.id.modaprotect_lw);
        modaprotect_hw=(EditText)findViewById(R.id.modaprotect_hw);
        modaprotect_sBar1=(SeekBar) findViewById(R.id.modaprotect_sBar1);
        modaprotect_sBar2=(SeekBar) findViewById(R.id.modaprotect_sBar2);
        modaprotect_sBar3=(SeekBar) findViewById(R.id.modaprotect_sBar3);
        modaprotect_sBar4=(SeekBar) findViewById(R.id.modaprotect_sBar4);
        modaprotect_sBar5=(SeekBar) findViewById(R.id.modaprotect_sBar5);
        modaprotect_sBar6=(SeekBar) findViewById(R.id.modaprotect_sBar6);
        modaprotect_sBar7=(SeekBar) findViewById(R.id.modaprotect_sBar7);
        modaprotect_btn1=(Button) findViewById(R.id.modaprotect_btn1);
        modaprotect_btn2=(Button) findViewById(R.id.modaprotect_btn2);
        modaprotect_btn3=(Button) findViewById(R.id.modaprotect_btn3);
        modaprotect_btn4=(Button) findViewById(R.id.modaprotect_btn4);
        modaprotect_btn5=(Button) findViewById(R.id.modaprotect_btn5);
        modaprotect_btn6=(Button) findViewById(R.id.modaprotect_btn6);
        modaprotect_btn7=(Button) findViewById(R.id.modaprotect_btn7);
        modaprotect_btnsetting=(Button) findViewById(R.id.modaprotect_btnsetting);
        modabacktoset=(ImageButton)findViewById(R.id.modabacktoset);

    }
    private Handler BTPoccess = new Handler(){
        private StringPoccess poccess = new StringPoccess();

        private String data ;

        private boolean tagflag = false;

        /**
         *  連線中狀態值
         *  1. DISCONNECT 斷線狀態    0
         *  2. CONNECTED  連線中      1
         *  3. CONNECTING 嘗試連線中  2
         **/
        private final static int DISCONNECT = 0 ;
        private final static int CONNECTED = 1 ;
        private final static int CONNECTING = 2 ;

        /**
         *  連線中狀態值
         *  1. DATAWAIT   等待資料中    0
         *  2. DATAREAD   資料讀取      1
         *  3. DATAWIRTE  資料寫入      2
         **/

        private final static int DATAWAIT = 0 ;
        private final static int DATAREAD = 1 ;
        private final static int DATAWIRTE = 2 ;

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case DISCONNECT :
                    if(tagflag) Log.d("Handle" ,"DISCONNECT" );
                    break;
                case CONNECTED :
                    if(tagflag)Log.d("Handle" ,"CONNECTED" );

                    if(msg.arg1 == DATAREAD) {
                        data = (String) msg.obj;
                        if(data == null)return;
                        System.out.println(data);
                        primaevalData.split(data);
                    }

                    //System.out.println(data);
                    if(primaevalData.startcheck()) {
                        if (primaevalData.getId()[2] == 0xCE) {
                            checkflag = true;
                            writeCheck.setIDCheck(primaevalData.getdata());
                            if (writeCheck.CheckRAMWrite()) {
                                checkflag = true;
                            }if (writeCheck.CheckEEPROMWrite()) {
                                checkflag = true;
                            }
                        }

                        if (primaevalData.getId()[2]==0xCF) {
                            readCheck.setData(primaevalData.getdata());
                            if (readCheck.getAddress().equals("081")) {
                                Float a = (float) readCheck.getValues();
                                String s = String.valueOf(a);
                                int b = (int) (a / 65535.0f * 100);
                                modaprotect_sBar1.setProgress(b);
                                modaprotect_nm.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load moda_nm:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("082")) {
                                Float a = (float) readCheck.getValues() / 10.f ;
                                String s = String.format("%.1f", a);
                                int b = (int) ((a + 50) / 70.0f*100);
                                modaprotect_sBar2.setProgress(b);
                                modaprotect_lp.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load moda_lp:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("083")) {
                                Float a = (float) readCheck.getValues() / 10.f ;
                                String s = String.format("%.1f", a);
                                int b = (int) ((a + 50) / 70.0f*100);
                                modaprotect_sBar3.setProgress(b);
                                modaprotect_lr.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load moda_lr:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("084")) {
                                Float a = (float) readCheck.getValues() / 10.f ;
                                String s = String.format("%.1f", a);
                                int b = (int) ((a + 50) / 70.0f*100);
                                modaprotect_sBar4.setProgress(b);
                                modaprotect_lw.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load moda_lw:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("085")) {
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) (a / 150.0f*100);
                                modaprotect_sBar5.setProgress(b);
                                modaprotect_hp.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load moda_hp:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("086")) {
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) (a / 150.0f*100);
                                modaprotect_sBar6.setProgress(b);
                                modaprotect_hr.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load moda_hr:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("087")) {
                                Float a = (float) readCheck.getValues() / 10.f;
                                String s = String.valueOf(a);
                                int b = (int) (a / 150.0f*100);
                                modaprotect_sBar7.setProgress(b);
                                modaprotect_hw.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load moda_hw:" + s);
                                readCheck.getValues();
                                checkflag = true;
                            } else {
                                checkflag = false;
                            }
                        }
                    }

                    break;
                case CONNECTING :
                    if(tagflag)Log.d("Handle" ,"CONNECTING" );
                    break;
                case 5:
                    try {
                        dialog.dismiss();
                    }catch (Exception e){
                        dialog.dismiss();
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };





}
