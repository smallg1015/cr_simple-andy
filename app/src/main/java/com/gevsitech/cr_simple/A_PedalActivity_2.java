package com.gevsitech.cr_simple;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gevsitech.cr_simple.data_separation.PrimaevalData;
import com.gevsitech.cr_simple.data_separation.ReadCheck;
import com.gevsitech.cr_simple.data_separation.SettingCheck;
import com.gevsitech.cr_simple.data_separation.WriteCheck;

public class A_PedalActivity_2 extends AppCompatActivity {
    char[]  s_x1,s_x2,s_x3,s_x4,s_x5,s_y1,s_y2,s_y3,s_y4,s_y5;
    int sendCount;
    private Boolean Sendflag = true;
    private volatile Boolean checkflag = true;
    private ProgressDialog dialog;
    private int SendCount = 1 ;
    private EditText e_x1 ;
    private EditText e_x2 ;
    private EditText e_x3 ;
    private EditText e_x4 ;
    private EditText e_x5 ;
    private EditText e_y1 ;
    private EditText e_y2 ;
    private EditText e_y3 ;
    private EditText e_y4 ;
    private EditText e_y5 ;

    ValueLineView valueLineView ;

    Button btn_pedal2set,btn_pedal2to1,btn_pedaleep ;

    private PrimaevalData primaevalData = new PrimaevalData();
    private ReadCheck    readCheck  = new ReadCheck();
    private WriteCheck   writeCheck = new WriteCheck();
    private SettingCheck settingCheck = new SettingCheck();

    BluetoothService BluetoothS = BluetoothService.getBluetoothService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_activity_pedal2);

        //螢幕控件初始化
        widgetsInit();
//        valueLineView = (ValueLineView)findViewById(R.id.view);
//        valueLineView.sethandle(BTPoccess);
        btn_pedal2set = (Button)findViewById(R.id.btn_pedal2set);
        btn_pedal2to1=(Button)findViewById(R.id.btn_pedal2to1);
        btn_pedaleep=(Button)findViewById(R.id.btn_pedaleep);
        BluetoothS.setBtHandler(BTPoccess);

        if(BluetoothS.isConnectedCheck()){
            dialog = ProgressDialog.show( A_PedalActivity_2.this,
                    "讀取初始資料中", "請稍待片刻...",true);
            SendCount = 1 ;
            Sendflag  = true;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(Sendflag){
                        try {
                            Thread.sleep(200);
                        }catch (Exception e){

                        }
                        switch (SendCount) {

                            case 1 :
                                //寫入1
                                char[] s1 = settingCheck.Send("0174");
                                if(checkflag){
                                    BluetoothS.write2(s1);
                                    SendCount = 2 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入25%");
                                }
                                break;
                            case 2 :
                                char[] s2 = settingCheck.Send("0179");
                                if(checkflag){
                                    BluetoothS.write2(s2);
                                    SendCount = 3 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入50%");
                                }
                                break;
                            case 3 :
                                char[] s3 = settingCheck.Send("0175");
                                if(checkflag){
                                    BluetoothS.write2(s3);
                                    SendCount = 4 ;
                                    checkflag = false;
                                    // dialog.setMessage("寫入75%");
                                }
                                break;
                            case 4 :
                                char[] s4 = settingCheck.Send("017A");
                                if(checkflag){
                                    BluetoothS.write2(s4);
                                    SendCount = 5 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入75%");
                                }
                                break;
                            case 5 :
                                char[] s5 = settingCheck.Send("0176");
                                if(checkflag){
                                    BluetoothS.write2(s5);
                                    SendCount = 6 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 6 :
                                char[] s6 = settingCheck.Send("017B");
                                if(checkflag){
                                    BluetoothS.write2(s6);
                                    SendCount = 7 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 7 :
                                char[] s7 = settingCheck.Send("0177");
                                if(checkflag){
                                    BluetoothS.write2(s7);
                                    SendCount = 8 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 8 :
                                char[] s8 = settingCheck.Send("017C");
                                if(checkflag){
                                    BluetoothS.write2(s8);
                                    SendCount = 9 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 9 :
                                char[] s9 = settingCheck.Send("0178");
                                if(checkflag){
                                    BluetoothS.write2(s9);
                                    SendCount = 10 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 10 :
                                char[] s10 = settingCheck.Send("017D");
                                if(checkflag){
                                    BluetoothS.write2(s10);
                                    SendCount = 0 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }
                                break;
                            case 0 :
                                if(checkflag) {
                                    System.out.println("b");

                                    Sendflag = false;
                                    break;
                                }
                        }


                    }
                    BTPoccess.obtainMessage(5).sendToTarget();

                }
            });
            thread.start();



        }else{
            Toast.makeText(A_PedalActivity_2.this,"藍芽尚未連線，無法讀取資料",Toast.LENGTH_SHORT).show();
        }

        btn_pedaleep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = ProgressDialog.show(A_PedalActivity_2.this,
                        "寫入資料中", "請稍後...", true);
                SendCount = 1;
                Sendflag = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (Sendflag) {

                            switch (SendCount) {

                                case 1:
                                    if(checkflag){
                                        checkflag = false;
                                        s_x1 =settingCheck.SendEEPROMData("0174");
                                        BluetoothS.write2(s_x1);
                                        SendCount=2;
                                    }
                                    break;
                                case 2:
                                    if(checkflag){
                                        checkflag = false;
                                        s_y1 =settingCheck.SendEEPROMData("0179");
                                        BluetoothS.write2(s_y1);
                                        SendCount=3;
                                    }
                                    break;
                                case 3:
                                    if(checkflag){
                                        checkflag = false;
                                        s_x2 =settingCheck.SendEEPROMData("0175");
                                        BluetoothS.write2(s_x2);
                                        SendCount=4;
                                    }
                                    break;
                                case 4:
                                    if(checkflag){
                                        checkflag = false;
                                        s_y2 =settingCheck.SendEEPROMData("017A");
                                        BluetoothS.write2(s_y2);
                                        SendCount=5;
                                    }
                                    break;
                                case 5:
                                    if(checkflag){
                                        checkflag = false;
                                        s_x3 =settingCheck.SendEEPROMData("0176");
                                        BluetoothS.write2(s_x3);
                                        SendCount=6;
                                    }
                                    break;
                                case 6:
                                    if(checkflag){
                                        checkflag = false;
                                        s_y3 =settingCheck.SendEEPROMData("017B");
                                        BluetoothS.write2(s_y3);
                                        SendCount=7;
                                    }
                                    break;
                                case 7:
                                    if(checkflag){
                                        checkflag = false;
                                        s_x4 =settingCheck.SendEEPROMData("0177");
                                        BluetoothS.write2(s_x4);
                                        SendCount=8;
                                    }
                                    break;
                                case 8:
                                    if(checkflag){
                                        checkflag = false;
                                        s_y4 =settingCheck.SendEEPROMData("017C");
                                        BluetoothS.write2(s_y4);
                                        SendCount=9;
                                    }
                                    break;
                                case 9:
                                    if(checkflag){
                                        checkflag = false;
                                        s_x5 =settingCheck.SendEEPROMData("0178");
                                        BluetoothS.write2(s_x5);
                                        SendCount=10;
                                    }
                                    break;
                                case 10:
                                    if(checkflag){
                                        checkflag = false;
                                        s_y5 =settingCheck.SendEEPROMData("017D");
                                        BluetoothS.write2(s_y5);
                                        SendCount=0;
                                    }
                                    break;

                                case 0:
                                    if (checkflag) {
                                        Sendflag = false;

                                    }
                                    break;
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        } catch (Exception e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();

            }
        });

        btn_pedal2to1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });

        btn_pedal2set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = ProgressDialog.show(A_PedalActivity_2.this,
                        "寫入資料中", "請稍後...", true);
                SendCount = 1;
                Sendflag = true;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (Sendflag) {

                            switch (SendCount) {

                                case 1:
                                    if(checkflag){
                                        checkflag = false;
                                        s_x1 =settingCheck.SendWithData("0174" ,(short)(Float.valueOf( e_x1.getText().toString())*100));
                                        BluetoothS.write2(s_x1);
                                        SendCount=2;
                                    }
                                    break;
                                case 2:
                                    if(checkflag){
                                        checkflag = false;
                                        s_y1 =settingCheck.SendWithData("0179" ,(short)(Float.valueOf( e_y1.getText().toString())*10000));
                                        BluetoothS.write2(s_y1);
                                        SendCount=3;
                                    }
                                    break;
                                case 3:
                                    if(checkflag){
                                        checkflag = false;
                                        s_x2 =settingCheck.SendWithData("0175" ,(short)(Float.valueOf(e_x2.getText().toString())*100));
                                        BluetoothS.write2(s_x2);
                                        SendCount=4;
                                    }
                                    break;
                                case 4:
                                    if(checkflag){
                                        checkflag = false;
                                        s_y2 =settingCheck.SendWithData("017A" , (short)(Float.valueOf(e_y2.getText().toString())*10000));
                                        BluetoothS.write2(s_y2);
                                        SendCount=5;
                                    }
                                    break;
                                case 5:
                                    if(checkflag){
                                        checkflag = false;
                                        s_x3 =settingCheck.SendWithData("0176" , (short)(Float.valueOf(e_x3.getText().toString())*100));
                                        BluetoothS.write2(s_x3);
                                        SendCount=6;
                                    }
                                    break;
                                case 6:
                                    if(checkflag){
                                        checkflag = false;
                                        s_y3 =settingCheck.SendWithData("017B" , (short)(Float.valueOf(e_y3.getText().toString())*10000));
                                        BluetoothS.write2(s_y3);
                                        SendCount=7;
                                    }
                                    break;
                                case 7:
                                    if(checkflag){
                                        checkflag = false;
                                        s_x4 =settingCheck.SendWithData("0177" , (short)(Float.valueOf(e_x4.getText().toString())*100));
                                        BluetoothS.write2(s_x4);
                                        SendCount=8;
                                    }
                                    break;
                                case 8:
                                    if(checkflag){
                                        checkflag = false;
                                        s_y4 =settingCheck.SendWithData("017C" ,(short)(Float.valueOf( e_y4.getText().toString())*10000));
                                        BluetoothS.write2(s_y4);
                                        SendCount=9;
                                    }
                                    break;
                                case 9:
                                    if(checkflag){
                                        checkflag = false;
                                        s_x5 =settingCheck.SendWithData("0178" , (short)(Float.valueOf(e_x5.getText().toString())*100));
                                        BluetoothS.write2(s_x5);
                                        SendCount=10;
                                    }
                                    break;
                                case 10:
                                    if(checkflag){
                                        checkflag = false;
                                        s_y5 =settingCheck.SendWithData("017D" ,(short)(Float.valueOf( e_y5.getText().toString())*10000));
                                        BluetoothS.write2(s_y5);
                                        SendCount=0;
                                    }
                                    break;

                                case 0:
                                    if (checkflag) {
                                        Sendflag = false;

                                    }
                                    break;
                            }
                        }
                        try {
                            System.out.println("B");
                            dialog.setMessage("寫入成功");
                            Thread.sleep(1000);
                            dialog.dismiss();
                        } catch (Exception e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
            }
        });
    }

    private void widgetsInit(){

        e_x1 = (EditText)findViewById(R.id.padal_edit_x1);
        e_x2 = (EditText)findViewById(R.id.padal_edit_x2);
        e_x3 = (EditText)findViewById(R.id.padal_edit_x3);
        e_x4 = (EditText)findViewById(R.id.padal_edit_x4);
        e_x5 = (EditText)findViewById(R.id.padal_edit_x5);


        e_y1 = (EditText)findViewById(R.id.padal_edit_y1);
        e_y2 = (EditText)findViewById(R.id.padal_edit_y2);
        e_y3 = (EditText)findViewById(R.id.padal_edit_y3);
        e_y4 = (EditText)findViewById(R.id.padal_edit_y4);
        e_y5 = (EditText)findViewById(R.id.padal_edit_y5);

    }

    private Handler BTPoccess = new Handler(){
        private StringPoccess poccess = new StringPoccess();

        private String data ;

        private boolean tagflag = true;

        /**
         *  連線中狀態值
         *  1. DISCONNECT 斷線狀態    0
         *  2. CONNECTED  連線中      1
         *  3. CONNECTING 嘗試連線中  2
         **/
        private final static int DISCONNECT = 0 ;
        private final static int CONNECTED = 1 ;
        private final static int CONNECTING = 2 ;


        private final static int SETTING = 3;

        /**
         *  連線中狀態值
         *  1. DATAWAIT   等待資料中    0
         *  2. DATAREAD   資料讀取      1
         *  3. DATAWIRTE  資料寫入      2
         **/

        private final static int DATAWAIT = 0 ;
        private final static int DATAREAD = 1 ;
        private final static int DATAWIRTE = 2 ;

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case DISCONNECT :
                    if(tagflag) Log.d("Handle" ,"DISCONNECT" );
                    break;
                case CONNECTED :
                    if(msg.arg1 == DATAREAD) {
                        data = (String) msg.obj;
                        System.out.println(data);
                        if(data == null)return;
                        primaevalData.split(data);
                    }


                    if(primaevalData.startcheck()) {
                        if (primaevalData.getId()[2]==0xCE) {
                            checkflag = true;
                            writeCheck.setIDCheck(primaevalData.getdata());
                            System.out.println("NoCheck");
                            if (writeCheck.CheckRAMWrite()) {
                                System.out.println("Check");
                                //checkflag = true;
                            }
                            if (writeCheck.CheckEEPROMWrite()) {
                                System.out.println("Check");
                                //checkflag = true;

                            }
                        }


                        if (primaevalData.getId()[2]==0xCF) {
                            readCheck.setData(primaevalData.getdata());
                            if (readCheck.getAddress().equals("174")) {
                                Float a = (float) readCheck.getValues() / 100.00f;
                                String s = String.valueOf(a);
                                e_x1.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("179")) {
                                Float a = (float) readCheck.getValues() / 10000.0000f;
                                String s = String.valueOf(a);
                                e_y1.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("175")) {
                                Float a = (float) readCheck.getValues() / 100.00f;
                                String s = String.valueOf(a);
                                e_x2.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("17a")) {
                                Float a = (float) readCheck.getValues() / 10000.0000f;
                                String s = String.valueOf(a);
                                e_y2.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("176")) {
                                Float a = (float) readCheck.getValues() / 100.00f;
                                String s = String.valueOf(a);
                                e_x3.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("17b")) {
                                Float a = (float) readCheck.getValues() / 10000.0000f;
                                String s = String.valueOf(a);
                                e_y3.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("177")) {
                                Float a = (float) readCheck.getValues() / 100.00f;
                                String s = String.valueOf(a);
                                e_x4.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("17c")) {
                                Float a = (float) readCheck.getValues() / 10000.0000f;
                                String s = String.valueOf(a);
                                e_y4.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("178")) {
                                Float a = (float) readCheck.getValues() / 100.00f;
                                String s = String.valueOf(a);
                                e_x5.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("17d")) {
                                Float a = (float) readCheck.getValues() / 10000.0000f;
                                String s = String.valueOf(a);
                                e_y5.setText(s);
                                readCheck.getValues();
                                checkflag = true;
                            } else {
                                checkflag = false;

                            }
                        }
                    }


                    break;
                case CONNECTING :
                    if(tagflag)Log.d("Handle" ,"CONNECTING" );
                    break;
                case SETTING :
                    String XandY = (String)msg.obj;
                    if(msg.arg1 == 1){
                        e_x1.setText(XandY);
                        e_y1.setText("20%");
                    }
                    if(msg.arg1 == 2){
                        e_x2.setText(XandY);
                        e_y2.setText("40%");
                    }
                    if(msg.arg1 == 3){
                        e_x3.setText(XandY);
                        e_y3.setText("60%");
                    }
                    if(msg.arg1 == 4){
                        e_x4.setText(XandY);
                        e_y4.setText("80%");
                    }
                    if(msg.arg1 == 5){
                        e_x5.setText(XandY);
                        e_y5.setText("100%");
                    }

                    break;

                case 4 :
                    Toast.makeText(A_PedalActivity_2.this,"Send Success",Toast.LENGTH_SHORT);
                    break;
                case 5:
                    try {
                        dialog.dismiss();
                    }catch (Exception e){
                        dialog.dismiss();
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };
}
