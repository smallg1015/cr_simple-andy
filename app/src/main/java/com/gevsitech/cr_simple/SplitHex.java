package com.gevsitech.cr_simple;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Created by Jian on 2017/3/5.
 */

public class SplitHex {
    private String filename = "/CR_FLASH_125_2.hex";
    private String mainHexString;
    private String justOne;
    //最終數據陣列
    private String[] datalist;
    //數據陣列
    private String[] allData;

    private static Integer datalength1;
    private int  sendDataLength;


    private String datalength;

    //主要地址
    private static String address;
    private static String address2;

    //主要狀態
    private static String status;

    //主要狀態
    private static String checksum;

    public SplitHex(String string){
        mainHexString = string;
        splitHexString(mainHexString);
    }
    public SplitHex(){
    }

    private Boolean splitHexString(String string){
        if(mainHexString.indexOf(":")!=0){
            return false;
        }

        //取得資料位元數量
        datalength = mainHexString.substring(1,3);

        //取得寫入狀態
        address = mainHexString.substring(3,7);

        //取得寫入狀態
        status= mainHexString.substring(7,9);

        if(status.equals("04")){
            address2 = mainHexString.substring(mainHexString.length()-6,mainHexString.length()-2);
        }

        //checksum
        checksum= mainHexString.substring(mainHexString.length()-2,mainHexString.length());

        datalength1 = Integer.parseInt(datalength,16);
        //透過數量分割資料列
        String data = mainHexString.substring(9,9+datalength1*2);
        if(data.length()<=16) {
            String substring = data.substring(0,data.length());
            //計算資料Byte數量
            String[] dataAll = new String[datalength1];
            for(int A = 0 ; A < datalength1 ; A++){
                dataAll[A] = substring.substring(A*2 ,2+(A*2));
            }
            StringBuffer buf = new StringBuffer();
            for(int k = 0 ; k < 4 ; k ++){
                if(datalength1/2 > k) {
                    buf.append("," + dataAll[1+k*2] + "," +dataAll[k*2] );
                }else{
                    buf.append(",00,00");
                }
            }
            justOne = buf.insert(0,"<AD,8").toString();
        }
        if(datalength1/8 == 2){
            datalist = new String[3];
        }else if(datalength1/8 == 1){
            datalist = new String[2];
        }else {
            datalist = new String[datalength1/8];
        }


        for (int i = 0 ; i < (datalength1/8) ; i++ ){
            if(i==0){
                String substring ="";
                substring = data.substring(0,16);
                //計算資料Byte數量
                String[] dataAll = new String[datalength1];
                for(int A = 0 ; A < substring.length()/2 ; A++){
                    dataAll[A] = substring.substring(A*2 ,2+(A*2));
                }
                StringBuffer buf = new StringBuffer();
                for(int k = 0 ; k < 4 ; k ++){
                    if(datalength1/2 > k) {
                        buf.append("," + dataAll[1+k*2] + "," +dataAll[k*2] );
                    }else{
                        buf.append(",00,00");
                    }
                }
                datalist[i] = buf.insert(0,"<AD,8").toString().replace("\n","");

                if(Math.ceil(((float)datalength1/8.f))==2){
                    String substring1 ="";
                    substring1 = data.substring(16,data.length());
                    //計算資料Byte數量
                    String[] dataAll1 = new String[datalength1];
                    for(int A = 0 ; A < substring1.length()/2 ; A++){
                        dataAll1[A] = substring1.substring(A*2 ,2+(A*2));
                    }
                    StringBuffer buf1 = new StringBuffer();
                    /*
                    for(int k = 0 ; k < 4 ; k ++){

                        if(substring1.length()/4 > k) {
                            buf1.append("," + dataAll1[1+k*2] + "," +dataAll1[k*2] );
                        }else{
                            System.out.println("IN2");
                            buf1.append(",00,00");
                        }
                    }
                     */
                    for(int k = 0 ; k < 8 ; k ++){
                        if( ((float)k/2.f )== 0) {
                            //buf1.append("," + dataAll1[k+1]);
                            if( substring1.length()/2 > k )buf1.append("," + dataAll1[k+1]);
                            else buf1.append("," + "00");
                        }else{
                            //buf1.append("," + dataAll1[k-1]);
                            if( substring1.length()/2 > k )buf1.append("," + dataAll1[k-1]);
                            else buf1.append("," + "00");
                        }
                    }
                    datalist[1] = buf1.insert(0,"<AD,8").toString().replace("\n","");
                }


            }else if(i==1){
                String substring ="";
                substring = data.substring(16,32);
                //計算資料Byte數量
                String[] dataAll = new String[datalength1];
                for(int A = 0 ; A < substring.length()/2 ; A++){
                    dataAll[A] = substring.substring(A*2 ,2+(A*2));
                }
                StringBuffer buf = new StringBuffer();
                for(int k = 0 ; k < 4 ; k ++){
                    if(datalength1/2 > k) {
                        buf.append("," + dataAll[1+k*2] + "," +dataAll[k*2] );
                    }else{
                        buf.append(",00,00");
                    }
                }
                datalist[i] = buf.insert(0,"<AD,8").toString().replace("\n","");

                if(Math.ceil(((float)datalength1/8.f))==3){
                    String substring1 ="";
                    substring1 = data.substring(32,data.length());
                    //計算資料Byte數量
                    String[] dataAll1 = new String[datalength1];
                    for(int A = 0 ; A < substring1.length()/2 ; A++){
                        dataAll1[A] = substring1.substring(A*2 ,2+(A*2));
                    }
                    StringBuffer buf1 = new StringBuffer();
                    /*
                    for(int k = 0 ; k < 4 ; k ++){

                        if(substring1.length()/4 > k) {
                            buf1.append("," + dataAll1[1+k*2] + "," +dataAll1[k*2] );
                        }else{
                            System.out.println("IN2");
                            buf1.append(",00,00");
                        }
                    }
                     */
                    for(int k = 0 ; k < 8 ; k ++){

                        if( ((float)k/2.f )== 0) {
                            //buf1.append("," + dataAll1[k+1]);
                            if( substring1.length()/2 > k )buf1.append("," + dataAll1[k+1]);
                            else buf1.append("," + "00");
                        }else{
                            //buf1.append("," + dataAll1[k-1]);
                            if( substring1.length()/2 > k )buf1.append("," + dataAll1[k-1]);
                            else buf1.append("," + "00");
                        }
                    }
                    datalist[2] = buf1.insert(0,"<AD,8").toString().replace("\n","");
                }
            }else if(i==2){
                String substring ="";
                substring = data.substring(32,48);
                //計算資料Byte數量
                String[] dataAll = new String[datalength1];
                for(int A = 0 ; A < substring.length()/2 ; A++){
                    dataAll[A] = substring.substring(A*2 ,2+(A*2));
                }
                StringBuffer buf = new StringBuffer();
                for(int k = 0 ; k < 4 ; k ++){
                    if(datalength1/2 > k) {
                        buf.append("," + dataAll[1+k*2] + "," +dataAll[k*2] );
                    }else{
                        buf.append(",00,00");
                    }
                }
                datalist[i] = buf.insert(0,"<AD,8").toString().replace("\n","");
            }else if(i==3) {
                String substring ="";
                substring = data.substring(48,64);
                //計算資料Byte數量
                String[] dataAll = new String[datalength1];
                for(int A = 0 ; A < substring.length()/2 ; A++){
                    dataAll[A] = substring.substring(A*2 ,2+(A*2));
                }
                StringBuffer buf = new StringBuffer();
                for(int k = 0 ; k < 4 ; k ++){
                    if(datalength1/2 > k) {
                        buf.append("," + dataAll[1+k*2] + "," +dataAll[k*2] );
                    }else{
                        buf.append(",00,00");
                    }
                }
                datalist[i] = buf.insert(0,"<AD,8").toString().replace("\n","").replace("\n","").replace("\r","");
            }
        }
        return true;
    }

    public int length(String path,String filename){
        Boolean countflag=true;
        try {
            //建立FileReader物件，並設定讀取的檔案為SD卡中的output.txt檔案
            FileReader fr = new FileReader(path + File.separator+ filename);
            BufferedReader br = new BufferedReader(fr);

            while (countflag){

                String temp = br.readLine(); //readLine()讀取一整行
                String temp1 =  temp.substring(7,9);
                sendDataLength++;
                String temp2 =  temp.substring(9,13);
                if(temp1.equals("04")){
                    sendDataLength = sendDataLength -1;
                    if(temp2.equals("003F")){
                        countflag = false;
                    }
                }
            }
            sendDataLength = sendDataLength+ 1;
        }catch (Exception e){
            e.printStackTrace();
        }

        return sendDataLength;
    }

    public int getLength(){
        return datalength1;
    }

    public String getStatus(){
        return status;
    }

    public int getDataLength(){
        return datalength1;
    }

    public String[] getDatalist(){
        return datalist;
    }

    public String getAlongString(){
        return justOne;
    }

    public String getAddressOffset(){


        String Hbyte = address.substring(0,2);
        String Lbyte = address.substring(2,4);

        String ALbyte="00";
        String AHbyte  = address2.substring(0,2);
        if(address2.length()>2) ALbyte = address2.substring(2,address2.length());
        String S = "<AD,8,"+datalength+",00,"+ALbyte+","+AHbyte+","+Lbyte+","+Hbyte+","+checksum+",00";
        return S;
    }

    public String getAddress(){
        return address;
    }
}