package com.gevsitech.cr_simple;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;



public class LineChartFragment extends Fragment {
    private RelativeLayout layout1;
    private RelativeLayout layout2;
    private RelativeLayout layout3;
    private RelativeLayout layout4;
    private SharedPreferences prefile;
    ArrayList<Entry> ListEntrymotor_speed;
    ArrayList<Entry> ListEntryVkey_V;
    ArrayList<Entry> ListEntryMotorA_U;
    ArrayList<Entry> ListEntryMotorA_V;
    ArrayList<Entry> ListEntryMos_tp1;
    ArrayList<Entry> ListEntryMos_tp2;
    ArrayList<Entry> ListEntryMotor_tp;
    ArrayList<Entry> ListEntryACC1;
    ArrayList<Entry> ListEntryBus_C;
    ArrayList<Entry> ListEntryBus_V;
    ArrayList<String> label;
    ArrayList<String> c2label;
    ArrayList<String> c3label;
    ArrayList<String> c5label;
    ArrayList<ILineDataSet> ListC1dataset;
    LineChart C1LineChart;
    LineChart C2LineChart;
    LineChart C5LineChart;
    LineChart C3LineChart;

    LineData C1Linedata;


    private CheckBox check1_MotorSpeed,check1_Vkey,check1_BusV,check1_BusC,check1_MotorA_U,check1_MotorA_V,check1_ACC1;
    private Button btn_datanext,btn_backtofile;








    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_line_chart, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //元件初始化區
        layout1=(RelativeLayout)getView().findViewById(R.id.chart);


        check1_MotorSpeed=(CheckBox)getView().findViewById(R.id.check1_MotorSpeed);
        check1_Vkey=(CheckBox)getView().findViewById(R.id.check1_Vkey);
        check1_BusV=(CheckBox)getView().findViewById(R.id.check1_BusV);
        check1_BusC=(CheckBox)getView().findViewById(R.id.check1_BusC);
        check1_MotorA_U=(CheckBox)getView().findViewById(R.id.check1_MotorA_U);
        check1_MotorA_V=(CheckBox)getView().findViewById(R.id.check1_MotorA_V);
        check1_ACC1=(CheckBox)getView().findViewById(R.id.check1_ACC1);
        btn_datanext=(Button)getView().findViewById(R.id.btn_datanext);
        btn_backtofile=(Button)getView().findViewById(R.id.btn_backtofile);






        prefile=getActivity().getSharedPreferences("prefile",Context.MODE_PRIVATE);

        C1LineChart=new LineChart(getActivity());

        ListEntrymotor_speed=new ArrayList<Entry>();
        ListEntryVkey_V=new ArrayList<Entry>();
        ListEntryMotorA_U=new ArrayList<Entry>();
         ListEntryMotorA_V=new ArrayList<Entry>();
        ListEntryMos_tp1=new ArrayList<Entry>();
        ListEntryMos_tp2=new ArrayList<Entry>();
        ListEntryMotor_tp=new ArrayList<Entry>();
        ListEntryACC1=new ArrayList<Entry>();
        ListEntryBus_C=new ArrayList<Entry>();
        ListEntryBus_V=new ArrayList<Entry>();
        label=new ArrayList<String>();
        c2label=new ArrayList<String>();
        c3label=new ArrayList<String>();
        c5label=new ArrayList<String>();



        File nowfilepath=new File(prefile.getString("nowfilepath",""));

        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(nowfilepath));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append(",");
            }
            br.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        String[] txtdata=text.toString().split(",");
        int labeldata=0;
        int c1data=0;
        int c2data=0;
        int c3data=0;
        int c5data=0;

        for(int l=0;l<txtdata.length;l++){
            System.out.println(txtdata[l]);
            String[] dataline=txtdata[l].replace(" ",",").split(",");

            labeldata++;
            System.out.println("labeldata"+labeldata);

            if(dataline[2].equals("c1")){
                c1data++;
                short Motorspeed=(short)Integer.parseInt(dataline[5]+dataline[4],16);
                System.out.println(Motorspeed);
                short Vkey_V_e=(short)Integer.parseInt(dataline[7]+dataline[6],16);
                float Vkey_V=Vkey_V_e/10.0f;
                short MotorA_U_e=(short)Integer.parseInt(dataline[9]+dataline[8],16);
                float MotorA_U=MotorA_U_e/10.0f;
                short MotorA_V_e=(short)Integer.parseInt(dataline[11]+dataline[10],16);
                float MotorA_V=MotorA_V_e/10.0f;

                ListEntrymotor_speed.add(new Entry(Motorspeed,c1data-1));
                ListEntryVkey_V.add(new Entry(Vkey_V,c1data-1));
                ListEntryMotorA_U.add(new Entry(MotorA_U,c1data-1));
                ListEntryMotorA_V.add(new Entry(MotorA_V,c1data-1));

                System.out.println("c1data"+c1data);
            }
            if(dataline[2].equals("c2")){
                c2data++;
                c2label.add(String.valueOf(c2data-1));
                short Mos_tp1_e=(short)Integer.parseInt(dataline[5]+dataline[4],16);
                float Mos_tp1=Mos_tp1_e/10.0f;
                short Mos_tp2_e=(short)Integer.parseInt(dataline[7]+dataline[6],16);
                float Mos_tp2=Mos_tp2_e/10.0f;
                short Motor_tp_e=(short)Integer.parseInt(dataline[9]+dataline[8],16);
                float Motor_tp=Motor_tp_e/10.0f;


                ListEntryMos_tp1.add(new Entry(Mos_tp1,c2data-1));
                ListEntryMos_tp2.add(new Entry(Mos_tp2,c2data-1));
                ListEntryMotor_tp.add(new Entry(Motor_tp,c2data-1));

                System.out.println("c2data"+c2data);
            }
            if(dataline[2].equals("c3")){
                c3data++;
                c3label.add(String.valueOf(c3data-1));
                short ACC1_e=(short)Integer.parseInt(dataline[5]+dataline[4],16);
                float ACC1=ACC1_e/10.0f;
                ListEntryACC1.add(new Entry(ACC1,c3data-1));

                System.out.println("c3data"+c3data);
//                short Brake=(short)Integer.parseInt(dataline[9]+dataline[8],16);
//                ListBrake.add(Brake);
            }
            if(dataline[2].equals("c5")){
                c5data++;
                c5label.add(String.valueOf(c5data-1));
                short Bus_C_e=(short)Integer.parseInt(dataline[9]+dataline[8],16);
                float Bus_C=Bus_C_e/10.0f;
                short Bus_V_e=(short)Integer.parseInt(dataline[11]+dataline[10],16);
                float Bus_V=Bus_V_e/10.0f;

                ListEntryBus_C.add(new Entry(Bus_C,c5data-1));
                ListEntryBus_V.add(new Entry(Bus_V,c5data-1));

                System.out.println("c5data"+c5data);
            }

        }

        int labeldatause=c1data+1;//因為

        System.out.println("labeldatause="+labeldatause);

        for(int i=0;i<labeldatause;i++){
            label.add(String.valueOf(i));
        }

        ListC1dataset=new ArrayList<ILineDataSet>();

        ListC1dataset.add(createDataSet(ListEntrymotor_speed,"motor_speed", Color.RED));
        ListC1dataset.add(createDataSet(ListEntryVkey_V,"Vkey_V", Color.parseColor("#FFFF7B00")));
        ListC1dataset.add(createDataSet(ListEntryBus_V,"Bus_V", Color.BLUE));
        ListC1dataset.add(createDataSet(ListEntryBus_C,"Bus_C", Color.GREEN));



        C1Linedata=new LineData(label,ListC1dataset);
        C1LineChart.clear();
        C1LineChart.setData(C1Linedata);
        C1LineChart.setDescription("基本資料");
        C1LineChart.refreshDrawableState();

        linechooseboxcreate(check1_MotorSpeed,check1_Vkey,check1_BusV,check1_BusC,check1_MotorA_U,check1_MotorA_V,check1_ACC1);


        Chartsetting(C1LineChart);



        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        layout1.addView(C1LineChart,params);


        btn_datanext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft=getFragmentManager().beginTransaction();
                ft.replace(R.id.center,new LineChartFragment2());
                ft.commit();
            }
        });

        btn_backtofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft=getFragmentManager().beginTransaction();
                ft.replace(R.id.center,new FileFragment());
                ft.commit();
            }
        });


    }

    //建立圖表線資料區
    private LineDataSet createDataSet(ArrayList<Entry> whichlist, String linename, int color){
        LineDataSet dataset=new LineDataSet(whichlist,linename);
        dataset.setDrawValues(false);
        dataset.setDrawCircles(false);
        dataset.setColor(color);

        return dataset;


    }


    //可刪除或顯示資料
    public void linechooseboxcreate(CheckBox box1, CheckBox box2, CheckBox box3, CheckBox box4,CheckBox box5,CheckBox box6,CheckBox box7){
        box1.setChecked(true);
        box2.setChecked(true);
        box3.setChecked(true);
        box4.setChecked(true);

        box1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    ListC1dataset.add(createDataSet(ListEntrymotor_speed,"motor_speed", Color.RED));
                }
                else {
                    ListC1dataset.remove(C1Linedata.getDataSetByLabel("motor_speed", true));

                }
                C1Linedata=new LineData(label,ListC1dataset);
                C1LineChart.clear();
                C1LineChart.setData(C1Linedata);
                C1LineChart.refreshDrawableState();
            }
        });
        box2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ListC1dataset.add(createDataSet(ListEntryVkey_V,"Vkey_V", Color.parseColor("#FFFF7B00")));
                }else{
                    ListC1dataset.remove(C1Linedata.getDataSetByLabel("Vkey_V",true));
                }
                C1Linedata=new LineData(label,ListC1dataset);
                C1LineChart.clear();
                C1LineChart.setData(C1Linedata);
                C1LineChart.refreshDrawableState();
            }
        });
        box3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ListC1dataset.add(createDataSet(ListEntryBus_V,"Bus_V", Color.BLUE));
                }
                else{
                    ListC1dataset.remove(C1Linedata.getDataSetByLabel("Bus_V",true));
                }
                C1Linedata=new LineData(label,ListC1dataset);
                C1LineChart.clear();
                C1LineChart.setData(C1Linedata);
                C1LineChart.refreshDrawableState();
            }
        });
        box4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ListC1dataset.add(createDataSet(ListEntryBus_C,"Bus_C", Color.GREEN));
                }else{
                    ListC1dataset.remove(C1Linedata.getDataSetByLabel("Bus_C",true));
                }
                C1Linedata=new LineData(label,ListC1dataset);
                C1LineChart.clear();
                C1LineChart.setData(C1Linedata);
                C1LineChart.refreshDrawableState();
            }
        });
        box5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ListC1dataset.add(createDataSet(ListEntryMotorA_U,"MotorA_U", Color.parseColor("#FFB700FF")));

                }else{
                    ListC1dataset.remove(C1Linedata.getDataSetByLabel("MotorA_U",true));

                }
                C1Linedata=new LineData(label,ListC1dataset);
                C1LineChart.clear();
                C1LineChart.setData(C1Linedata);
                C1LineChart.refreshDrawableState();
            }
        });
        box6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ListC1dataset.add(createDataSet(ListEntryMotorA_V,"MotorA_V", Color.CYAN));
                }else{
                    ListC1dataset.remove(C1Linedata.getDataSetByLabel("MotorA_V",true));
                }
                C1Linedata=new LineData(label,ListC1dataset);
                C1LineChart.clear();
                C1LineChart.setData(C1Linedata);
                C1LineChart.refreshDrawableState();
            }
        });
        box7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ListC1dataset.add(createDataSet(ListEntryACC1,"ACC1", Color.DKGRAY));
                }else{
                    ListC1dataset.remove(C1Linedata.getDataSetByLabel("ACC1",true));

                }
                C1Linedata=new LineData(label,ListC1dataset);
                C1LineChart.clear();
                C1LineChart.setData(C1Linedata);
                C1LineChart.refreshDrawableState();
            }
        });

    }

    public void Chartsetting(LineChart linechart){
        linechart.getAxisRight().setDrawLabels(false);
        linechart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        linechart.getXAxis().setDrawLimitLinesBehindData(true);
        linechart.getAxisLeft().setDrawZeroLine(false);
        linechart.getAxisLeft().setSpaceBottom(0f);
        linechart.getAxisRight().setSpaceBottom(0f);
        linechart.setDescription("");
    }






}
