package com.gevsitech.cr_simple;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;
import com.gevsitech.cr_simple.data_separation.PrimaevalData;
import com.gevsitech.cr_simple.data_separation.ReadCheck;
import com.gevsitech.cr_simple.data_separation.SettingCheck;
import com.gevsitech.cr_simple.data_separation.WriteCheck;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class A_BreakActivity_1 extends AppCompatActivity {

    private Spinner spinner ;
    private EditText edit_init ;
    private EditText edit_final ;
    private EditText edit_min ;
    private EditText edit_max ;
    private Button check_ok ;
    private Button advance_button ;
    private Boolean Sendflag = true;
    private volatile Boolean checkflag = true;
    private Boolean changenumber=true;
    private ProgressDialog dialog;
    private int SendCount = 1 ;
    private SeekBar sb_bkmax,sb_bkmin,sb_bkstart,sb_bkend;
    private String save_init,save_final,save_min,save_max;
    private Button btn_bkstart,btn_bkend,btn_bkmin,btn_bkmax;
    private ImageButton breakbacktoset;



    private File LogPath;
    String logFileName;
    LogService logService = LogService.getLogService();
    private String downFilename;



    private PrimaevalData primaevalData = new PrimaevalData();
    private ReadCheck    readCheck  = new ReadCheck();
    private WriteCheck   writeCheck = new WriteCheck();
    private SettingCheck settingCheck = new SettingCheck();

    BluetoothService BluetoothS = BluetoothService.getBluetoothService();

    private ArrayAdapter<CharSequence> lunchList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a__break_1);

        //螢幕控件初始化
        widgetsInit();

        //隱藏功能


        LogPath = this.getExternalFilesDir(null);
        //取得今日日期
        Calendar mCal = Calendar.getInstance();
        CharSequence ss = DateFormat.format("yyyy-MM-dd", mCal.getTime());    // kk:24小時制, hh:12小時制
        logFileName = String.valueOf(ss)+".txt";

        logService.SaveLog(LogPath, logFileName, "START BreakActivity");



        lunchList = ArrayAdapter.createFromResource(A_BreakActivity_1.this, R.array.Pedal_Type_List ,android.R.layout.simple_list_item_1);
        //spinner.setAdapter(lunchList);
        lunchList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(lunchList);

        //取得藍芽連線服務
        BluetoothS.setBtHandler(BTPoccess);

        //頁面初始讀取
        if(BluetoothS.isConnectedCheck()){
            dialog = ProgressDialog.show(A_BreakActivity_1.this,
                    "讀取初始資料中", "請稍待片刻...",true);
            SendCount = 1 ;
            Sendflag  = true;

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(Sendflag){
                        switch (SendCount) {
                            case 1 :
                                //寫入1
                                char[] s1 = settingCheck.Send("01AD");
                                if(checkflag){
                                    BluetoothS.write2(s1);
                                    SendCount = 2 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入25%");
                                }
                                break;
                            case 2 :
                                char[] s2 = settingCheck.Send("01AE");
                                if(checkflag){
                                    BluetoothS.write2(s2);
                                    SendCount = 3 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入50%");
                                }
                                break;

                            case 3 :

                                char[] s3 = settingCheck.Send("01AF");

                                if(checkflag){
                                    BluetoothS.write2(s3);
                                    SendCount = 4 ;
                                    checkflag = false;
                                    // dialog.setMessage("寫入75%");
                                }

                                break;

                            case 4 :

                                char[] s4 = settingCheck.Send("01B0");

                                if(checkflag){
                                    BluetoothS.write2(s4);
                                    SendCount = 5 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入75%");
                                }

                                break;

                            case 5 :

                                char[] s5 = settingCheck.Send("01B1");

                                if(checkflag){

                                    BluetoothS.write2(s5);
                                    SendCount = 0 ;
                                    checkflag = false;
                                    //dialog.setMessage("寫入100%");
                                }

                                break;

                            case 0 :
                                if(checkflag) {
                                    Sendflag = false;
                                    break;
                                }
                        }

                    }
                    try {
                        dialog.setMessage("寫入成功");
                        Thread.sleep(3000);
                        dialog.dismiss();
                    }catch (Exception e){
                        dialog.dismiss();
                        e.printStackTrace();
                    }

                }
            });

            thread.start();


        }else{
            Toast.makeText(A_BreakActivity_1.this,"藍芽尚未連線，無法讀取資料",Toast.LENGTH_SHORT).show();
            check_ok.setEnabled(false);
            btn_bkend.setEnabled(false);
            btn_bkstart.setEnabled(false);
            btn_bkmin.setEnabled(false);
            btn_bkmax.setEnabled(false);
            logService.SaveLog(LogPath, logFileName, "bluetooth isn't connect");
        }

        breakbacktoset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        advance_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(A_BreakActivity_1.this, A_BreakActivity_2.class);
                startActivity(intent);
            }
        });
        btn_bkstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BluetoothS.isConnectedCheck()){
                    dialog = ProgressDialog.show(A_BreakActivity_1.this,
                            "寫入資料中", "請稍後...", true);
                    SendCount=1;
                    Sendflag=true;
                    Thread thread=new Thread(new Runnable() {
                        @Override
                        public void run() {
                            while(Sendflag){
                                switch(SendCount){
                                    case 1:
                                        char[] s1 = settingCheck.SendWithData("01AE", (short) (Float.valueOf(edit_init.getText().toString()) * 100));

                                        if (checkflag) {
                                            BluetoothS.write2(s1);
                                            SendCount = 0;
                                            checkflag = false;
                                            // dialog.setMessage("25%");
                                        }

                                        break;
                                    case 0:
                                        if (checkflag) {
                                            Sendflag = false;
                                            break;
                                        }
                                }
                            }
                            try {
                                System.out.println("B");
                                dialog.setMessage("寫入成功");
                                Thread.sleep(1000);
                                dialog.dismiss();
                            } catch (Exception e) {
                                dialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();
                }
            }
        });
        btn_bkend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BluetoothS.isConnectedCheck()){
                    dialog = ProgressDialog.show(A_BreakActivity_1.this,
                            "寫入資料中", "請稍後...", true);
                    SendCount=1;
                    Sendflag=true;
                    Thread thread=new Thread(new Runnable() {
                        @Override
                        public void run() {
                            while(Sendflag){
                                switch(SendCount){
                                    case 1:
                                        char[] s1 = settingCheck.SendWithData("01AF", (short) (Float.valueOf(edit_final.getText().toString()) * 100));
                                        if (checkflag) {
                                            BluetoothS.write2(s1);
                                            SendCount = 0;
                                            checkflag = false;
                                            // dialog.setMessage("25%");
                                        }

                                        break;
                                    case 0:
                                        if(checkflag) {
                                            Sendflag = false;
                                            break;
                                        }
                                }
                            }
                            try {
                                System.out.println("B");
                                dialog.setMessage("寫入成功");
                                Thread.sleep(1000);
                                dialog.dismiss();
                            } catch (Exception e) {
                                dialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();
                }
            }
        });
        btn_bkmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BluetoothS.isConnectedCheck()){
                    dialog = ProgressDialog.show(A_BreakActivity_1.this,
                            "寫入資料中", "請稍後...", true);
                    SendCount=1;
                    Sendflag=true;
                    Thread thread=new Thread(new Runnable() {
                        @Override
                        public void run() {
                            while(Sendflag){
                                switch(SendCount){
                                    case 1:
                                        char[] s1 = settingCheck.SendWithData("01B0", (short) (Float.valueOf(edit_min.getText().toString()) * 100));
                                        if (checkflag) {
                                            BluetoothS.write2(s1);
                                            SendCount = 0;
                                            checkflag = false;
                                            // dialog.setMessage("25%");
                                        }

                                        break;
                                    case 0:
                                        if(checkflag) {
                                            Sendflag = false;
                                            break;
                                        }
                                }
                            }
                            try {
                                System.out.println("B");
                                dialog.setMessage("寫入成功");
                                Thread.sleep(1000);
                                dialog.dismiss();
                            } catch (Exception e) {
                                dialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();
                }
            }
        });
        btn_bkmax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BluetoothS.isConnectedCheck()){
                    dialog = ProgressDialog.show(A_BreakActivity_1.this,
                            "寫入資料中", "請稍後...", true);
                    SendCount=1;
                    Sendflag=true;
                    Thread thread=new Thread(new Runnable() {
                        @Override
                        public void run() {
                            while(Sendflag){
                                switch(SendCount){
                                    case 1:
                                        char[] s1 = settingCheck.SendWithData("01B1", (short) (Float.valueOf(edit_max.getText().toString()) * 100));
                                        if (checkflag) {
                                            BluetoothS.write2(s1);
                                            SendCount = 0;
                                            checkflag = false;
                                            // dialog.setMessage("25%");
                                        }

                                        break;
                                    case 0:
                                        if(checkflag) {
                                            Sendflag = false;
                                            break;
                                        }
                                }
                            }
                            try {
                                System.out.println("B");
                                dialog.setMessage("寫入成功");
                                Thread.sleep(1000);
                                dialog.dismiss();
                            } catch (Exception e) {
                                dialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();
                }
            }
        });
        //寫入按鈕
        check_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BluetoothS.isConnectedCheck()) {
                    dialog = ProgressDialog.show(A_BreakActivity_1.this,
                            "寫入資料中", "請稍後...", true);
                    SendCount = 1;
                    Sendflag = true;
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {

                            while (Sendflag) {

                                switch (SendCount) {

                                    case 1:
                                        char[] s1 = settingCheck.SendEEPROMData("01AE");
                                        if (checkflag) {
                                            BluetoothS.write2(s1);
                                            SendCount = 2;
                                            checkflag = false;
                                            // dialog.setMessage("25%");
                                        }
                                        break;

                                    case 2:

                                        char[] s2 =  settingCheck.SendEEPROMData("01AF");
                                        if (checkflag) {
                                            checkflag = false;
                                            BluetoothS.write2(s2);
                                            SendCount = 3;
                                        }
                                        break;

                                    case 3:

                                        char[] s3 = settingCheck.SendEEPROMData("01B0");

                                        if (checkflag) {
                                            checkflag = false;
                                            BluetoothS.write2(s3);
                                            SendCount = 4;
                                        }

                                        break;

                                    case 4:

                                        char[] s4 =  settingCheck.SendEEPROMData("01B1");
                                        if (checkflag) {
                                            BluetoothS.write2(s4);
                                            SendCount = 0;
                                            checkflag = false;
                                        }
                                        break;

                                    case 0:
                                        if(checkflag) {
                                            Sendflag = false;
                                            break;
                                        }
                                }

                            }
                            try {
                                System.out.println("B");
                                dialog.setMessage("寫入成功");
                                Thread.sleep(1000);
                                dialog.dismiss();
                            } catch (Exception e) {
                                dialog.dismiss();
                                e.printStackTrace();
                            }

                        }
                    });
                    thread.start();
                    logService.SaveLog(LogPath, logFileName, "start save data");
                    logService.SaveLog(LogPath, logFileName, "剎車初值:" + edit_init.getText().toString());
                    logService.SaveLog(LogPath, logFileName, "剎車終值:" + edit_final.getText().toString());
                    logService.SaveLog(LogPath, logFileName, "剎車min:" + edit_min.getText().toString());
                    logService.SaveLog(LogPath, logFileName, "剎車max:" + edit_max.getText().toString());
                }



            }
        });
        sb_bkstart.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Float a=(float)(sb_bkstart.getProgress()/100.00f*12);
                if(changenumber) {
                    edit_init.setText(String.format("%.2f",a));
                }
                else{
                    edit_init.setText(save_init);
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        sb_bkend.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Float a=(float)(sb_bkend.getProgress()/100.00f*12);
                if(changenumber) {
                    edit_final.setText(String.format("%.2f",a));

                }
                else{
                    edit_final.setText(save_final);
                    changenumber=true;
                }


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        sb_bkmin.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Float a=(float)(sb_bkmin.getProgress()/100.00f);
                if(changenumber) {
                    edit_min.setText(String.format("%.2f",a));

                }
                else{
                    edit_min.setText(save_min);
                    changenumber=true;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        sb_bkmax.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Float a=(float)(sb_bkmax.getProgress()/100.00f);
                if(changenumber) {
                    edit_max.setText(String.format("%.2f",a));

                }
                else{
                    edit_max.setText(save_max);
                    changenumber=true;
                }

            }

            @Override

            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        edit_init.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(edit_init.getText().toString().length() > 0) {
                        int a =(int)(Float.valueOf(edit_init.getText().toString())/12.00f*100);
                        save_init=edit_init.getText().toString();
                        changenumber=false;
                        sb_bkstart.setProgress(a);
                    }
                }
                return false;
            }
        });
        edit_final.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(edit_final.getText().toString().length() > 0) {
                        int a =(int)(Float.valueOf(edit_final.getText().toString())/12.00f*100);
                        save_final=edit_final.getText().toString();
                        changenumber=false;
                        sb_bkend.setProgress(a);
                    }
                }
                return false;
            }
        });
        edit_min.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(edit_min.getText().toString().length() > 0) {
                        int a =(int)(Float.valueOf(edit_min.getText().toString())*100);
                        save_min=edit_min.getText().toString();
                        changenumber=false;
                        sb_bkmin.setProgress(a);
                    }
                }
                return false;
            }
        });
        edit_max.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    if(edit_max.getText().toString().length() > 0) {
                        int a =(int)(Float.valueOf(edit_max.getText().toString())*100);
                        save_max=edit_max.getText().toString();
                        changenumber=false;
                        sb_bkmax.setProgress(a);
                    }
                }
                return false;
            }
        });




    }

    private void widgetsInit(){
        spinner = (Spinner)findViewById(R.id.padel_spinner);
        edit_init = (EditText)findViewById(R.id.break_init_Edit);
        edit_final = (EditText)findViewById(R.id.break_final_Edit);
        edit_min = (EditText)findViewById(R.id.break_min_Edit);
        edit_max = (EditText)findViewById(R.id.break_max_Edit);
        check_ok = (Button)findViewById(R.id.break_ok);
        advance_button = (Button)findViewById(R.id.go_advanced_setting_bk);
        btn_bkstart = (Button)findViewById(R.id.btn_bkstart);
        btn_bkend = (Button)findViewById(R.id.btn_bkend);
        btn_bkmin = (Button)findViewById(R.id.btn_bkmin);
        btn_bkmax = (Button)findViewById(R.id.btn_bkmax);
        breakbacktoset = (ImageButton) findViewById(R.id.breakbacktoset);
        sb_bkmax=(SeekBar) findViewById(R.id.sb_bkmax);
        sb_bkmin=(SeekBar) findViewById(R.id.sb_bkmin);
        sb_bkend=(SeekBar) findViewById(R.id.sb_bkend);
        sb_bkstart=(SeekBar) findViewById(R.id.sb_bkstart);
        sb_bkmax=(SeekBar) findViewById(R.id.sb_bkmax);
        sb_bkmin=(SeekBar) findViewById(R.id.sb_bkmin);
        sb_bkend=(SeekBar) findViewById(R.id.sb_bkend);
        sb_bkstart=(SeekBar) findViewById(R.id.sb_bkstart);
    }

    private Handler BTPoccess = new Handler(){
        private StringPoccess poccess = new StringPoccess();


        private String data ;

        private boolean tagflag = false;

        /**
         *  連線中狀態值
         *  1. DISCONNECT 斷線狀態    0
         *  2. CONNECTED  連線中      1
         *  3. CONNECTING 嘗試連線中  2
         **/
        private final static int DISCONNECT = 0 ;
        private final static int CONNECTED = 1 ;
        private final static int CONNECTING = 2 ;

        /**
         *  連線中狀態值
         *  1. DATAWAIT   等待資料中    0
         *  2. DATAREAD   資料讀取      1
         *  3. DATAWIRTE  資料寫入      2
         **/

        private final static int DATAWAIT = 0 ;
        private final static int DATAREAD = 1 ;
        private final static int DATAWIRTE = 2 ;

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case DISCONNECT :
                    if(tagflag) Log.d("Handle" ,"DISCONNECT" );
                    break;
                case CONNECTED :
                    System.out.println("here i am");
                    if(tagflag)Log.d("Handle" ,"CONNECTED" );

                    if(msg.arg1 == DATAREAD) {
                        data = (String) msg.obj;
                        if(data == null)return;
                        System.out.println(data);
                        primaevalData.split(data);
                    }

                    //System.out.println(data);
                    System.out.println(primaevalData.startcheck());
                    if(primaevalData.startcheck()) {
                        System.out.println("yes1");
                        if (primaevalData.getId()[2]==0xCE) {
                            System.out.println("yes2");
                            checkflag = true;
                            writeCheck.setIDCheck(primaevalData.getdata());
                            if (writeCheck.CheckRAMWrite()) {
                                checkflag = true;
                                System.out.println("yes3");
                            }if (writeCheck.CheckEEPROMWrite()) {
                                checkflag = true;
                                System.out.println("yes3");
                            }
                        }

                        if (primaevalData.getId()[2]==0xCF) {
                            System.out.println("yes4");
                            readCheck.setData(primaevalData.getdata());
                            System.out.println(readCheck.getAddress());
                            if (readCheck.getAddress().equals("1ad")) {
                                System.out.println("yes5");
                                //Float a = (float)readCheck.getValues()/100.0f;
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("1ae")) {
                                Float a = (float) readCheck.getValues() / 100.00f;
                                int b = (int) (a / 12.00f * 100);
                                String s = String.valueOf(a);
                                System.out.println(readCheck.getAddress());
                                System.out.println(a);
                                System.out.println(s);
                                edit_init.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load break_init:" + s);
                                readCheck.getValues();
                                sb_bkstart.setProgress(b);
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("1af")) {
                                Float a = (float) readCheck.getValues() / 100.00f;
                                int b = (int) (a / 12.00f * 100);
                                String s = String.valueOf(a);
                                edit_final.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load break_end:" + s);
                                readCheck.getValues();
                                sb_bkend.setProgress(b);
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("1b0")) {
                                Float a = (float) readCheck.getValues() / 100.00f;
                                int b = (int) (a * 100);
                                String s = String.valueOf(a);
                                edit_min.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load break_min:" + s);
                                readCheck.getValues();
                                sb_bkmin.setProgress(b);
                                checkflag = true;
                            } else if (readCheck.getAddress().equals("1b1")) {
                                Float a = (float) readCheck.getValues() / 100.00f;
                                int b = (int) (a * 100);
                                String s = String.valueOf(a);
                                edit_max.setText(s);
                                logService.SaveLog(LogPath, logFileName, "load break_max:" + s);
                                readCheck.getValues();
                                checkflag = true;
                                sb_bkmax.setProgress(b);
                            } else {
                                checkflag = false;
                            }
                        }
                    }

                    break;
                case CONNECTING :
                    if(tagflag)Log.d("Handle" ,"CONNECTING" );
                    break;
            }
        }
    };

}
