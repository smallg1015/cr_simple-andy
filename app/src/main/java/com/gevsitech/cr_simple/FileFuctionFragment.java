package com.gevsitech.cr_simple;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gevsitech.cr_simple.data_separation.SaveTimeclass;
import com.gevsitech.cr_simple.data_separation.Savelag;
import com.kyleduo.switchbutton.SwitchButton;

import java.io.File;


/**

 */
public class FileFuctionFragment extends Fragment {
    private ListView Filelist;
    private String[] filenamearray=new String[]{"啟動紀錄","資料圖表","檔案上傳至雲端"/*"自雲端下載檔案"*/,"清除手機內部檔案"};
    private String[] filesubarray=new String[]{"","","","",""};
    FileViewHolder viewHolder;
    FileViewHolder2 viewHolder2;
    private Boolean savelag;
    private Savelag savelagclass=new Savelag();
    private File LogPath;
    private UploadFile FileUpload;
    private String UploadURL;
    private SaveTimeclass saveTimeclass=new SaveTimeclass();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_file_fuction, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        Filelist=(ListView)getView().findViewById(R.id.FileList);

        Fileadapter fileadapter=new Fileadapter(getActivity());

        Filelist.setAdapter(fileadapter);
        Filelist.setFocusableInTouchMode(true);

        LogPath=new File(Environment.getExternalStorageDirectory().getPath()+"/CR_simple");

        //獲取手機id作為上傳檔案辨認哪個裝置
        String androidId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        FileUpload=new UploadFile(androidId);
        UploadURL="http://wiper.gevsi-tech.com/uploadLogtest.php";


        savelag=savelagclass.getSavelag();



        Filelist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){

                    case 0:{


                    }
                    break;
                    case 1:{
                        showFragment(new FileFragment());
                    }
                    break;
                    case 2:{
                        Thread thread=new Thread(new Runnable() {
                            @Override
                            public void run() {
                                File[] file=LogPath.listFiles();
                                File[] fileday=null;
                                int i=0;
                                int j=0;
                                for(i=0;i<file.length;i++){
                                    if(file[i].isFile()){
//                                        FileUpload.uploadFile(UploadURL,file[i],new Thread(new Runnable() {
//                                            @Override
//                                            public void run() {
//
//                                            }
//                                        }));
//                                        System.out.println("UPLOAD FILE");
                                    }else{
                                        String datefile=file[i].getName();
                                        fileday=file[i].listFiles();
                                        for(j=0;j<fileday.length;j++){
                                            FileUpload.uploadFile(UploadURL,datefile,fileday[j],new Thread(new Runnable() {
                                                @Override
                                                public void run() {

                                                }
                                            }));
                                            System.out.println("UPLOAD FILE");

                                        }
                                    }
                                }
                                System.out.println("FILE LENGTH:"+file.length);
                                System.out.println("I:"+i);
                                System.out.println("J:"+j);
                                System.out.println("FILEDAY LENGTH:"+fileday.length);
                                System.out.println(i==file.length&&j==fileday.length);
                                if(i==file.length&&j==fileday.length){
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            System.out.println("I AM HERE");
                                            Toast.makeText(getActivity(),"檔案上傳成功",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }

                            }
                        });thread.start();


                    }
                    break;
                    case 3:{
                        File[] file=LogPath.listFiles();
                        File[] fileday=null;
                        int i=0;
                        int j=0;
                        for(i=0;i<file.length;i++){
                            if(file[i].isFile()){
                                file[i].delete();
                            }else{
                                fileday=file[i].listFiles();
                                for(j=0;j<fileday.length;j++){
                                    fileday[j].delete();
                                }

                            }
                        }
                        Toast.makeText(getActivity(),"已經清除所有檔案",Toast.LENGTH_LONG).show();
                    }
                    break;
                }
            }
        });

    }



    public class Fileadapter extends BaseAdapter{
        final int TYPE_1=0;
        final int TYPE_2=1;
        private LayoutInflater myInflater;
        public Fileadapter(Context c){
            myInflater=LayoutInflater.from(c);
        }
        @Override
        public int getCount(){
            return filenamearray.length;
        }
        @Override
        public Object getItem(int position){
            return filenamearray[position];
        }
        @Override
        public long getItemId(int postion){
            return postion;
        }
        @Override
        public int getItemViewType(int position){
            int p=position;
            if(p==0){
                return TYPE_1;
            }else{return TYPE_2;}

        }
        @Override
        public int getViewTypeCount(){return 2;}

        @Override
        public View getView(int position,View convertView,ViewGroup parent){
            viewHolder=null;
            viewHolder2=null;

            if(convertView==null){
                switch(getItemViewType(position)){
                    case TYPE_1:
                        viewHolder=new FileViewHolder();
                        convertView=myInflater.inflate(R.layout.setting_haveswitch,null);
                        viewHolder.textView=(TextView)convertView.findViewById(R.id.sw_txt_listhead);
                        viewHolder.switch_open=(SwitchButton)convertView.findViewById(R.id.swh_setopen);
                        viewHolder.edt_savetime=(EditText)convertView.findViewById(R.id.edt_savetime);

                        viewHolder.textView.setText(filenamearray[position]);
                        viewHolder.switch_open.setFocusable(true);
                        if(savelag){
                            viewHolder.switch_open.setChecked(true);
                        }else{
                            viewHolder.switch_open.setChecked(false );
                        }
                        viewHolder.switch_open.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                                    if(!savelag) {
                                        if(!viewHolder.edt_savetime.getText().toString().equals("")){
                                            Toast.makeText(getActivity(), "啟動紀錄功能", Toast.LENGTH_LONG).show();
                                            savelag=true;
                                            savelagclass.saveSavelag(savelag);
                                            viewHolder.switch_open.setChecked(true);
                                            saveTimeclass.putsavetime(Integer.valueOf(viewHolder.edt_savetime.getText().toString()));
                                            viewHolder.edt_savetime.setFocusable(false);
                                        }else{
                                            Toast.makeText(getActivity(), "請輸入間隔時間,重新開啟", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                    else{
                                        Toast.makeText(getActivity(), "關閉紀錄功能", Toast.LENGTH_LONG).show();
                                        savelag=false;
                                        savelagclass.saveSavelag(savelag);
                                        viewHolder.switch_open.setChecked(false);
                                        viewHolder.edt_savetime.setFocusable(true);

                                      }
                            }
                        });


//                        viewHolder.edt_savetime.setOnKeyListener(new View.OnKeyListener() {
//                            @Override
//                            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                                if(keyCode==66){
//                                    viewHolder.edt_savetime.setFocusable(false);
//                                }
//                                return false;
//                            }
//                        });

                        convertView.setTag(viewHolder);
                        break;

                    case TYPE_2:
                        viewHolder2=new FileViewHolder2();
                        convertView=myInflater.inflate(R.layout.list_setting,null);
                        viewHolder2.txt_head=(TextView)convertView.findViewById(R.id.txt_listhead);
                        viewHolder2.txt_content=(TextView)convertView.findViewById(R.id.txt_listsub);

                        viewHolder2.txt_head.setText(filenamearray[position]);
                        viewHolder2.txt_content.setText(filesubarray[position]);

                        convertView.setTag(viewHolder2);
                        break;

                }
            }else{
                switch (getItemViewType(position)){
                    case TYPE_1:
                        viewHolder=(FileViewHolder)convertView.getTag();
                        viewHolder.textView.setText(filenamearray[position]);
                        viewHolder.switch_open.setFocusable(true);
                        if(savelag){
                            viewHolder.switch_open.setChecked(true);
                        }else{
                            viewHolder.switch_open.setChecked(false );
                        }
                        break;
                    case TYPE_2:
                        viewHolder2=(FileViewHolder2) convertView.getTag();
                        viewHolder2.txt_head.setText(filenamearray[position]);
                        viewHolder2.txt_content.setText(filesubarray[position]);

                }
            }



            return convertView;

        }
    }

    //显示Fragment
    private void showFragment(Fragment fragment) {
        //先隐藏
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.center,fragment);
        ft.addToBackStack(null);
        ft.commit();
    }


    private static class FileViewHolder{
        TextView textView;
        static SwitchButton switch_open;
        static EditText edt_savetime;
    }
    private static class FileViewHolder2{
        TextView txt_head;
        TextView txt_content;
    }
}
