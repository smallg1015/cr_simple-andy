package com.gevsitech.cr_simple;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.gevsitech.cr_simple.data_separation.SettingCheck;

public class CRSetting extends Fragment {

    private ListView listView;
    private String[] list = {"油門設定"/*"驅動輸出","Digital Input","Driver Outpt"*/,"響應速率設定","保護設定"};
    private ArrayAdapter<String> listAdapter;
    private SettingCheck settingCheck=new SettingCheck();
    private SharedPreferences prepassword;
    private BluetoothService bluetoothService=BluetoothService.getBluetoothService();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_crsetting, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);

        prepassword=getActivity().getSharedPreferences("prepassword", Context.MODE_PRIVATE);

        if(bluetoothService.isConnectedCheck()){
            bluetoothService.write2(settingCheck.SendPassword("03EE",prepassword.getString("password","0000")));
        }
        listView = (ListView)getView().findViewById(R.id.crsetting);
        listAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, list);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Toast.makeText(getApplicationContext(), "你選擇的是" + list[position], Toast.LENGTH_SHORT).show();
                // Toast.makeText(getApplicationContext(), "你選擇的是" +position, Toast.LENGTH_SHORT).show();
                switch ( position)
                {
                    case 0:
                        // 切換至設定頁面
                        Intent intent0 = new Intent();
                        intent0.setClass(getActivity(), A_PedalActivity_1.class);
                        CRSetting.this.startActivity(intent0);
                        break;

//                    case 1:
//                        // 切換至設定頁面
//                        Intent intent1 = new Intent();
//                        intent1.setClass(getActivity(), A_BreakActivity_1.class);
//                        CRSetting.this.startActivity(intent1);
//                        break;
//                    case 2:
//                        // 切換至動力輸出設定頁面
//                        Intent intent2 = new Intent();
//                        intent2.setClass(getActivity(), Actout.class);
//                        CRSetting.this.startActivity(intent2);
//                        break;



//                    case 3:
//                        // 切換至設定 Digital Input 設定頁面
//                        Intent intent3 = new Intent();
//                        intent3.setClass(getActivity(),CR_digital_in.class);
//                        CRSetting.this.startActivity(intent3);
//                        break;

//                    case 4:
//                        // 切換至設定 Digital Output 設定頁面
//                        Intent intent4 = new Intent();
//                        intent4.setClass(getActivity(),CR_digital_out.class);
//                        CRSetting.this.startActivity(intent4);
//                        break;
                    case 1:
                        // 切換至設定 靈敏度設定頁面
                        Intent intent5 = new Intent();
                        intent5.setClass(getActivity(), CR_response.class);
                        CRSetting.this.startActivity(intent5);
                        break;
                    case 2:
                        // 切換至設定頁面
                        Intent intent6 = new Intent();
                        intent6.setClass(getActivity(), Protection.class);
                        CRSetting.this.startActivity(intent6);
                        break;

                    /*case 2:
                        // 切換至動力輸出設定頁面
                        Intent intent2 = new Intent();
                        intent2.setClass(CRSetting.this, B_CRSettingPowerOutActivity.class);
                        CRSetting.this.startActivity(intent2);
                        break;





                    case 6:
                        // 切換至設定頁面
                        Intent intent6 = new Intent();
                        intent6.setClass(CRSetting.this, BuildActivity.class);
                        CRSetting.this.startActivity(intent6);
                        break;*/

                }
            }
        });

    }
}
