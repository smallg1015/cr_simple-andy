package com.gevsitech.cr_simple;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;

//import okhttp3.Call;
//import okhttp3.Callback;
//import okhttp3.OkHttpClient;
//import okhttp3.Request;
//import okhttp3.Response;

import static android.R.attr.path;
import static android.R.attr.start;


public class
FileFragment extends Fragment {
    private SharedPreferences prefile;
    private String[] datearray;
    private String[] filearray;
    private String url,urldate,urltime,urltimefile,state,nowdate,nowtime,prewhat;
    String filenameplusform,filename,fileform;
    private ListView list_file;
    private EditText edt_date;
    private Button btn_date;
    private int downloadnumber;

//    private OkHttpClient client=new OkHttpClient();
    private Button button;
    private Spinner spn_date;
    private Handler handler;
    private volatile boolean Savelag=false;
    private volatile boolean Sendlag=true;
    private Timer downloadTimer;


    ArrayAdapter<String> dateAdapter;
    private int mYear, mMonth, mDay,muchfile;


    private String filesave;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_file, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        //控建初始化區

        list_file=(ListView)getView().findViewById(R.id.list_file);
        btn_date=(Button)getView().findViewById(R.id.btn_date);
        edt_date=(EditText)getActivity().findViewById(R.id.edt_date);

        prefile=getActivity().getSharedPreferences("prefile",Context.MODE_PRIVATE);

        filesave=Environment.getExternalStorageDirectory().getPath()+"/CR_simple";


        //判斷是否有文件存取權限，有則檢查資料夾是否創建，無則提醒使用者注意。
        requestStoragePermission();


        btn_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();

            }
        });


        //Listview響應區
        list_file.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String nowfile=list_file.getItemAtPosition(position).toString();
                String nowfilepath=filesave+File.separator+edt_date.getText().toString()+File.separator+nowfile;
                System.out.println(nowfilepath);
                prefile.edit().putString("nowfilepath",nowfilepath).commit();
                showFragment(new LineChartFragment());


            }
        });








    }

    //显示Fragment
    private void showFragment(Fragment fragment) {
        //先隐藏
        FragmentManager fm=getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.center,fragment);
        ft.addToBackStack(null);
        ft.commit();
    }



    //判斷是否取得權限
    private void requestStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            int hasPermission = getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
                return;//如果權限未開啟,則跳出取得權限對話框

            }

        }
        File file=new File(filesave);
        if (!file.exists()) {//先判斷目錄存不存在
            file.mkdirs();}
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions, int[] grantResults) {
        if(requestCode==1){


            if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                File file=new File(filesave);
                if (!file.exists()) {//先判斷目錄存不存在
                    file.mkdirs();}
            }
            else {
                new AlertDialog.Builder(getActivity())
                        .setTitle("警告")
                        .setIcon(R.mipmap.ic_launcher)
                        .setMessage("若不允許存取權限,程式將無法執行功能,是否取得權限?")
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {
                                requestStoragePermission();
                            }
                        })

                        .setNegativeButton("拒絕", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {
                                getActivity().finish();
                            }
                        }).show();
            }

            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }}

    public void showDatePickerDialog() {
        // 設定初始日期
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        // 跳出日期選擇器
        DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // 完成選擇，顯示日期
                        edt_date.setText(setdateform(year,monthOfYear,dayOfMonth));
                        listdatefile();

                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    public String setdateform(int year,int month,int date) {
        String Smonth;
        String Sdate;

        if(String.valueOf(month).length()==1){
            Smonth="0"+String.valueOf(month+1);
        }else{Smonth=String.valueOf(month+1);}
        if(String.valueOf(date).length()==1){
            Sdate="0"+String.valueOf(date);
        }else{Sdate=String.valueOf(date);}

        return String.valueOf(year)+"-"+Smonth+"-"+Sdate;
    }


    public void listdatefile(){
        File filedate=new File(Environment.getExternalStorageDirectory().getPath()+"/CR_simple/"+edt_date.getText().toString());
        ArrayList<String> filenamearray=new ArrayList<String>();
        if(filedate.exists()){
            File[] filearray=filedate.listFiles();
            if(filearray.length!=0){
                for(int f=0;f<filearray.length;f++) {
                    filenamearray.add(filearray[f].getName());
                }
            }else{filenamearray.add("no files");}
        }else{filenamearray.add("no files");}

        ArrayAdapter<String> filesadapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,filenamearray);
        list_file.setAdapter(filesadapter);
        list_file.invalidateViews();

    }
}