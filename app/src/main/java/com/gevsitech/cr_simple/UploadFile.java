package com.gevsitech.cr_simple;

import java.io.File;
import java.io.IOException;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Jian on 2017/4/9.
 */


public class UploadFile {
    private String androidId;
    public  UploadFile(String id){
        androidId = id;
    }
    public Boolean uploadFile(String serverURL,String date, File file, final Thread thread) {
        System.out.println(file.getAbsolutePath());
        System.out.println(file.getName());
        OkHttpClient client = new OkHttpClient();
        try {
            MultipartBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("androidId",androidId)
                    .addFormDataPart("date",date)
                    .addFormDataPart("txt", file.getName(),
                            RequestBody.create(MediaType.parse("text/plain"), file))

                    .build();

            Request request = new Request.Builder()
                    .url(serverURL)
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {

                @Override
                public void onFailure(Call call, IOException e) {
                    System.out.println("bybyonFailure");

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        // Handle the error
                    }else{
                        System.out.println(response.body().string());
                        thread.start();

                    }
                }
            });

            return true;
        } catch (Exception ex) {
            // Handle the error
        }
        return false;
    }
}
