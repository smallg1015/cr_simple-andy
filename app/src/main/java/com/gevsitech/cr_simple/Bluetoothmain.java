package com.gevsitech.cr_simple;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Set;

public class Bluetoothmain extends AppCompatActivity {
    private String path = Environment.getExternalStorageDirectory().getPath()+"/MPFMotor";
    private String filename ="",filekey="";
    private String[] deviceString;
    private String[] deviceMAC;
    private String[] filenames;

    private int i;
    private int BT_count ;
    private int count = 0;

    private BluetoothService bluetoothService;

    //控鍵類型.................................
    private Button button ;
    private Button button2 ;
    private Button download_button ;
    private Button filebutton ;

    private TextView textView;

    private String StartString;
    private static Thread thread;
    private static Thread thread1;
    private static boolean data_Tran_OK =false;
    private SplitHex splitHex;

    private static Boolean BootLoader_Start_flag = false;
    private static Boolean BootLoader_Eraser_Start_flag = false;
    private static Boolean flag1 = false;

    private ProgressBar progressBar;

    SharedPreferences settings;
    private SharedPreferences preautobluename;


    private ImageView imagebluestate;
    private HTTPDownload httpDownload ;
    private TextView txtfilename;
    private TextView txtloaderstate;
    private TextView textView3;

    String s1="尚未連線",s2="",s3="",s4="";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetoothmain);



        imagebluestate=(ImageView)findViewById(R.id.imagebluestate);
        txtfilename=(TextView)findViewById(R.id.txtfilename);
        txtloaderstate=(TextView)findViewById(R.id.txtloaderstate);
        button2 = (Button)findViewById(R.id.button2);
        filebutton = (Button)findViewById(R.id.button3);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        textView3 = (TextView)findViewById(R.id.textView3);

        saveToPictureFolder();

        bluetoothService = BluetoothService.getBluetoothService();
        bluetoothService.setBtHandler(BTPoccess);
        Boolean bluetoothconnect= bluetoothService.isConnectedCheck();
        if(bluetoothconnect){
            data_Tran_OK=true;
            imagebluestate.setImageResource(R.mipmap.presence_online);
            preautobluename=getSharedPreferences("preautobluename", MODE_PRIVATE);
            String autobluetoothname=preautobluename.getString("bluetoothname", "");
            s1="連線裝置:"+autobluetoothname;



        }
        System.out.println(bluetoothconnect);

        txtloaderstate.setText(s1+"\n"+s2+"\n"+s3+"\n"+s4);
        requestStoragePermission();





        //檢測是否有預設檔案，如無則不執行
        if(!readData().equals("")){
            filename = readData();
            createStartString();
            txtfilename.setText(filename);
        }


        filebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                File dir = new File(path);
                filenames = dir.list();
                System.out.println(dir.getPath());
                System.out.println(dir.list());

                new AlertDialog.Builder(Bluetoothmain.this).setTitle("選擇檔案").setIcon(
                        android.R.drawable.ic_dialog_info).setSingleChoiceItems(
                        filenames, 0,
                        new DialogInterface.OnClickListener () {
                            public void onClick(DialogInterface dialog, int which) {
                                filename = filenames[which];
                                filekey="yes";


                            }
                        }).setPositiveButton("選擇", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(filekey.equals("")){
                            filename = filenames[0];
                        }
                        saveData();
                        createStartString();
                        txtfilename.setText(filename);
                        txtloaderstate.setText(s1);
                        progressBar.setProgress(0);
                        textView3.setText(0+"%");
                        filekey="";
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                    }
                }).create().show();
            }
        });

        button2.setOnClickListener(

                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!filename.equals("")) {


                    if (data_Tran_OK) {
                        bluetoothService.write("<AE,8,0,F6,1,1,CD,AB,0,0");

                        //回傳CE0000000000
                        flag1 = false;
                        BootLoader_Start_flag = false;

                        new Handler().postDelayed(
                                new Runnable() {
                            public void run() {
                                System.out.println(StartString);
                                bluetoothService.write(StartString);
                                //textView.setText("燒錄進行中..請稍後");
                                s3 = "燒錄進行中..請稍後";
                                txtloaderstate.setText(s1 + "\n" + "" + "\n" + s3);
                                filebutton.setEnabled(false);
                            }
                        }, 1000);
                    } else {
                        Toast.makeText(Bluetoothmain.this, "尚未進行連線", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(Bluetoothmain.this, "尚未選擇檔案", Toast.LENGTH_SHORT).show();
                }


            }
        });

        /*download_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog();
            }
        });*/


    }


    private void requestStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            int hasPermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
                return;//如果權限未開啟,則跳出取得權限對話框

            }

        }
        File file = new File(path);
        if (!file.exists()) {//先判斷目錄存不存在
            file.mkdirs();}

        }

    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions, int[] grantResults) {
        if(requestCode==1){
            if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                File file = new File(path);
                if (!file.exists()) {//先判斷目錄存不存在
                    file.mkdirs();}
            }
            else {
                new AlertDialog.Builder(Bluetoothmain.this)
                        .setTitle("警告")
                        .setIcon(R.mipmap.presence_invisible)
                        .setMessage("若不允許存取權限,程式將無法執行功能,是否取得權限?")
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {
                                requestStoragePermission();
                                }
                            })

                        .setNegativeButton("拒絕", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {
                                finish();
                            }
                        }).show();
            }

            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }}

    /**
     * 儲存到圖片庫
     */
    private boolean saveToPictureFolder() {
        //取得 Pictures 目錄
        File picDir = Environment.getDataDirectory();
        Log.d(">>>", "path: " + picDir.getAbsolutePath());
        //假如有該目錄
        if (picDir.exists()) {
        }
        return false;
    }

    //writeToFile 方法如下
    private void writeToFile(File fout, String data) {
        FileOutputStream osw = null;
        try {
            osw = new FileOutputStream(fout);
            osw.write(data.getBytes());
            osw.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                osw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(!(bluetoothAdapter==null)){
            if(!(bluetoothAdapter.isEnabled()))bluetoothAdapter.enable();
            //Toast.makeText(EvtActivity.this, "自動開啟藍芽", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(Bluetoothmain.this, "Your Cell Phone not suppose Bluetooth", Toast.LENGTH_SHORT).show();
        }
    }
    //@Override
    //protected void onPause(){
        //bluetoothService.DisConnect();

    //}

    private Handler BTPoccess = new Handler(){
        boolean tagflag = false;
        String  data ;
        /**
         *  連線中狀態值
         *  1. DISCONNECT 斷線狀態    0
         *  2. CONNECTED  連線中      1
         *  3. CONNECTING 嘗試連線中  2
         **/
        private final static int DISCONNECT = 0 ;
        private final static int CONNECTED = 1 ;
        private final static int CONNECTING = 2 ;

        /**
         *  連線中狀態值
         *  1. DATAWAIT   等待資料中    0
         *  2. DATAREAD   資料讀取      1
         *  3. DATAWIRTE  資料寫入      2
         **/

        private final static int DATAWAIT = 0 ;
        private final static int DATAREAD = 1 ;
        private final static int DATAWIRTE = 2 ;
        StringBuilder builder = new StringBuilder();
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what){


                case DISCONNECT :
                    if(tagflag) Log.d("Handle" ,"DISCONNECT" );
                    imagebluestate.setImageResource(R.mipmap.presence_invisible);
                    s1="未連線";
                    txtloaderstate.setText(s1);
                    progressBar.setProgress(0);
                    textView3.setText(0+"%");
                    filebutton.setEnabled(true);
                    break;


                case CONNECTED :
                    //如成功連線，則拋出連線成功訊息，供使用者辨識
                    if(tagflag)Log.d("Handle" ,"CONNECTED" );

                    if(msg.arg1 == DATAREAD) {
                        //取得回傳資訊
                        data = (String) msg.obj;

                        if(BootLoader_Start_flag){
                            if(data.substring(0,3).equals("<CD")){
                                if(data.substring(6,11).equals("AA,AA")){
                                    if(thread!=null){
                                        thread.interrupt();
                                        s3="寫入失敗,請重試";
                                        txtloaderstate.setText(s1+"\n"+""+"\n"+s3);
                                        progressBar.setProgress(0);
                                        textView3.setText(0+"%");
                                        BootLoader_Start_flag = false;
                                    }
                                    //startThread();
                                }
                            }
                        }

                        if(data.substring(0,3).equals("<CD")){
                            if(data.substring(6,9).equals("0,0")){
                                BootLoader_Eraser_Start_flag = true;
                                if(!flag1){
                                    startThread();
                                    thread.start();
                                    flag1 = true;
                                    BootLoader_Start_flag = true;
                                }
                            }
                        }
                        data_Tran_OK = true;
                    }

                    if(msg.arg1 == 5) {
                        //textView.setText("已連線，等待燒錄中..");
                        imagebluestate.setImageResource(R.mipmap.presence_online);
                        s1="連線裝置:"+deviceString[BT_count];
                        txtloaderstate.setText(s1);
                        data_Tran_OK = true;
                    }

                    break;
                case CONNECTING :
                    if(tagflag)Log.d("Handle" ,"CONNECTING" );
                    break;
            }

            switch (msg.arg2){
            }
        }
    };


    private void startThreadTest(){
        thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                   Thread.sleep(100);
                    while (true){
                        Thread.sleep(100);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }

            }
        });
    }
    private void customDialog(){
        final View item = LayoutInflater.from(Bluetoothmain.this).inflate(R.layout.edit_layout, null);
        new AlertDialog.Builder(Bluetoothmain.this)
                .setTitle("輸入下載碼")
                .setView(item)
                .setPositiveButton("Certain", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final EditText editText = (EditText) item.findViewById(R.id.editText);
                        Thread thread01 = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                HTTPDownload httpDownload = new HTTPDownload(path);
                                File dir = Bluetoothmain.this.getExternalFilesDir(null);
                                int a = httpDownload.downFile("http://cloudla.gevsi-tech.com/uploads/"+ editText.getText()+".hex",path+File.separator,editText.getText().toString()+".hex");
                                if(a != 0){
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(Bluetoothmain.this , "下載碼錯誤，請重新確認一次!",Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(Bluetoothmain.this , "下載完成!",Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        });
                        thread01.start();
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }}
        ).show();
    }
    private void startThread(){
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    FileReader fr = new FileReader(path+File.separator+filename);
                    BufferedReader br = new BufferedReader(fr);
                    String readData = "";
                    String temp = "Start"; //readLine()讀取一整行

                    count=0;
                    while (true){
                        temp=br.readLine();
                        splitHex = new SplitHex(temp);
                        if(splitHex.getStatus().equals("04")){
                            temp=br.readLine();
                            splitHex = new SplitHex(temp);
                            Thread.sleep(30);
                            System.out.println(splitHex.getAddressOffset());
                            //傳送地址偏移
                            bluetoothService.write(splitHex.getAddressOffset());

                            //傳送地址偏移
                            String[] s = splitHex.getDatalist();
                            for(int k = 0 ; k < s.length ;k++){

                                Thread.sleep(30);
                                bluetoothService.write(s[k]);
                            }

                            if(s.length == 0) {
                                Thread.sleep(30);
                                bluetoothService.write(splitHex.getAlongString());
                            }

                            if(splitHex.getAddress().equals("3FFE")) {
                                break;
                            }

                        }else{
                            splitHex = new SplitHex(temp);
                            String[] s = splitHex.getDatalist();
                            bluetoothService.write(splitHex.getAddressOffset());
                            for(int k = 0 ; k < s.length ;k++){
                                Thread.sleep(30);
                                if(s[k]!=null) bluetoothService.write(s[k]);
                            }

                            if(s.length == 0) {
                                Thread.sleep(30);
                                //no
                                bluetoothService.write(splitHex.getAlongString());
                            }
                        }
                        count++;
                        progressBar.setProgress(count);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                float a=progressBar.getProgress();
                                int b=(int)(a/169*100);
                                textView3.setText(b+"%");
                            }
                        });

                    }
                    Thread.sleep(30);
                    bluetoothService.write("<AD,8,00,00,00,00,00,00,00,00");
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            s4="燒錄完成";
                            filebutton.setEnabled(true);
                            txtloaderstate.setText(s1+"\n"+""+"\n"+s4);
                           // textView.setText("燒錄完成");
                        }
                    });
                }catch(Exception e){
                    e.printStackTrace();
                }

            }
        });
    }
    private void createStartString(){

        splitHex = new SplitHex();
        int a = splitHex.length(path,filename);
        if (a > 0) {
            String aa = Integer.toHexString(a);
            if(aa.length()==3)  aa = "0"+ aa;
            if(aa.length()==2)  aa = "00"+ aa;
            progressBar.setMax(a);
            String Hbyte="00";
            String Lbyte  = aa.substring(2,4);
            if(aa.length()>2) Hbyte=aa.substring(0,2);
            StartString = "<AD,8,AA,AA,"+Lbyte.toUpperCase() +","+Hbyte.toUpperCase() +",00,00,00,00";
        }

    }
    public void saveData(){
        settings = getSharedPreferences("data",0);
        settings.edit()
                .putString("filename", filename)
                .apply();
    }
    public String readData(){
        settings = getSharedPreferences("data",0);
        return settings.getString("filename", "");
    }

}
