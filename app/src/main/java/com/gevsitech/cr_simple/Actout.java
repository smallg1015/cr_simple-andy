package com.gevsitech.cr_simple;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Actout extends AppCompatActivity {
    private ListView actout_list;
    private String[] actout=new String[]{"轉速-->動力輸出","輸入電壓-->動力輸出","控制器溫度-->動力輸出","馬達溫度-->動力輸出"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actout);
        actout_list=(ListView)findViewById(R.id.actout_list);

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,actout);
        actout_list.setAdapter(adapter);
        actout_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch(position){
                    case 0:{

                    }
                }
            }
        });
    }
}
