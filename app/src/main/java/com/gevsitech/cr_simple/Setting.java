package com.gevsitech.cr_simple;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gevsitech.cr_simple.data_separation.PrimaevalData;
import com.gevsitech.cr_simple.data_separation.ReadCheck;
import com.gevsitech.cr_simple.data_separation.Savelag;
import com.gevsitech.cr_simple.data_separation.SettingCheck;
import com.gevsitech.cr_simple.data_separation.WriteCheck;
import com.kyleduo.switchbutton.SwitchButton;

import java.io.File;
import java.util.Calendar;

import static android.content.Context.MODE_PRIVATE;

public class Setting extends Fragment {

    private ListView SettingList;
    private ListView SettingList2;
    String Setnamenet[] = {"自動連線"};
    String Setnamenet2[] = {/*"Bluetoothloader",*/"版本資訊","控制器資訊","帳號/授權"};
    String txt_text[] =new String[3];
    private SharedPreferences prebluetooth;
    private SharedPreferences preautoblue;
    private SharedPreferences preautokey;
    String AutoKey;
    private FragmentManager fm;


    //紀錄之參數
    private File LogPath,fileexist;
    String logFileName;
    String SaveFileName;
    LogService logService = LogService.getLogService();
    private String downFilename;

    ViewHolder2 viewHolder2;

    UploadFile Fileupload;
    String uploadURL;

    private BluetoothService bluetoothService=BluetoothService.getBluetoothService();
    private PrimaevalData primaevalData=new PrimaevalData();
    private ReadCheck readCheck=new ReadCheck();
    private WriteCheck writeCheck=new WriteCheck();
    private ProgressDialog dialog;
    private volatile boolean checkflag=true;
    private int SendCount;
    private boolean Sendflag;
    private SettingCheck settingCheck=new SettingCheck();






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_setting, container, false);
    }


    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);



        bluetoothService.setBtHandler(BTPoccess);
        prebluetooth = getActivity().getSharedPreferences("prebluetooth", MODE_PRIVATE);
        preautokey = getActivity().getSharedPreferences("preautokey", MODE_PRIVATE);
        String autokey=preautokey.getString("autokey","");

        LogPath = new File(Environment.getExternalStorageDirectory().getPath()+"/CR_simple");
        //取得今日日期
        Calendar mCal = Calendar.getInstance();
        CharSequence ss = DateFormat.format("yyyy-MM-dd", mCal.getTime());    // kk:24小時制, hh:12小時制
        logFileName = String.valueOf(ss)+".txt";
        SaveFileName = String.valueOf(ss)+"saveData"+".txt";
        fileexist=new File(LogPath.getPath()+File.separator+SaveFileName);

        //ListView的副標說明
        txt_text[0]="Version 0.6Beta";
        txt_text[1]="";
        txt_text[2]="";


//        if(bluetoothService.isConnectedCheck()){
//            dialog = ProgressDialog.show(getActivity(),
//                    "讀取初始資料中", "請稍待片刻...",true);
//            SendCount = 1 ;
//            Sendflag  = true;
//            Thread thread = new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    while(Sendflag){
//                        try {
//                            Thread.sleep(200);
//                        }catch (Exception e){
//
//                        }
//                        switch (SendCount) {
//                            case 1 :
//                                //寫入1
//                                char[] s1 = settingCheck.Send("03EE");
//                                if(checkflag){
//                                    bluetoothService.write2(s1);
//                                    SendCount = 0 ;
//                                    checkflag = false;
//                                    //dialog.setMessage("寫入25%");
//                                }
//                                break;
//                            case 0 :
//                                if(checkflag) {
//                                    Sendflag = false;
//                                    break;
//                                }
//                        }
//
//                    }
//                    BTPoccess.obtainMessage(5).sendToTarget();
//
//                }
//            });
//            thread.start();
//
//
//
//        }else{
//            Toast.makeText(getActivity(),"藍芽尚未連線，無法讀取資料",Toast.LENGTH_SHORT).show();
//
//        }



        //獲取手機id作為上傳檔案辨認哪個裝置
        String androidId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        Fileupload=new UploadFile(androidId);

        uploadURL="http://wiper.gevsi-tech.com/uploadLogtest.php?";




        fm=getActivity().getSupportFragmentManager();




        SettingList = (ListView)getView().findViewById(R.id.SettingList);
        SettingList2= (ListView)getView().findViewById(R.id.SettingList2);

        SettingList.setVisibility(ListView.GONE);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_multiple_choice, Setnamenet);
        Settingadapter cunstomadapter=new Settingadapter(getActivity());

        SettingList.setAdapter(adapter);
        SettingList2.setAdapter(cunstomadapter);
        SettingList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        SettingList.setOnItemClickListener(SettingListener);
        SettingList2.setOnItemClickListener(SettingListener2);



        SettingList.setEnabled(false);

        if(autokey.equals("open")){
            SettingList.setItemChecked(0,true);
            AutoKey = "open";
        }


    }

    private ListView.OnItemClickListener SettingListener = new ListView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String address = prebluetooth.getString("bluetoohaddress", "");
            System.out.println(address);
            if (SettingList.isItemChecked(position)) {
                if (address.equals("")) {
                    Toast.makeText(getActivity(), "尚未有連線裝置紀錄", Toast.LENGTH_LONG).show();
                } else {
                    preautoblue= getActivity().getSharedPreferences("autobluetooth", MODE_PRIVATE);
                    preautoblue.edit()
                            .putString("autobluetoohadress", address)
                            .commit();
                    Toast.makeText(getActivity(), "已設定自動連線", Toast.LENGTH_LONG).show();
                }
                AutoKey = "open";
            } else {
                preautoblue = getActivity().getSharedPreferences("autobluetooth", MODE_PRIVATE);
                preautoblue.edit()
                        .clear()
                        .commit();
                Toast.makeText(getActivity(), "取消自動連線", Toast.LENGTH_LONG).show();
                AutoKey = "";

            }
        }

    };

    private ListView.OnItemClickListener SettingListener2 = new ListView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id){
            String listname=SettingList2.getItemAtPosition(position).toString();
            switch (position){
                case 0:{
                    Intent intent=new Intent(getActivity(),VersionDataActivity.class);
                    startActivity(intent);
                }
                break;
                case 1:{
                    Intent intent=new Intent(getActivity(),DriveDataActivity.class);
                    startActivity(intent);
                }
                break;
                case 2:{
                    Intent intent=new Intent(getActivity(),LoginActivity.class);
                    startActivity(intent);
                }

                break;
            }
        }
    };



    public class Settingadapter extends BaseAdapter{
        final int TYPE_1=0;
        final int TYPE_2=1;
        private LayoutInflater myInflater;
        public Settingadapter(Context c){
            myInflater=LayoutInflater.from(c);
        }
        @Override
        public int getCount(){
            return Setnamenet2.length;
        }
        @Override
        public Object getItem(int position){
            return Setnamenet2[position];
        }
        @Override
        public long getItemId(int postion){
            return postion;
        }
        @Override
        public int getItemViewType(int position){
            int p=position;
            if(p<3){
                return TYPE_1;
            }else{return TYPE_2;}

        }
        @Override
        public int getViewTypeCount(){return 2;}
        @Override
        public View getView(int position,View convertView,ViewGroup parent){
            viewHolder2=null;

            if(convertView==null){
                switch(getItemViewType(position)){
                    case TYPE_1:
                        viewHolder2=new ViewHolder2();
                        convertView=myInflater.inflate(R.layout.list_setting,null);
                        viewHolder2.txt_head=(TextView)convertView.findViewById(R.id.txt_listhead);
                        viewHolder2.txt_content=(TextView)convertView.findViewById(R.id.txt_listsub);

                        viewHolder2.txt_head.setText(Setnamenet2[position]);
                        viewHolder2.txt_content.setText(txt_text[position]);

                        convertView.setTag(viewHolder2);
                        break;

                    case TYPE_2:
                        break;

                }
            }else{
                switch (getItemViewType(position)){
                    case TYPE_1:
                        viewHolder2=(ViewHolder2) convertView.getTag();
                        viewHolder2.txt_head.setText(Setnamenet2[position]);
                        viewHolder2.txt_content.setText(txt_text[position]);
                        break;
                    case TYPE_2:
                       break;

                }
            }

            return convertView;
        }
    }

    private static class ViewHolder2{
        TextView txt_head;
        TextView txt_content;
    }

    //显示Fragment
    private void showFragment(Fragment fragment) {
        //先隐藏
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.center,fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    private Handler BTPoccess = new Handler(){
        private StringPoccess poccess = new StringPoccess();

        private String data ;

        private boolean tagflag = false;

        /**
         *  連線中狀態值
         *  1. DISCONNECT 斷線狀態    0
         *  2. CONNECTED  連線中      1
         *  3. CONNECTING 嘗試連線中  2
         **/
        private final static int DISCONNECT = 0 ;
        private final static int CONNECTED = 1 ;
        private final static int CONNECTING = 2 ;

        /**
         *  連線中狀態值
         *  1. DATAWAIT   等待資料中    0
         *  2. DATAREAD   資料讀取      1
         *  3. DATAWIRTE  資料寫入      2
         **/

        private final static int DATAWAIT = 0 ;
        private final static int DATAREAD = 1 ;
        private final static int DATAWIRTE = 2 ;

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case DISCONNECT :
                    if(tagflag) Log.d("Handle" ,"DISCONNECT" );
                    break;
                case CONNECTED :
                    if(tagflag)Log.d("Handle" ,"CONNECTED" );

                    if(msg.arg1 == DATAREAD) {
                        data = (String) msg.obj;
                        if(data == null)return;
                        System.out.println(data);
                        primaevalData.split(data);
                    }

                    //System.out.println(data);
                    if(primaevalData.startcheck()) {
                        if (primaevalData.getId()[2] == 0xCE) {
                            writeCheck.setIDCheck(primaevalData.getdata());
                            checkflag = true;

                        }

                        if(primaevalData.getId()[2]== 0xCF){
                            readCheck.setData(primaevalData.getdata());
                            if (readCheck.getAddress().equals("081")) {
                                String s = readCheck.getPassword();
                                System.out.println("授權碼="+s);
                                if(s.equals("4576")){
                                    txt_text[2]="以取得授權";
                                    SettingList2.invalidateViews();

                                }
                                readCheck.getValues();
                                checkflag = true;
                            }

                        }
                    }

                    break;
                case CONNECTING :
                    if(tagflag)Log.d("Handle" ,"CONNECTING" );
                    break;
                case 5:
                    try {
                        dialog.dismiss();
                    }catch (Exception e){
                        dialog.dismiss();
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };




    @Override
    public void onPause() {
        super.onPause();
        preautokey.edit()
                .putString("autokey", AutoKey)
                .commit();
        SharedPreferences preSave=getActivity().getSharedPreferences("preSave",MODE_PRIVATE);

    }
}