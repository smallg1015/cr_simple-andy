package com.gevsitech.cr_simple;

import android.text.format.DateFormat;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

/**
 * Created by Jian on 2017/4/9.
 */

public class LogService {
    public static LogService logService = new LogService();

    public static LogService getLogService(){
        return logService;
    }

    public Boolean SaveLog(File file , String filename , String msg){
        String filepath = file.getAbsolutePath()+File.separator+filename;
        String s = "";
        file = new File(filepath);
        try {
            if(file.exists())file.createNewFile();
        } catch (IOException e) {
            Log.e("LogService", "Createfaile");
            e.printStackTrace();
            return false;
        }

        try{

            FileWriter fw = new FileWriter(filepath, true);
            BufferedWriter bw = new BufferedWriter(fw);
            // 方法一
            Calendar mCal = Calendar.getInstance();
            CharSequence ss = DateFormat.format("kk-mm-ss", mCal.getTime());    // kk:24小時制, hh:12小時制
            s = String.valueOf(ss);

            bw.write("["+s+"] : " + msg);
            bw.newLine();
            bw.close();
        }catch(IOException e){
            Log.e("LogService", "Writefaile");

            return false;
        }
        Log.d("LogService", "WriteSuccess : "+ s + "["+s+"] : " + msg);
        return true;
    }

    public Boolean SaveData(File file , String filename, String[] tempdata){
        if(file.exists()){}else{file.mkdirs();}
        String filepath = file.getAbsolutePath()+File.separator+filename;
        String s = "";
        //可換種寫法


        File namefile = new File(filepath);
        try {
            if(namefile.exists()){}else{namefile.createNewFile();}
        } catch (IOException e) {
            Log.e("LogService", "Createfaile");
            e.printStackTrace();
            return false;
        }

        try{

            FileWriter fw = new FileWriter(filepath, true);
            BufferedWriter bw = new BufferedWriter(fw);
            // 方法一

            for(int i=0;i<7;i++){
                bw.write(tempdata[i]);
                bw.newLine();
            }
            bw.close();
        }catch(IOException e){
            Log.e("LogService", "Writefaile");

            return false;
        }
        Log.d("LogService", "WriteSuccess : "+ s + "["+s+"] : " +tempdata);
        return true;
    }


}
