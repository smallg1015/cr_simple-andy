package com.gevsitech.cr_simple;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Protection extends AppCompatActivity {

    private String[] protect=new String[]{"相關保護設定","控制器溫度保護設定","馬達溫度保護設定"};
    private ListView protect_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_protection);

        protect_list=(ListView)findViewById(R.id.protect_list);
        ArrayAdapter<String> adapter =new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,protect);
        protect_list.setAdapter(adapter);
        protect_list.setOnItemClickListener(new ListView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?>parent,View view,int position,long id){
                switch(position){
                    case 0: {
                        Intent intent = new Intent();
                        intent.setClass(Protection.this, Protection_battery.class);
                        startActivity(intent);
                        break;
                    }
                    case 1: {
                        Intent intent = new Intent();
                        intent.setClass(Protection.this, Protection_mos.class);
                        startActivity(intent);
                        break;
                    }
                    case 2: {
                        Intent intent = new Intent();
                        intent.setClass(Protection.this, Protection_moda.class);
                        startActivity(intent);
                        break;
                    }

                }
            }
        });
    }
}
